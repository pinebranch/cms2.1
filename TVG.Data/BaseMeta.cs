﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Data
{
    public interface IBaseMeta
    {
        string MetaKey { get; set; }
        string MetaValue { get; set; }
    }

    public static class BaseMetaExtensions
    {
        public static string GetMetaValue(this List<IBaseMeta> metas, string metaKey)
        {
            if (string.IsNullOrEmpty(metaKey)) return "";
            var meta = metas.Find(x => x.MetaKey == metaKey);
            return meta != null ? meta.MetaValue : "";
        }
    }
}
