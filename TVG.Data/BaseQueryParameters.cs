﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Data
{
    public class BaseQueryParameters
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = int.MaxValue;
    }
}
