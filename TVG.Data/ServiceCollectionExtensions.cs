﻿using Microsoft.Extensions.DependencyInjection;
using TVG.Data.Services;

namespace TVG.Data
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDataServices(this IServiceCollection services)
        {
            services.AddTransient<PermissionService>();
            services.AddTransient<TaxonomyService>();
            services.AddTransient<PostTypeService>();
            services.AddTransient<PostService>();

            return services;
        }
    }
}
