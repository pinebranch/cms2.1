﻿namespace TVG.Data.TvgConstants
{
    public static class CacheNames
    {
        public const string IDENTITY_USERS_ALL = "c.identity.user.all-";        
        public const string IDENTITY_USERS_ALL_GROUP = "c.identity.user.all.group-";
        public const string IDENTITY_USER_BY_ID = "c.identity.user.by.id-";
        public const string IDENTITY_USER_BY_NAME = "c.identity.user.by.name-";
        public const string IDENTITY_USER_BY_EMAIL = "c.identity.user.by.email-";
        public const string IDENTITY_USER_GROUP_BY_ID = "c.identity.user.group.id-";
        public const string IDENTITY_USER_CLAIMS_BY_ID = "c.identity.user.claims.id-";
        public const string IDENTITY_USERS_ALL_BY_CLAIM = "c.identity.user.all.by.claim-";
        public const string IDENTITY_USERS_ALL_CLAIM_GROUP = "c.identity.user.all.claim.group-";
        public const string IDENTITY_USER_BY_LOGIN = "c.identity.user.by.login-";
        public const string IDENTITY_USER_LOGIN_GROUP = "c.identity.user.login.group";
        public const string IDENTITY_USER_LOGINS_BY_ID = "c.identity.user.logins.id-";
        public const string IDENTITY_USER_ROLE = "c.identity.user.role-";
        public const string IDENTITY_USER_ROLES_BY_ID = "c.identity.user.roles.id-";
        public const string IDENTITY_USERS_BY_ROLE = "c.identity.users.by.role.id-";
        public const string IDENTITY_ROLE_ALL = "c.identity.role.all";

        public const string PERMISSION_CATEGORY_ALL = "c.permission.category.all";
        public const string PERMISSION_ALL = "c.permission.all-";
        public const string PERMISSION_GROUP_ALL = "c.permission.group.all";
        public const string PERMISSION_BY_ID = "c.permission.id-";
        public const string PERMISSION_BY_NAME = "c.permission.name-";
        public const string PERMISSION_ROLE_BY_ROLE = "c.permission.role.by.role-";
        public const string PERMISSION_ROLE_BY_USER = "c.permission.role.by.user-";
        public const string PERMISSION_ROLE_BY_CATEGORY = "c.permission.role.by.category-";
        public const string PERMISSION_ROLE_GROUP = "c.permission.role.group";

        public const string TAXONOMY_ALL = "c.taxonomy.all";
        public const string TERM_ALL = "c.term.all-";
        public const string TERM_ALL_HIERARCHY = "c.term.all.h-";
        public const string TERM_GROUP_ALL = "c.term.group.all-";
        public const string TERM_BY_ID = "c.term.id-";
        public const string TERM_BY_SLUG = "c.term.slug-";
        public const string TERM_META_BY_TERM_ID = "c.term.meta.t.id-";
        public const string TERM_GROUP_BY_ID = "c.term.group.id-";

        public const string POST_TYPE_ALL = "c.posttype.all";
        public const string POST_TYPE_TAXONOMIES_BY_POST_TYPE = "c.posttype.taxonomies-";

        public const string POST_ALL = "c.post.all-";
        public const string POST_ALL_GROUP = "c.post.all.group";
        public const string POST_BY_ID = "c.post.id-";
        public const string POST_GROUP_BY_ID = "c.post.group.id-";
        public const string POST_BY_SLUG = "c.post.slug-";
        public const string POST_CHILDREN_BY_POST_ID = "c.post.children.pid-";
        public const string POST_META_BY_POST_ID = "c.post.meta.pid-";
        public const string POST_TERM_BY_POST_ID = "c.post.term.pid-";
    }
}
