﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TVG.Data.TvgConstants
{
    public enum PublishStatus
    {
        [Description("Không xác định"), Color("#e4e4e4")]
        KhongXacDinh = -1,

        [Description("Chờ duyệt"), Color("#03a9f4")]
        ChoDuyet = 0,

        [Description("Đã duyệt"), Color("#4caf50")]
        DaDuyet = 1,

        [Description("Bị trả về"), Color("#F44336")]
        BiTraVe = 2,

        [Description("Nháp"), Color("#9E9E9E")]
        Nhap = 3
    }

    [AttributeUsage(AttributeTargets.Field)]
    internal class ColorAttribute : Attribute
    {
        public string Color { get; }

        public ColorAttribute(string color)
        {
            Color = color;
        }
    }
}
