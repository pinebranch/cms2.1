﻿namespace TVG.Data.TvgConstants
{
    public static class RedisDatabases
    {
        public const int DEFAULT = 0;
        public const int USERS = 1;
        public const int POSTS = 2;
    }
}
