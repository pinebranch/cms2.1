﻿namespace TVG.Data.TvgConstants
{
    public static class TableNames
    {
        public const string TABLE_ROLE_CLAIMS = "AspNetRoleClaims";
        public const string TABLE_ROLES = "AspNetRoles";
        public const string TABLE_USER_CLAIMS = "AspnetUserClaims";
        public const string TABLE_USER_LOGINS = "AspNetUserLogins";
        public const string TABLE_USER_ROLES = "AspNetUserRoles";
        public const string TABLE_USERS = "AspNetUsers";
        public const string TABLE_USER_TOKENS = "AspNetUserTokens";

        public const string TABLE_PERMISSION_CATEGORY = "PermissionCategory";
        public const string TABLE_PERMISSION = "Permission";
        public const string TABLE_PERMISSION_ROLE = "PermissionRole";

        public const string TABLE_TAXONOMY = "Taxonomy";
        public const string TABLE_TERM = "Term";
        public const string TABLE_TERM_META = "TermMeta";

        public const string TABLE_POST_TYPE = "PostType";
        public const string TABLE_POST_TYPE_TAXONOMY = "PostTypeTaxonomy";
        public const string TABLE_POST = "Post";
        public const string TABLE_POST_META = "PostMeta";
        public const string TABLE_POST_TERM = "PostTerm";

    }
}
