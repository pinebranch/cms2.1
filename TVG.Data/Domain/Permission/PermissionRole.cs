﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Data.Domain
{
    [TableName("PermissionRole")]
    [PrimaryKey("PermissionId, RoleId", AutoIncrement = false)]
    public class PermissionRole
    {
        public int PermissionId { get; set; }
        public int RoleId { get; set; }

        [ResultColumn] public string PermissionName {get;set;}
    }
}
