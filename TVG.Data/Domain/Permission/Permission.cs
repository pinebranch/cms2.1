﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Data.Domain
{
    [TableName("Permission")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Permission
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }

    public class PermissionQueryParameters : BaseQueryParameters
    {
        public int CategoryId { get; set; }
    }
}
