﻿using NPoco;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Extensions;
using TVG.Data.Domain;
using TVG.Data.TvgConstants;

namespace TVG.Data.Services
{
    public class PermissionService : BaseService
    {
        public PermissionService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.USERS;
        }

        #region PermissionCategory
        public Task<List<PermissionCategory>> GetPermissionCategoriesAsync()
        {
            var key = CacheHelper.SetCacheName(CacheNames.PERMISSION_CATEGORY_ALL);
            return cache.GetOrAddAsync("", key, () =>
            {
                return db.FetchAsync<PermissionCategory>("ORDER BY DisplayOrder");
            }, CacheHelper.CacheDurationLong);
        }

        public async Task<PermissionCategory> GetPermissionCategoryByIdAsync(int id)
        {
            List<PermissionCategory> list = await GetPermissionCategoriesAsync().ConfigureAwait(false);
            return list.Find(x => x.Id == id);
        }

        public async Task<int> InsertPermissionCategoryAsync(PermissionCategory obj)
        {
            cache.Remove(CacheNames.PERMISSION_CATEGORY_ALL);
            await db.InsertAsync(obj).ConfigureAwait(false);
            return obj.Id;
        }

        public Task<int> UpdatePermissionCategoryAsync(PermissionCategory obj)
        {
            cache.Remove(CacheNames.PERMISSION_CATEGORY_ALL);
            return db.UpdateAsync(obj);
        }

        public Task<int> DeletePermissionCategoryAsync(int id)
        {
            cache.Remove(CacheNames.PERMISSION_CATEGORY_ALL);
            return db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_PERMISSION_CATEGORY} WHERE Id=@0", id);
        }
        #endregion

        #region Permission
        public Task<Page<Permission>> GetPermissionsAsync(PermissionQueryParameters parameters)
        {
            string key = CacheHelper.SetCacheName(CacheNames.PERMISSION_ALL, parameters.CategoryId, parameters.PageIndex, parameters.PageSize);
            string group = CacheNames.PERMISSION_GROUP_ALL;
            return cache.GetOrAddAsync(group, key, () =>
            {
                var sql = new Sql();
                sql.Select("*");
                sql.From(TableNames.TABLE_PERMISSION);
                sql.WhereIf(parameters.CategoryId > 0, "CategoryId=@0", parameters.CategoryId);
                sql.OrderBy("Name");
                return db.PageAsync<Permission>(parameters.PageIndex, parameters.PageSize, sql);
            }, CacheHelper.CacheDurationLong);
        }

        public Task<Permission> GetPermissionByIdAsync(int id)
        {
            var key = CacheHelper.SetCacheName(CacheNames.PERMISSION_BY_ID, id);
            
            return cache.GetOrAddAsync("", key, () =>
            {
                return db.FirstOrDefaultAsync<Permission>("WHERE Id=@0", id);
            }, CacheHelper.CacheDurationLong);
        }

        public Task<Permission> GetPermissionByNameAsync(string name)
        {
            var key = CacheHelper.SetCacheName(CacheNames.PERMISSION_BY_NAME, name);
            
            return cache.GetOrAddAsync("", key, () =>
            {
                return db.FirstOrDefaultAsync<Permission>("WHERE Name=@0", name);
            }, CacheHelper.CacheDurationLong);
        }

        public async Task<int> InsertPermissionAsync(Permission obj)
        {
            cache.RemoveByGroup(CacheNames.PERMISSION_GROUP_ALL);
            await db.InsertAsync(obj).ConfigureAwait(false);
            return obj.Id;
        }

        public Task<int> UpdatePermissionAsync(Permission obj)
        {
            cache.RemoveByGroup(CacheNames.PERMISSION_GROUP_ALL);
            cache.Remove(CacheHelper.SetCacheName(CacheNames.PERMISSION_BY_ID, obj.Id));
            cache.Remove(CacheHelper.SetCacheName(CacheNames.PERMISSION_BY_NAME, obj.Name));
            return db.UpdateAsync(obj);
        }

        public async Task<int> DeletePermissionAsync(int id)
        {
            var result = 0;
            var obj = await GetPermissionByIdAsync(id).ConfigureAwait(false);
            if (obj != null)
            {
                cache.RemoveByGroup(CacheNames.PERMISSION_GROUP_ALL);
                cache.Remove(CacheHelper.SetCacheName(CacheNames.PERMISSION_BY_ID, obj.Id));
                cache.Remove(CacheHelper.SetCacheName(CacheNames.PERMISSION_BY_NAME, obj.Name));
                result = await db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_PERMISSION} WHERE Id=@0", id).ConfigureAwait(false);
            }
            return result;
        }
        #endregion

        #region PermissionRole
        public Task<List<PermissionRole>> GetPermissionsByRoleAsync(int roleId)
        {
            var key = CacheHelper.SetCacheName(CacheNames.PERMISSION_ROLE_BY_ROLE, roleId);
            var group = CacheNames.PERMISSION_ROLE_GROUP;
            return cache.GetOrAddAsync(group, key, () => {
                var sql = $@"SELECT pr.*, p.Name AS PermissionName
                        FROM {TableNames.TABLE_PERMISSION_ROLE} pr
                        INNER JOIN {TableNames.TABLE_PERMISSION} p ON pr.PermissionId = p.Id 
                        WHERE RoleId=@0";
                return db.FetchAsync<PermissionRole>(sql, roleId);
            }, CacheHelper.CacheDurationLong);
        }

        public async Task<bool> CheckPermissionRoleAsync(int roleId, int permissionId)
        {
            var list = await GetPermissionsByRoleAsync(roleId).ConfigureAwait(false);
            return list.Any(x => x.RoleId == roleId && x.PermissionId == permissionId);
        }

        public async Task SavePermissionRolesAsync(int categoryId, List<PermissionRole> permissionRoles)
        {
            using (var transaction = db.GetTransaction())
            {
                await db.ExecuteAsync($@"DELETE pr FROM {TableNames.TABLE_PERMISSION_ROLE} pr
                            INNER JOIN {TableNames.TABLE_PERMISSION} p ON pr.PermissionId = p.Id
                            WHERE p.CategoryId = @0", categoryId).ConfigureAwait(false);
                db.InsertBatch<PermissionRole>(permissionRoles);
                transaction.Complete();
                cache.RemoveByGroup(CacheNames.PERMISSION_ROLE_GROUP);
            }
        }
        #endregion
    }
}
