﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Data.Domain
{
    [TableName("PermissionCategory")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class PermissionCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
    }
}
