﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;

namespace TVG.Data.Domain
{
    [TableName("Taxonomy")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Taxonomy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsHierarchy { get; set; }
        public bool HasThumbnail { get; set; }
        public bool HasColor { get; set; }
        public string Note { get; set; }
    }
}
