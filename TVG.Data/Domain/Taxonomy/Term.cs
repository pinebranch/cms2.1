﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Data.Domain
{
    [TableName(TableNames.TABLE_TERM)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Term
    {
        public int Id { get; set; }
        public string Taxonomy { get; set; } = "";
        public int ParentId { get; set; }
        public string Name { get; set; } = "";
        public string Slug { get; set; } = "";
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        public int DisplayOrder { get; set; }
        public int Count { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }

        [Ignore] public int Level { get; set; }
        [Ignore] public List<string> Path { get; set; } = new List<string>();
    }

    public class TermQueryParameters : BaseQueryParameters
    {
        public string Taxonomy { get; set; }
        public int ParentId { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}
