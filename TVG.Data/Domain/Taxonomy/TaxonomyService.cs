﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core;
using TVG.Core.Caching;
using TVG.Data.Domain;
using TVG.Data.TvgConstants;

namespace TVG.Data.Services
{
    public class TaxonomyService : BaseService
    {
        public TaxonomyService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.DEFAULT;
        }

        #region Taxonomy

        public Task<List<Taxonomy>> GetTaxonomiesAsync()
        {
            var key = CacheNames.TAXONOMY_ALL;
            return cache.GetOrAddAsync("", key, () => db.FetchAsync<Taxonomy>(), CacheHelper.CacheDurationLong);
        }

        public async Task<Taxonomy> GetTaxonomyAsync(string name)
        {
            var list = await GetTaxonomiesAsync().ConfigureAwait(false);
            return list.Find(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        public async Task<bool> IsTaxonomyExistsAsync(string name)
        {
            return await GetTaxonomyAsync(name).ConfigureAwait(false) != null;
        }

        public async Task<int> InsertTaxonomyAsync(Taxonomy obj)
        {
            if (obj == null) return 0;
            await db.InsertAsync(obj).ConfigureAwait(false);
            cache.Remove(CacheNames.TAXONOMY_ALL);
            return obj.Id;
        }

        public Task<int> UpdateTaxonomyAsync(Taxonomy obj)
        {
            if (obj == null) return Task.FromResult(0);
            cache.Remove(CacheNames.TAXONOMY_ALL);
            return db.UpdateAsync(obj);
        }

        public Task<int> DeleteTaxonomyAsync(int id)
        {
            cache.Remove(CacheNames.TAXONOMY_ALL);
            return db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_TAXONOMY} WHERE Id=@0", id);
        }

        #endregion

        #region Term

        public async Task<Page<Term>> GetTermsAsync(TermQueryParameters parameters)
        {
            var taxonomy = await GetTaxonomyAsync(parameters.Taxonomy).ConfigureAwait(false);
            if (taxonomy == null) return new Page<Term> { Items = new List<Term>() };

            var key = CacheHelper.SetCacheName(CacheNames.TERM_ALL,
                parameters.Taxonomy, parameters.ParentId, parameters.IsDeleted, parameters.PageIndex, parameters.PageSize);
            var group = CacheHelper.SetCacheName(CacheNames.TERM_GROUP_ALL, parameters.Taxonomy);

            return await cache.GetOrAddAsync(group, key, async () =>
            {
                if (!taxonomy.IsHierarchy)
                {
                    var sql = new Sql();
                    sql.Select("*").From(TableNames.TABLE_TERM);
                    sql.Where("Taxonomy=@0", parameters.Taxonomy);
                    sql.Where("IsDeleted=@0", parameters.IsDeleted);
                    sql.OrderBy("DisplayOrder");
                    return await db.PageAsync<Term>(parameters.PageIndex, parameters.PageSize, sql).ConfigureAwait(false);
                }
                else
                {
                    var list = await GetAllTermsHierarchyAsync(parameters).ConfigureAwait(false);

                    var pagedList = new Page<Term>
                    {
                        TotalItems = list.Count,
                        CurrentPage = parameters.PageIndex,
                        ItemsPerPage = parameters.PageSize,
                        TotalPages = (long)Math.Ceiling(list.Count * 1.0 / parameters.PageSize * 1.0),
                        Items = list.Skip((parameters.PageIndex - 1) * parameters.PageSize).Take(parameters.PageSize).ToList()
                    };
                    return pagedList;
                }
            }, CacheHelper.CacheDurationMedium).ConfigureAwait(false);
        }

        public async Task<List<Term>> GetTermsAsync(string taxonomyName)
        {
            var taxonomy = await GetTaxonomyAsync(taxonomyName).ConfigureAwait(false);
            if (taxonomy == null) return new List<Term>();

            if(!taxonomy.IsHierarchy)
            {
                return await GetAllTermsAsync(taxonomyName).ConfigureAwait(false);
            }
            else
            {
                return await GetAllTermsHierarchyAsync(new TermQueryParameters { Taxonomy = taxonomyName, IsDeleted = false }).ConfigureAwait(false);
            }
        }

        private async Task<List<Term>> GetAllTermsAsync(string taxonomyName, bool isDeleted = false)
        {
            var taxonomy = await GetTaxonomyAsync(taxonomyName).ConfigureAwait(false);
            if (taxonomy == null) return new List<Term>();

            var key = CacheHelper.SetCacheName(CacheNames.TERM_ALL, taxonomyName);
            var group = CacheHelper.SetCacheName(CacheNames.TERM_GROUP_ALL, taxonomyName);

            return await cache.GetOrAddAsync(group, key, () =>
            {
                return db.FetchAsync<Term>("WHERE Taxonomy=@0 AND IsDeleted=@1 ORDER BY DisplayOrder, Id", taxonomyName, isDeleted);
            }, CacheHelper.CacheDurationMedium).ConfigureAwait(false);
        }

        private Task<List<Term>> GetAllTermsHierarchyAsync(TermQueryParameters parameters)
        {
            var key = CacheHelper.SetCacheName(CacheNames.TERM_ALL_HIERARCHY, parameters.Taxonomy);
            var group = CacheHelper.SetCacheName(CacheNames.TERM_GROUP_ALL, parameters.Taxonomy);

            return cache.GetOrAddAsync(group, key, async () =>
            {
                var originalList = await GetAllTermsAsync(parameters.Taxonomy, parameters.IsDeleted).ConfigureAwait(false);
                var list = new List<Term>();
                var children = new Dictionary<int, List<int>>();
                foreach (var item in originalList)
                {
                    if (item.ParentId == 0) continue;

                    if (children.ContainsKey(item.ParentId))
                    {
                        children[item.ParentId].Add(item.Id);
                    }
                    else
                    {
                        children.Add(item.ParentId, new List<int> { item.Id });
                    }
                }

                foreach (var item in originalList)
                {
                    if (item.ParentId == parameters.ParentId)
                    {
                        list.Add(item);
                        item.Path.Add(item.Name);
                        if (children.ContainsKey(item.Id))
                        {
                            list.AddRange(GetTermFromListRecursive(originalList, children, item.Id, item.Level + 1, item.Path));
                        }
                    }
                }

                return list;
            }, CacheHelper.CacheDurationMedium);
        }

        private List<Term> GetTermFromListRecursive(List<Term> terms, Dictionary<int, List<int>> childrenList, int parentId, int level, List<string> path)
        {
            var list = new List<Term>();
            if (!childrenList.ContainsKey(parentId)) return list;

            var childrenIds = childrenList[parentId];
            foreach(var id in childrenIds)
            {
                var term = terms.Find(x => x.Id == id);
                term.Level = level;
                term.Path.AddRange(path);
                term.Path.Add(term.Name);
                list.Add(term);
                if (childrenList.ContainsKey(term.Id))
                {
                    list.AddRange(GetTermFromListRecursive(terms, childrenList, term.Id, term.Level + 1, term.Path));
                }
            }
            return list;
        }

        public Task<Term> GetTermByIdAsync(int id)
        {
            if (id <= 0) return Task.FromResult<Term>(null);

            var key = CacheHelper.SetCacheName(CacheNames.TERM_BY_ID, id);
            var group = CacheHelper.SetCacheName(CacheNames.TERM_GROUP_BY_ID, id);
            return cache.GetOrAddAsync(group, key, () => db.SingleOrDefaultByIdAsync<Term>(id), CacheHelper.CacheDurationMedium);
        }

        public async Task<Term> GetTermBySlugAsync(string slug, string taxonomy)
        {
            if (string.IsNullOrEmpty(slug) || string.IsNullOrEmpty(taxonomy)) return null;

            var key = CacheHelper.SetCacheName(CacheNames.TERM_BY_SLUG, taxonomy, slug);
            var term = await cache.GetOrAddAsync("", key, () => {
                return db.SingleOrDefaultAsync<Term>("WHERE Slug=@0 AND Taxonomy=@1", slug, taxonomy);
            }, CacheHelper.CacheDurationMedium).ConfigureAwait(false);

            if(term != null)
            {
                cache.AddKeyToGroup(CacheHelper.SetCacheName(CacheNames.TERM_GROUP_BY_ID, term.Id), key, CacheHelper.CacheDurationMedium);
            }

            return term;
        }

        public async Task<int> InsertTermAsync(Term obj)
        {
            if (obj == null) return 0;

            obj.Slug = StringHelper.Slugify(obj.Slug);

            while(await GetTermBySlugAsync(obj.Slug, obj.Taxonomy).ConfigureAwait(false) != null)
            {
                obj.Slug += "-1";
            }

            await db.InsertAsync(obj).ConfigureAwait(false);
            AfterModifyTerm(obj);
            return obj.Id;
        }

        public async Task<int> UpdateTermAsync(Term obj)
        {
            if (obj == null) return 0;

            obj.Slug = StringHelper.Slugify(obj.Slug);

            while (await GetTermBySlugAsync(obj.Slug, obj.Taxonomy).ConfigureAwait(false) != null)
            {
                obj.Slug += "-1";
            }

            var result = await db.UpdateAsync(obj).ConfigureAwait(false);
            AfterModifyTerm(obj);
            return result;
        }

        public async Task<int> DeleteTermAsync(int id, bool isPermanent = false)
        {
            var obj = await GetTermByIdAsync(id).ConfigureAwait(false);
            if (obj == null) return 0;

            var result = 0;
            if (!isPermanent)
            {
                obj.IsDeleted = true;
                using(var transaction = db.GetTransaction())
                {
                    await db.ExecuteAsync($"UPDATE {TableNames.TABLE_TERM} SET ParentId=@0 WHERE ParentId=@1", obj.ParentId, obj.Id).ConfigureAwait(false);
                    result = await UpdateTermAsync(obj).ConfigureAwait(false);
                    transaction.Complete();
                }
            }
            else
            {
                using (var transaction = db.GetTransaction())
                {
                    if (!obj.IsDeleted)
                    {
                        await db.ExecuteAsync($"UPDATE {TableNames.TABLE_TERM} SET ParentId=@0 WHERE ParentId=@1", obj.ParentId, obj.Id).ConfigureAwait(false);
                    }
                    result = await db.DeleteAsync(obj).ConfigureAwait(false);
                    transaction.Complete();
                }
            }
            AfterModifyTerm(obj);
            return result;
        }

        #endregion

        #region

        public Task<List<TermMeta>> GetTermMetasAsync(int termId)
        {
            var key = CacheHelper.SetCacheName(CacheNames.TERM_META_BY_TERM_ID, termId);
            var group = CacheHelper.SetCacheName(CacheNames.TERM_GROUP_BY_ID, termId);
            return cache.GetOrAddAsync(group, key, () =>
            {
                return db.FetchAsync<TermMeta>("WHERE TermId=@0", termId);
            }, CacheHelper.CacheDurationMedium);
        }

        #endregion

        private void AfterModifyTerm(Term obj)
        {
            cache.RemoveByGroup(CacheHelper.SetCacheName(CacheNames.TERM_GROUP_ALL, obj.Taxonomy));
            cache.RemoveByGroup(CacheHelper.SetCacheName(CacheNames.TERM_GROUP_BY_ID, obj.Id));
        }
    }
}
