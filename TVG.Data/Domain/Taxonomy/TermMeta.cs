﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;

namespace TVG.Data.Domain
{
    [TableName("TermMeta")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class TermMeta : IBaseMeta
    {
        public int Id { get; set; }
        public int TermId { get; set; }
        public string MetaKey { get; set; }
        public string MetaValue { get; set; }
    }
}
