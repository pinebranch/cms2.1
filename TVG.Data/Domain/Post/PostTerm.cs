﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Data.Domain
{
    [TableName(TableNames.TABLE_POST_TERM)]
    [PrimaryKey("PostId, TermId", AutoIncrement = false)]
    public class PostTerm
    {
        public int PostId { get; set; }
        public int TermId { get; set; }
        public int DisplayOrder { get; set; }
    }
}
