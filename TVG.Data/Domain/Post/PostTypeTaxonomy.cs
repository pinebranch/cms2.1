﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Data.Domain
{
    [TableName(TableNames.TABLE_POST_TYPE_TAXONOMY)]
    [PrimaryKey("PostTypeId, TaxonomyId")]
    public class PostTypeTaxonomy
    {
        public int PostTypeId { get; set; }
        public int TaxonomyId { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsStatusTag { get; set; }
        public bool IsPrimary { get; set; }
    }
}
