﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using EnumsNET;
using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Data.Domain
{
    [TableName(TableNames.TABLE_POST)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Post
    {
        public int Id { get; set; }
        public string PostType { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Image { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public int PublishStatus { get; set; }
        public bool IsDeleted { get; set; }

        [ResultColumn] public string CreatedUserName { get; set; }
        [ResultColumn] public string UpdatedUserName { get; set; }

        [Ignore] public string StatusName
        {
            get { return Enums.Parse<PublishStatus>(this.PublishStatus.ToString()).GetAttributes().Get<DescriptionAttribute>().Description; }
        }

        [Ignore]
        public string StatusColor
        {
            get { return Enums.Parse<PublishStatus>(this.PublishStatus.ToString()).GetAttributes().Get<ColorAttribute>().Color; }
        }
    }

    public class PostQueryParameters : BaseQueryParameters
    {
        public string PostType { get; set; } = "post";
        public List<string> TermIds { get; set; } = new List<string>();
        public string Keyword { get; set; } = "";
        public int PublishStatus { get; set; } = -1;
        public int CreatedBy { get; set; } = 0;
        public bool IsDeleted { get; set; } = false;
    }
}
