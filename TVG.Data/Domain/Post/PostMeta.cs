﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Data.Domain
{
    [TableName(TableNames.TABLE_POST_META)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class PostMeta : IBaseMeta
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string MetaKey { get; set; }
        public string MetaValue { get; set; }
    }
}
