﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Data.Domain;
using TVG.Data.TvgConstants;

namespace TVG.Data.Services
{
    public class PostTypeService :BaseService
    {
        public PostTypeService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.POSTS;
        }

        public Task<List<PostType>> GetPostTypesAsync()
        {
            var key = CacheNames.POST_TYPE_ALL;
            return cache.GetOrAddAsync("", key, () => db.FetchAsync<PostType>(), CacheHelper.CacheDurationLong);
        }

        public async Task<PostType> GetPostTypeAsync(string name)
        {
            var list = await GetPostTypesAsync().ConfigureAwait(false);
            return list.Find(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        public async Task<bool> IsPostTypeExistsAsync(string name)
        {
            return await GetPostTypeAsync(name).ConfigureAwait(false) != null;
        }

        public async Task<int> InsertPostTypeAsync(PostType obj)
        {
            if (obj == null) return 0;
            await db.InsertAsync(obj).ConfigureAwait(false);
            cache.Remove(CacheNames.POST_TYPE_ALL);
            return obj.Id;
        }

        public async Task<int> UpdatePostTypeAsync(PostType obj)
        {
            if (obj == null) return 0;
            cache.Remove(CacheNames.POST_TYPE_ALL);
            var result = await db.UpdateAsync(obj).ConfigureAwait(false);
            return result;
        }

        public Task<int> DeletePostTypeAsync(int id)
        {
            cache.Remove(CacheNames.POST_TYPE_ALL);
            return db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_POST_TYPE} WHERE Id=@0", id);
        }

        public Task<List<PostTypeTaxonomy>> GetPostTypeTaxonomiesAsync(int postTypeId)
        {
            var key = CacheHelper.SetCacheName(CacheNames.POST_TYPE_TAXONOMIES_BY_POST_TYPE, postTypeId);
            return cache.GetOrAddAsync("", key, () => 
            {
                return db.FetchAsync<PostTypeTaxonomy>("WHERE PostTypeId=@0", postTypeId);
            }, CacheHelper.CacheDurationLong);
        }

        public async Task<PostTypeTaxonomy> GetPostTypeStatusTagAsync(int postTypeId)
        {
            var list = await GetPostTypeTaxonomiesAsync(postTypeId).ConfigureAwait(false);
            return list.Find(x => x.IsStatusTag);
        }

        public async Task<PostTypeTaxonomy> GetPrimaryTaxonomyAsync(int postTypeId)
        {
            var list = await GetPostTypeTaxonomiesAsync(postTypeId).ConfigureAwait(false);
            return list.Find(x => x.IsPrimary);
        }

        public async Task SavePostTypeTaxonomiesAsync(int postTypeId, List<PostTypeTaxonomy> taxonomies)
        {
            if (postTypeId <= 0) return;
            using(var transation = db.GetTransaction())
            {
                await db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_POST_TYPE_TAXONOMY} WHERE PostTypeId=@0", postTypeId).ConfigureAwait(false);

                if(taxonomies?.Count > 0)
                {
                    foreach (var tax in taxonomies)
                    {
                        tax.PostTypeId = postTypeId;
                    }

                    db.InsertBatch<PostTypeTaxonomy>(taxonomies);
                }

                cache.Remove(CacheHelper.SetCacheName(CacheNames.POST_TYPE_TAXONOMIES_BY_POST_TYPE, postTypeId));
                transation.Complete();
            }
        }
    }
}
