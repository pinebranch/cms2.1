﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Data.Domain
{
    [TableName(TableNames.TABLE_POST_TYPE)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class PostType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Rewrite { get; set; }
        public bool HasThumbnail { get; set; }
    }
}
