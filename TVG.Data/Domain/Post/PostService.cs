﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using TVG.Core.Caching;
using TVG.Core.Extensions;
using TVG.Core.Helpers;
using TVG.Data.Domain;
using TVG.Data.TvgConstants;

namespace TVG.Data.Services
{
    public class PostService : BaseService
    {
        public PostService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.POSTS;
        }

        public Task<Page<Post>> GetPostsAsync(PostQueryParameters parameters)
        {
            var keyTermIds = string.Join(";", parameters.TermIds);
            var key = CacheHelper.SetCacheName(CacheNames.POST_ALL,
                parameters.PostType, keyTermIds, parameters.Keyword, parameters.PublishStatus,
                parameters.CreatedBy, parameters.IsDeleted, parameters.PageIndex, parameters.PageSize);
            var group = CacheNames.POST_ALL_GROUP;
            return cache.GetOrAddAsync(group, key, () => {

                var cols = new StringBuilder(@"p.Id, p.PostType, p.ParentId, p.Title, p.Slug, p.Description, p.MetaDescription,
                            p.MetaKeyword, p.Image, p.CreatedBy, p.CreatedDate, p.UpdatedBy, p.UpdatedDate,
                            p.PublishStatus, p.IsDeleted");
                cols.Append(",u.UserName AS CreatedUserName");
                cols.Append(", u2.UserName AS UpdatedUserName");
                if(!string.IsNullOrEmpty(parameters.Keyword))
                {
                    cols.Append(",").Append(SqlHelper.GenerateMySqlFtsRankColumn("p.Title", parameters.Keyword, "Rank"));
                }

                var sql = new Sql();
                sql.Select(cols);
                sql.From($"{TableNames.TABLE_POST} p");
                sql.LeftJoin($"{TableNames.TABLE_USERS} u").On("p.CreatedBy = u.Id");
                sql.LeftJoin($"{TableNames.TABLE_USERS} u2").On("p.UpdatedBy = u2.Id");
                sql.LeftJoinIf(parameters.TermIds.Count > 0, $"{TableNames.TABLE_POST_TERM} pt", "p.Id = pt.PostId");

                sql.Where("p.PostType=@0", parameters.PostType);
                sql.WhereInInt("pt.TermId = @0", parameters.TermIds);
                sql.WhereIf(!string.IsNullOrEmpty(parameters.Keyword), SqlHelper.GenerateMySqlFtsWhere("p.Title", parameters.Keyword));
                sql.WhereIf(parameters.PublishStatus > -1, "p.PublishStatus = @0", parameters.PublishStatus);
                sql.WhereIf(parameters.CreatedBy > 0, "p.CreatedBy = @0", parameters.CreatedBy);
                sql.Where("p.IsDeleted = @0", parameters.IsDeleted);

                var orderBy = string.IsNullOrEmpty(parameters.Keyword) ? "p.Id DESC" : "Rank DESC";
                sql.OrderBy(orderBy);

                return db.PageAsync<Post>(parameters.PageIndex, parameters.PageSize, sql);
            }, CacheHelper.CacheDurationMedium);
        }

        public Task<Post> GetPostByIdAsync(int id)
        {
            if (id <= 0) return Task.FromResult<Post>(null);
            var key = CacheHelper.SetCacheName(CacheNames.POST_BY_ID, id);
            var group = CacheHelper.SetCacheName(CacheNames.POST_GROUP_BY_ID, id);
            return cache.GetOrAddAsync(group, key, () => {
                return db.SingleOrDefaultByIdAsync<Post>(id);
            }, CacheHelper.CacheDurationMedium);
        }

        public async Task<Post> GetPostBySlugAsync(string slug)
        {
            if (string.IsNullOrEmpty(slug)) return null;
            var key = CacheHelper.SetCacheName(CacheNames.POST_BY_SLUG, slug);
            var post = await cache.GetOrAddAsync("", key, () => {
                return db.FirstOrDefaultAsync<Post>("WHERE Slug=@0", slug);
            }, CacheHelper.CacheDurationMedium).ConfigureAwait(false);

            if(post != null)
            {
                var group = CacheHelper.SetCacheName(CacheNames.POST_GROUP_BY_ID, post.Id);
                cache.AddKeyToGroup(group, key, CacheHelper.CacheDurationMedium);
            }

            return post;
        }

        public async Task<int> InsertPostAsync(Post obj)
        {
            if (obj == null) return -1;
            await db.InsertAsync(obj).ConfigureAwait(false);
            AfterModifyPost(obj);
            return obj.Id;
        }

        public async Task<int> UpdatePostAsync(Post obj)
        {
            if (obj == null) return -1;
            var result = await db.UpdateAsync(obj).ConfigureAwait(false);
            if(result > 0)
            {
                AfterModifyPost(obj);
            }
            return result;
        }

        public async Task<int> UpdatePostStatusAsync(Post obj, int publishStatus)
        {
            if (obj == null) return -1;
            var result = await db.ExecuteAsync($"UPDATE {TableNames.TABLE_POST} SET PublishStatus=@0 WHERE Id=@1", publishStatus, obj.Id).ConfigureAwait(false);
            if(result > 0)
            {
                AfterModifyPost(obj);
            }
            return result;
        }

        public async Task<int> RestorePostAsync(Post obj)
        {
            //TODO: update term count
            if (obj == null) return -1;
            var result = await db.ExecuteAsync($"UPDATE {TableNames.TABLE_POST} SET IsDeleted=@0 WHERE Id=@1", false, obj.Id).ConfigureAwait(false);
            if (result > 0)
            {
                AfterModifyPost(obj);
            }
            return result;
        }

        public async Task<int> DeletePostAsync(int id, bool isPermanent = false)
        {
            var obj = await GetPostByIdAsync(id).ConfigureAwait(false);
            if (obj == null) return -1;

            using(var transaction = db.GetTransaction())
            {
                if (!isPermanent)
                {
                    await db.ExecuteAsync($"UPDATE {TableNames.TABLE_POST} SET IsDeleted=@0 WHERE Id=@1", true, obj.Id).ConfigureAwait(false);
                }
                else
                {
                    //TODO: Update term count
                    await db.DeleteAsync(obj).ConfigureAwait(false);
                }
                AfterModifyPost(obj);
                transaction.Complete();
            }

            return 1;
        }

        private void AfterModifyPost(Post obj)
        {
            cache.RemoveByGroup(CacheNames.POST_ALL_GROUP);
            cache.RemoveByGroup(CacheHelper.SetCacheName(CacheNames.POST_GROUP_BY_ID, obj.Id));
        }
    }
}
