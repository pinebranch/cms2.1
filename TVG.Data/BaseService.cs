﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Core.Caching;

namespace TVG.Data
{
    public class BaseService
    {
        protected IDatabase db;
        protected ICacheManager cache;
        protected int cacheDatabaseNumber;
    }
}
