﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVG.Admin.Data
{
    public static class SectionNames
    {
        public const string STYLES = "Styles";
        public const string HEADER_SCRIPTS = "HeaderScripts";
        public const string SCRIPTS = "Scripts";
    }
}
