﻿$(document).ready(function () {
    var loc = window.location.href;
    var $menuLinks = $('.menu-link').each(function (elem) {
        var $this = $(this),
            href = $this.attr('href');
        if (href.length > 1 && href !== "#" && loc.includes($this.attr('href'))) {
            $this.addClass("active");
            $this.parent().addClass("active");
            $this.parent().parent().prev('.main-menu-link').addClass('active subdrop');
            $this.parent().parent().show();
        }        
    });
    
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

var CMS = {};

CMS.UI = (function () {
    var obj = {};

    obj.bindOpenMediaPopup = function (selector, target, callback) {
        $(selector).on('click', function (e) {
            e.preventDefault();
            if (typeof window.puMedia === 'undefined' || window.puMedia.closed) {
                window.puMedia = $.popupWindow("/media/window/?target=" + target, {
                    onFileSelected: callback
                });
            } else {
                window.puMedia.focus();
            }
        });
    };

    obj.getThumbnailUrl = function (image) {
        if (image.length === 0) {
            return "/images/placeholder.jpg";
        }
        return '/uploads/' + image;
    };

    return obj;
})();

CMS.Utility = (function () {
    var obj = {};

    var stringIsEmpty = function(str) {
        return (!str || 0 === str.length);
    };

    var formatDateVN = function (date) {
        return moment(date).format('DD/MM/YYYY HH:mm');
    };

    var truncateString = function (str, n, useWordBoundary) {
        var isTooLong = str.length > n,
            s_ = isTooLong ? str.substr(0, n - 1) : str;
        useWordBoundary = useWordBoundary ? useWordBoundary : true;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return isTooLong ? s_ + '&hellip;' : s_;
    };

    var slugify = function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|;|“|”|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
        str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
        str = str.replace(/^\-+|\-+$/g, "");//cắt bỏ ký tự - ở đầu và cuối chuỗi

        return str;
    };

    return {
        formatDateVN: formatDateVN,
        truncateString: truncateString,
        slugify: slugify,
        stringIsEmpty: stringIsEmpty
    };
})();