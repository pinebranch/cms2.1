﻿import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ReactTable from "react-table";
import TermForm from "./form";

const TermManager = () => 
    <Router>
        <div>
            <Route exact path="/term/" component={TermList} />
            <Route path="/term/:action/:id?" render={props => <TermForm taxonomyName={document.getElementById('hfTaxonomy').value} {...props} />} /> 
        </div>
    </Router>
;

class TermList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            pages: null,
            loading: true,
            page: 0,
            pageSize: 10,
            taxonomy: {}
        };
     
        this.getData = this.getData.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });

        var pid = state.page + 1,
            pagesize = state.pageSize;
        this.getList(pid, pagesize);
    }

    getList(pid, pagesize) {
        var app = this;
        var type = $('#hfTaxonomy').val();
        $.get('/term/ajaxgetterms/', { type: type, pid: pid, pagesize: pagesize }).then(function (data) {
            app.setState({
                list: data.Items,
                pages: data.TotalPages,
                loading: false,
                page: pid,
                taxonomy: data.Taxonomy
            });
        });
    }

    handleDelete(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn xóa đối tượng này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/term/ajaxDeleteTerm/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList(app.state.page, app.state.pageSize);
            });
        }
    }

    render() {
        const { list, pages, loading, taxonomy } = this.state;
        var type = $('#hfTaxonomy').val();
        const cols = [
            {
                Header: 'Tên',
                id: 'name',
                accessor: x =>
                    <div>
                        {taxonomy.HasThumbnail && !CMS.Utility.stringIsEmpty(x.Image) && <img src={CMS.UI.getThumbnailUrl(x.Image)} style={{ maxWidth: '50px' }} className="mr-2 d-inline-block" />}
                        {taxonomy.HasColor && !CMS.Utility.stringIsEmpty(x.Color)
                            ? <span className="p-1 text-white d-inline-block" style={{ backgroundColor: x.Color }}>{x.Path.length > 0 ? x.Path.join(' » ') : x.Name}</span>
                            : <span>{x.Path.length > 0 ? x.Path.join(' » ') : x.Name}</span>
                        }                        
                    </div>
            },
            { Header: 'Url', id: 'slug', accessor: "Slug" },
            { Header: 'Thứ tự', accessor: 'DisplayOrder' },
            {
                Header: 'Thao tác',
                id: 'actions',
                accessor: x => 
                    <div>
                        <Link to={`edit/${x.Id}/?type=${x.Taxonomy}`} className="mr-3"><i className="fa fa-pencil" /> Sửa</Link>
                        <a href="#" onClick={this.handleDelete.bind(this, x.Id)}><i className="fa fa-remove" /> Xóa </a>
                    </div>
            }
        ];

        return (
            <div>
                <div className="text-right mb-3">
                    <Link className="btn btn-primary btn-sm" to={`/term/create/?type=${type}`}><i className="fa fa-plus" /> Tạo mới</Link>
                </div>
                <ReactTable
                    manual
                    data={list}
                    pages={pages}
                    loading={this.state.loading}
                    columns={cols}
                    onFetchData={this.getData}
                    defaultPageSize={this.state.pageSize}
                    showPageSizeOptions={false}
                    minRows={5}
                    onPageChange={page => this.setState({ page })}
                />
            </div>
        );
    }
}

ReactDOM.render(
    <TermManager />
, document.getElementById('app'));