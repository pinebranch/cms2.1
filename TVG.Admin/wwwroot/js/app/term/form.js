﻿import { Link, withRouter } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage, FieldArray, withFormik } from 'formik';
import * as Yup from 'yup';
import LoadingIndicator from './../components/loading-indicator.js';

class TermForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            taxonomy: {},
            id: this.props.match.params.id,
            action: this.props.match.params.action,
            item: null,
            terms: [],
            redirectToList: this.props.match.params.action === "create"
        };
    }

    componentDidMount() {
        this.getItem(this.props.match.params.id);
    }

    getItem(id) {
        $.get('/term/AjaxGetTerm/', { taxonomy: this.props.taxonomyName, id: id }).done(function (data) {
            this.setState({
                item: data.Item,
                terms: data.Terms,
                taxonomy: data.Taxonomy
            });
        }.bind(this));
    }

    render() {
        if (this.state.item) {
            return <FormikTerm item={this.state.item} terms={this.state.terms} taxonomy={this.state.taxonomy} action={this.state.action} redirectToList={this.state.redirectToList} />;
        } else {
            return <LoadingIndicator />;
        }
    }
}


class FrmTerm extends React.Component {

    constructor(props) {
        super(props);

        this.handleDeleteImage = this.handleDeleteImage.bind(this);
    }

    componentDidMount() {
        var app = this;
        CMS.UI.bindOpenMediaPopup('.js-open-pu', '#img', function (e) {
            $("#img").attr('src', e.files[0].url);
            console.log(e.files);
            app.props.setFieldValue('Image', e.files[0].path);
            window.puMedia.close();
        });

        $('#colorpicker').colorpicker({ format: 'hex', align: 'left', useAlpha: false })
            .on('colorpickerChange', function (e) {
                app.props.setFieldValue('Color', e.value.toHexString());
                console.log(app.props.values.Color);
            });
    }

    handleDeleteImage() {
        $("#img").attr('src', CMS.UI.getThumbnailUrl(""));
        app.props.setFieldValue('Image', "");
    }

    render() {
        let { values, terms, taxonomy, errors, touched, isSubmitting } = this.props;
        let image = values.Image;
        return (
            <Form id="form1" autoComplete="off">
                <ul className="nav nav-tabs" id="formTab" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="tab-info" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Thông tin</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="tab-seo" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                    </li>
                    {(taxonomy.HasThumbnail || taxonomy.HasColor) &&
                        <li className="nav-item">
                            <a className="nav-link" id="tab-thumbnail" data-toggle="tab" href="#thumbnail" role="tab" aria-controls="thumbnail" aria-selected="false">Hình ảnh</a>
                        </li>
                    }
                </ul>
                <div className="tab-content p-3" id="formTabContent">
                    {/*tab info*/}
                    <div className="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="tab-info">
                        <input type="hidden" name="Id" value={values.Id} />
                        <input type="hidden" name="FormAction" value={values.FormAction} />
                        <input type="hidden" name="Taxonomy" value={values.Taxonomy} />
                        <div className="form-group row">
                            <label className="col-md-2 col-form-label">Tên (*)</label>
                            <div className="col-md-10">
                                <Field type="text" name="Name" className="form-control" placeholder="Nhập tên đối tượng" />
                                <ErrorMessage name="Name" render={msg => <div className="invalid-input">{msg}</div>} />
                            </div>
                        </div>

                        {values.FormAction === "edit" &&
                            <div className="form-group row">
                                <label className="col-md-2 col-form-label">Đường dẫn tĩnh (*)</label>
                                <div className="col-md-10">
                                    <Field type="text" name="Slug" className="form-control" placeholder="Nhập đường dẫn tĩnh (vd: duong-dan-tinh)" />
                                    <small className={`fc-length mt-2 mr-1 ${values.Slug.length > 100 ? 'bg-danger text-white' : ''}`}>{values.Slug.length} ký tự </small><small><i className="fa fa-exclamation-circle" /> Thay đổi đường dẫn tĩnh có thể ảnh hưởng tới SEO hoặc không tìm thấy bài viết</small>
                                    <ErrorMessage name="Slug" render={msg => <div className="invalid-input">{msg}</div>} />
                                </div>
                            </div>
                        }
                        <div className="form-group row">
                            <label className="col-md-2 col-form-label">Mô tả</label>
                            <div className="col-md-10">
                                <Field component="textarea" name="Description" className="form-control" placeholder="Mô tả ngắn về đối tượng" />
                            </div>
                        </div>

                        {taxonomy.IsHierarchy &&
                            <div className="form-group row">
                                <label className="col-md-2 col-form-label">Thuộc đối tượng</label>
                                <div className="col-md-10">
                                    <Field component="select" name="ParentId" className="form-control">
                                        <option value="0">-- Chọn đối tượng cấp trên --</option>
                                        {terms.map(x =>
                                            <option key={x.Id} value={x.Id}>{x.Path.length > 0 ? x.Path.join(' / ') : x.Name}</option>)}
                                    </Field>
                                </div>
                            </div>
                        }

                        <div className="form-group row">
                            <label className="col-md-2 col-form-label">Thứ tự</label>
                            <div className="col-md-10">
                                <Field type="number" name="DisplayOrder" className="form-control" />
                            </div>
                        </div>
                    </div>

                    {/*tab seo*/}
                    <div className="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="tab-seo">
                        <div className="form-group row">
                            <label className="col-md-2 col-form-label">Meta description</label>
                            <div className="col-md-10">
                                <Field component="textarea" name="MetaDescription" className="form-control fc-check-length" />
                                <small className={`fc-length mt-2 mr-1 ${values.MetaDescription.length > 130 ? 'bg-danger text-white' : ''}`}>{values.MetaDescription.length} ký tự</small>
                                <small className="text-muted">Meta Description chỉ nên từ <strong className="text-danger">110-130</strong> ký tự</small>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-md-2 col-form-label">Meta keyword</label>
                            <div className="col-md-10">
                                <Field component="textarea" name="MetaKeyword" className="form-control" />
                            </div>
                        </div>
                    </div>

                    {/*tab thumbnail*/}
                    {(taxonomy.HasThumbnail || taxonomy.HasColor) &&
                        <div className="tab-pane fade" id="thumbnail" role="tabpanel" aria-labelledby="tab-thumbnail">
                            {taxonomy.HasThumbnail &&
                                <div className="form-group row">
                                    <label className="col-md-2 col-form-label">Hình đại diện</label>
                                    <div className="col-md-10">
                                        <img id="img" src={CMS.UI.getThumbnailUrl(values.Image)} className="img-thumbnail d-block mb-1" style={{ maxWidth: '200px' }} />
                                        <button type="button" className="btn btn-primary js-open-pu mr-1">Chọn hình</button>
                                        <button type="button" className="btn btn-secondary" onClick={this.handleDeleteImage}>Xóa hình</button>
                                        <Field type="hidden" name="Image" id="image" />
                                    </div>
                                </div>
                            }
                            {
                                taxonomy.HasColor &&
                                <div className="form-group row">
                                    <label className="col-md-2 col-form-label">Màu sắc</label>
                                    <div className="col-md-10">
                                        <div>
                                            <input id="colorpicker" type="text" className="form-control" value={values.Color} placeholder="Nhấn để chọn màu" />
                                            <small className="form-text">Chọn màu đánh dấu cho đối tượng, ví dụ <span className="d-inline-block p-1 text-white" style={values.Color !== "" ? { backgroundColor: values.Color } : { backgroundColor: '#155724' }}>{values.Name}</span></small>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    }
                </div>

                <div className="form-group text-center">
                    <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
                    <Link to={`/term/?type=${values.Taxonomy}`} className="btn btn-secondary">Quay lại</Link>
                </div>
            </Form>
        );
    }
}

const FormikTerm = withRouter(withFormik({
    mapPropsToValues({ item, terms, taxonomy, action }) {
        return {
            Id: item.Id || 0,
            Taxonomy: item.Taxonomy || taxonomy.Name,
            ParentId: item.ParentId || 0,
            Name: item.Name || "",
            Slug: item.Slug || "",
            Description: item.Description || "",
            MetaDescription: item.MetaDescription || "",
            MetaKeyword: item.MetaKeyword || "",
            Image: item.Image || "",
            DisplayOrder: item.DisplayOrder || 0,
            Color: item.Color || "",
            FormAction: action
        };
    },
    validationSchema: Yup.object().shape({
        Name: Yup.string().required('Vui lòng nhập tên'),
        Slug: Yup.string().max(100, 'Đường dẫn tĩnh không được quá ${max} ký tự')
    }),
    handleSubmit(values, { resetForm, setErrors, setSubmitting, props }) {
        const { history } = props;
        var token = $('#crsf').find('input[name="f"]').val();
        var image = $('#image').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/term/ajaxSaveTerm/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);
            if (props.redirectToList) {
                history.push('/term/?type=' + props.taxonomy.Name);
            }
        });
    }
})(FrmTerm));

export default TermForm;