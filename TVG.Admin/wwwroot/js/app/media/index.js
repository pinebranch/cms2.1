﻿
import FileUploader from './file-uploader-tmpl';

var onFileSelected = window.onFileSelected || function (event, file, target) { };

class MediaManager extends React.Component {
    constructor(props) {
        super(props);

        let selectedPath = localStorage.getItem('media_selected_path') || '';

        this.state = {
            isInit: false,
            directories: [],
            files: [],
            selectedFolderPath: "",
            filesCopyOrCut: [],
            copyAction: '',
            isUploaderLoaded: false,
            uploader: {},
            view: "list"
        };

        this.getDirectoryContents = this.getDirectoryContents.bind(this);
        this.handleDblClickDirectory = this.handleDblClickDirectory.bind(this);
        this.makeTree = this.makeTree.bind(this);

        this.handleChangeTree = this.handleChangeTree.bind(this);
        this.handleCheckFile = this.handleCheckFile.bind(this);

        this.handleCmd_Home = this.handleCmd_Home.bind(this);
        this.handleCmd_Up = this.handleCmd_Up.bind(this);
        this.handleCmd_NewFolder = this.handleCmd_NewFolder.bind(this);
        this.handleCreateFolder = this.handleCreateFolder.bind(this);
        this.handleCmd_Delete = this.handleCmd_Delete.bind(this);
        this.handleCmd_Copy = this.handleCmd_Copy.bind(this);
        this.handleCmd_Cut = this.handleCmd_Cut.bind(this);
        this.handleCmd_Paste = this.handleCmd_Paste.bind(this);
        this.handleCmd_Upload = this.handleCmd_Upload.bind(this);
        this.handleCmd_View = this.handleCmd_View.bind(this);
        this.handleCmd_Select = this.handleCmd_Select.bind(this);

        this.prepareCopy = this.prepareCopy.bind(this);
    }

    componentDidMount() {
        this.getDirectoryContents('');
        //this.getRootDirectoryTree(this.state.selectedFolderPath);
        $.contextMenu({
            selector: ".file-item__img",
            items: {
                view_image: {
                    name: "Xem hình lớn",
                    icon: "fa-picture-o",
                    callback: function (key, opt, e) {
                        $(this).ekkoLightbox();
                    }
                }
            }
        });
    }

    getRootDirectoryTree(path) {
        path = path || '/';
        var $tree = $(this.treeContainer);
        $.get('/media/GetDirectoryContents/', { path: '/' }).done(function (data) {
            var directories = data.Directories;
            this.setState({
                directories: directories
            });
            this.makeTree(directories);
            if (path !== '/') {
                $tree.jstree('deselect_all', true);
                $tree.jstree(true).select_node(path);
            }            
        }.bind(this));
    }

    getDirectoryContents(path, shouldMakeTree = true) {

        path = path || '/';

        $.get('/media/GetDirectoryContents/', { path: path }).done(function (data) {
            var directories = data.Directories;
            this.setState({
                directories: directories,
                files: data.Files
            });
            if (shouldMakeTree) {
                this.makeTree(directories);
            }
        }.bind(this));
    }

    makeTree(directories) {
        var parentId = $(this.treeContainer).jstree('get_selected');

        var $tree = $(this.treeContainer);
        var $selected = $tree.jstree('get_selected', true)[0];
       
        var treeDir = directories.map(x => {
            var obj = {
                text: x.Name
            };
            return obj;
        });

        if (!this.state.isInit) {
            var rootTree = {
                text: "/",
                state: { opened: true, selected: true },
                children: treeDir
            };

            $tree.jstree({
                core: {
                    data: rootTree,
                    multiple: false,
                    check_callback: true
                }
            });
            $tree.on('changed.jstree', (e, data) => {
                this.handleChangeTree(e, data);
            });
            this.setState({ isInit: true });
        }
        else {

            var parent = $tree.jstree('get_selected', true);
            var children = $selected.children;
            $tree.jstree(true).delete_node(children);

            treeDir.forEach(function (item, index) {
                var id = $tree.jstree('create_node', $selected, item, 'last', false, false);
            });
            $tree.jstree('open_node', $selected);
        }
    }

    handleDblClickDirectory(path) {
        var app = this;
        var $tree = $(this.treeContainer);
        var selectedNode = $tree.jstree(true).get_selected()[0];
        var children = $tree.jstree(true).get_children_dom(selectedNode);
        if (children.length > 0) {
            for (var i = 0, length = children.length; i < length; i++) {
                if (children[i].innerText === path) {
                    $tree.jstree('deselect_all', true);
                    $tree.jstree(true).select_node(children[i].id);
                    break;
                }
            }
        }
    }

    handleChangeTree(e, data) {
        var $tree = $(this.treeContainer);
        var path = $tree.jstree(true).get_path(data.node, '/');

        localStorage.setItem('media_selected_path', path.slice(1));
        this.setState({ selectedFolderPath: path.slice(1) });
        if (data.node) {
            this.getDirectoryContents(path);
        }
    }

    handleCheckFile(event) {
        var $fileItem = $(event.target).closest('.file-item');
        $fileItem.toggleClass("file-item__is-selected");
    }

    handleCmd_Home() {
        $(this.treeContainer).jstree('deselect_all', true);
        $(this.treeContainer).jstree('select_node', 'j1_1');
    }

    handleCmd_Up() {
        var $tree = $(this.treeContainer);
        var $selected = $tree.jstree(true).get_selected(true)[0];
        var parentId = $selected.parent;
        if (parentId !== '#') {
            $tree.jstree(true).deselect_all(true);
            $tree.jstree(true).select_node(parentId);
        }
    }

    handleCmd_NewFolder() {
        var $modal = $(this.modalNewFolder);
        $modal.modal().on('shown.bs.modal', function () {
            $('input:visible:enabled:first', this).focus();
        }).on('hidden.bs.modal', function () {
            $(this).find('form')[0].reset();
        });
    }

    handleCreateFolder(e) {
        e.preventDefault();
        var $modal = $(this.modalNewFolder);
        var app = this;
        var data = {
            f: $('#crsf').find('input[name="f"]').val(),
            newFolder: $modal.find('input[name="newFolder"]').val(),
            path: app.state.selectedFolderPath
        };

        $.post('/media/createFolder/', data).done(function (data) {
            toastr[data.status](data.message);
            $modal.modal('hide');
            app.getDirectoryContents(app.state.selectedFolderPath, false);
        });
    }

    handleCmd_Delete(e) {
        var app = this,
            path = this.state.selectedFolderPath,
            token = $('#crsf').find('input[name="f"]').val();

        swal({
            title: "Bạn có chắc muốn xóa các thư mục / file được chọn?",
            text: "Lưu ý: thư mục và file bị xóa không thể khôi phục được.",
            icon: "warning",
            buttons: ["Không", "Đồng ý"],
            dangerMode: true
        }).then((willDelete) => {
            if (willDelete) {
                var selectedFileNames = [];
                $(app.fileContainter).find('.file-item__is-selected').each(function () {
                    selectedFileNames.push($(this).attr('data-name'));
                });
                var dataPost = {
                    f: token,
                    path: path,
                    files: selectedFileNames
                };
                $.post('/media/deleteFiles/', dataPost).done(function (data) {
                    toastr[data.status](data.message);
                    app.getDirectoryContents(app.state.selectedFolderPath, true);
                });
            }
        });
    }

    handleCmd_Copy() {
        this.prepareCopy('copy');
    }

    handleCmd_Cut() {
        this.prepareCopy('cut');
    }

    handleCmd_Paste() {
        var app = this,
            token = $('#crsf').find('input[name="f"]').val(),
            action = this.state.copyAction,
            $toolbar = $(this.toolbar);
        var dataPost = {
            f: token,
            path: this.state.selectedFolderPath,
            files: this.state.filesCopyOrCut,
            action: this.state.copyAction
        };
        $.post('/media/pasteFiles/', dataPost).done(function (data) {
            toastr[data.status](data.message);
            app.getDirectoryContents(app.state.selectedFolderPath, true);
            app.setState({ filesCopyOrCut: [], copyAction: '' });
        });
    }

    handleCmd_Upload(e) {
        var $modal = $(this.mdFileUpload);
        $modal.modal().on('hidden.bs.modal', function () {
            $(this).find('.qq-upload-success').remove();
        });
        var app = this,
            token = $('#crsf').find('input[name="f"]').val();
        if (!this.state.isUploaderLoaded) {
            var uploader = new qq.FineUploader({
                element: document.getElementById("uploader"),
                request: {
                    endpoint: '/media/upload/',
                    params: {
                        f: token,
                        path: this.state.selectedFolderPath
                    }
                },
                autoUpload: false,
                validation: {
                    allowedExtensions: ['jpeg', 'jpg', 'bmp', 'png', 'gif', 'doc', 'docx', 'pdf', 'txt']
                },
                callbacks: {
                    onAllComplete: function () {
                        app.getDirectoryContents(app.state.selectedFolderPath, true);
                        toastr.success("Các files đã được upload");
                    }
                }
            });

            qq(document.getElementById("trigger-upload")).attach("click", function () {
                uploader.uploadStoredFiles();
            });

            this.setState({ isUploaderLoaded: true, uploader: uploader });
        }
        else {
            var currentUploader = this.state.uploader;
            currentUploader.setParams({ path: this.state.selectedFolderPath });
        }
    }

    handleCmd_View(type) {
        this.setState({ view: type });
    }

    handleCmd_Select() {
        var files = [];
        var target = $('#hfTarget');
        var path = this.state.selectedFolderPath;
        $(this.fileContainter).find('.file-item__file.file-item__is-selected').each(function () {
            files.push({
                path: path + '/' + $(this).attr('data-name'),
                url: $(this).attr('data-url')
            });
        });

        this.props.onFileSelected({ files: files, target: target });
    }

    prepareCopy(action) {
        var $toolbar = $(this.toolbar);
        var path = this.state.selectedFolderPath;
        var selectedFileNames = [];
        $(this.fileContainter).find('.file-item__is-selected').each(function () {
            selectedFileNames.push(path + '/' + $(this).attr('data-name'));
        });
        console.log(selectedFileNames);
        this.setState({ filesCopyOrCut: selectedFileNames, copyAction: action });
        toastr["success"]('Thư mục và files đã được copy hoặc cắt. Vui lòng chọn thư mục để dán.');
    }

    formatBytes(a, b) { if (0 === a) return "0 Bytes"; var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], f = Math.floor(Math.log(a) / Math.log(c)); return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f]; }

    render() {
        var { directories, files, view, copyAction } = this.state;
        
        return (
            <div>
                <div className="file-manager">
                    <div className="row file-manager__toolbar m-0" ref={el => this.toolbar = el}>
                        <div className="col-md-12 file-manager__commands">
                            <button type="button" className="file-manager__cmd btn btn-outline-primary mr-1" data-toggle="tooltip" data-original-title="Về thư mục gốc" onClick={this.handleCmd_Home}>
                                <i className="fa fa-home" />
                            </button>
                            <button type="button" className="file-manager__cmd btn btn-outline-primary mr-1" data-toggle="tooltip" data-original-title="Quay lại một cấp" onClick={this.handleCmd_Up}>
                                <i className="fa fa-level-up" />
                            </button>
                            <button type="button" className="file-manager__cmd btn btn-outline-success mr-1" data-toggle="tooltip" data-original-title="Upload" onClick={this.handleCmd_Upload}>
                                <i className="fa fa-upload" />
                            </button>
                            <button type="button" className="file-manager__cmd btn btn-outline-primary mr-1" data-toggle="tooltip" data-original-title="Tạo thư mục" onClick={this.handleCmd_NewFolder}>
                                <i className="fa fa-folder" />
                            </button>
                            <button type="button" className="file-manager__cmd btn btn-outline-danger mr-1" data-toggle="tooltip" data-original-title="Xóa thư mục/file">
                                <i className="fa fa-trash-o" onClick={this.handleCmd_Delete} />
                            </button>
                            <button type="button" className={`file-manager__cmd btn mr-1 js-fm-copy ${copyAction === 'copy' ? 'btn-primary' : 'btn-outline-primary'}`} data-toggle="tooltip" data-original-title="Copy">
                                <i className="fa fa-copy" onClick={this.handleCmd_Copy} />
                            </button>
                            <button type="button" className={`file-manager__cmd mr-1 js-fm-cut btn ${copyAction === 'cut' ? 'btn-primary' : 'btn-outline-primary'}`} data-toggle="tooltip" data-original-title="Cut">
                                <i className="fa fa-cut" onClick={this.handleCmd_Cut} />
                            </button>
                            <button type="button" className={`file-manager__cmd btn  mr-1 js-fm-paste ${copyAction !== '' ? 'btn-success' : 'btn-outline-success disabled'}`} data-toggle="tooltip" data-original-title="Dán" onClick={this.handleCmd_Paste} >
                                <i className="fa fa-paste" />
                            </button>
                            <button type="button" className="file-manager__cmd btn btn-primary mr-3" data-toggle="tooltip" data-original-title="Chèn file được chọn" onClick={this.handleCmd_Select}>
                                <i className="fa fa-hand-pointer-o" />
                            </button>

                            <button type="button" className="file-manager__cmd btn btn-outline-primary mr-1" data-toggle="tooltip" data-original-title="Xem dạng list" onClick={this.handleCmd_View.bind(this, "list")}>
                                <i className="fa fa-list" />
                            </button>
                            <button type="button" className="file-manager__cmd btn btn-outline-primary mr-3" data-toggle="tooltip" data-original-title="Xem dạng lưới" onClick={this.handleCmd_View.bind(this, "grid")}>
                                <i className="fa fa-th" />
                            </button>
                        </div>
                    </div>
                    <div className="row m-0">
                        <div className="col-md-3 pr-0 file-manager__folder">
                            <div id="jsTree" ref={div => this.treeContainer = div} />
                        </div>
                        <div className={`col-md-9 pl-0 file-manager__file-list file-manager__view-${view}`} ref={el => this.fileContainter = el}>
                            
                            <div className="row file-list__header ml-0">
                                <div className="d-inline col-md-6 file-list__header-title">Tên</div>
                                <div className="d-inline col-md-4 file-list__header-title">Ngày cập nhật</div>
                                <div className="d-inline col-md-2 file-list__header-title">Dung lượng</div>
                            </div>
                            
                            <div className="file-list__files">

                                {directories.map(item => 
                                    <div key={item.Name} className="file-item row ml-0 file-item__directory" data-type="folder" data-name={item.Name}>
                                        <div className="file-item__name col-md-6" title={item.Name} onClick={this.handleCheckFile} onDoubleClick={this.handleDblClickDirectory.bind(this, item.Name)}>
                                            <div className="file-item__thumb">
                                                <a href="#"><i className="fa fa-folder" /></a>
                                            </div>
                                            <div className="file-item__file-name">{item.Name}</div>
                                        </div>
                                        <div className="file-item__date col-md-4">
                                            <span>{moment(item.LastModified).format("DD/MM/YYYY HH:mm:ss")}</span>
                                        </div>
                                        <div className="file-item__size col-md-2" />
                                    </div>
                                )}
                                {files.map(item =>
                                    <div key={item.Url} className="file-item row ml-0 file-item__file" data-type="file" data-name={item.Name} data-url={item.Url}>
                                        <div className="file-item__name col-md-6" title={item.Name} onClick={this.handleCheckFile}>
                                            <div className="file-item__thumb">
                                                <a href="#">
                                                    {item.Thumbnail.length > 0
                                                    ? <img className="file-item__img" src={item.Thumbnail} data-toggle="lightbox" data-remote={item.Url} />
                                                        : item.FileType !== 'file'
                                                                ? <i className={`fa fa-file-${item.FileType}-o`} />
                                                                : <i className="fa fa-file-o" />
                                                        
                                                    }
                                                </a>
                                            </div>
                                            <div className="file-item__file-name">{item.Name}</div>
                                        </div>
                                        <div className="file-item__date col-md-4">
                                            <span>{moment(item.LastModified).format("DD/MM/YYYY HH:mm:ss")}</span>
                                        </div>
                                        <div className="file-item__size col-md-2">
                                            <span>{this.formatBytes(item.Length)}</span>
                                        </div>
                                    </div>
                                )}
                            </div>

                        </div>
                    </div>
                </div> {/* end file-manager */}

                {/* modal New folder */}
                <div className="modal fade" ref={elem => this.modalNewFolder = elem}>
                    <div className="modal-dialog modal-sm modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Tạo thư mục mới</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form id="frmNewFolder">
                                    <div className="form-group">
                                        <small className="form-text text-muted">Vui lòng nhập tên thư mục cần tạo</small>
                                        <div className="input-group mb-3">
                                            <input type="text" className="form-control" name="newFolder" placeholder="New Folder" />
                                            <div className="input-group-append">
                                                <button className="btn btn-primary" onClick={this.handleCreateFolder}>Tạo</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> {/* end modal new folder */}

                {/* modal file upload */}
                <div className="modal fade" ref={elem => this.mdFileUpload = elem}>
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Upload file</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div id="uploader" />
                            </div>
                        </div>
                    </div>
                </div> {/* end modal file upload */}                

                 <FileUploader /> 
            </div>
        );
    }
}

ReactDOM.render((
    <MediaManager onFileSelected={onFileSelected} />
), document.getElementById('app'));