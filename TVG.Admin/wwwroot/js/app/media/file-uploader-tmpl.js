﻿export default class FileUploader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <script type="text/template" id="qq-template">
                <div className="qq-uploader-selector qq-uploader" qq-drop-area-text="Kéo thả file vào đây">
                    <div className="qq-total-progress-bar-container-selector qq-total-progress-bar-container progress mb-2">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" className="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar progress-bar bg-success" />
                    </div>
                    <div className="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone="true">
                        <span className="qq-upload-drop-area-text-selector" />
                    </div>
                    <div className="buttons mb-3">
                        <div className="qq-upload-button-selector qq-upload-button btn btn-primary mr-3">
                            <div>Chọn files</div>
                        </div>
                        <button type="button" id="trigger-upload" className="btn btn-success">
                            <i className="icon-upload icon-white" /> Upload
                        </button>
                    </div>
                    <span className="qq-drop-processing-selector qq-drop-processing">
                        <span>Processing dropped files...</span>
                        <span className="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                    </span>
                    <ul className="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                        <li>
                            <div className="qq-progress-bar-container-selector progress mb-2">
                                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" className="qq-progress-bar-selector qq-progress-bar progress-bar bg-success" />
                            </div>
                            <span className="qq-upload-spinner-selector qq-upload-spinner" />
                            <img className="qq-thumbnail-selector" qq-max-size="100" qq-server-scale="true" />
                            <div className="d-inline-block">
                                <div className="qq-edit-filename-wrapper">
                                    <span className="qq-upload-file-selector qq-upload-file" />
                                    <span className="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename" />
                                    <input className="qq-edit-filename-selector qq-edit-filename" tabIndex="0" type="text" />
                                </div>
                                <div className="qq-upload-size-selector qq-upload-size" />
                                <div className="mb-2">
                                    <button type="button" className="qq-btn qq-upload-cancel-selector qq-upload-cancel btn btn-sm btn-secondary">Hủy</button>
                                    <button type="button" className="qq-btn qq-upload-retry-selector qq-upload-retry btn btn-sm btn-primary">Thử lại</button>
                                    <button type="button" className="qq-btn qq-upload-delete-selector qq-upload-delete btn btn-sm btn-danger">Xóa</button>
                                </div>
                                <div role="status" className="qq-upload-status-text-selector qq-upload-status-text"/>
                            </div>
                            
                        </li>
                    </ul>

                    <dialog className="qq-alert-dialog-selector">
                        <div className="qq-dialog-message-selector"></div>
                        <div className="qq-dialog-buttons">
                            <button type="button" className="btn btn-sm btn-primary qq-cancel-button-selector">Close</button>
                        </div>
                    </dialog>

                    <dialog className="qq-confirm-dialog-selector">
                        <div className="qq-dialog-message-selector"></div>
                        <div className="qq-dialog-buttons">
                            <button type="button" className="qq-cancel-button-selector">No</button>
                            <button type="button" className="qq-ok-button-selector">Yes</button>
                        </div>
                    </dialog>

                    <dialog className="qq-prompt-dialog-selector">
                        <div className="qq-dialog-message-selector"></div>
                        <input type="text" />
                        <div className="qq-dialog-buttons">
                            <button type="button" className="qq-cancel-button-selector">Cancel</button>
                            <button type="button" className="qq-ok-button-selector">Ok</button>
                        </div>
                    </dialog>
                </div>
            </script >
        );
    }
}