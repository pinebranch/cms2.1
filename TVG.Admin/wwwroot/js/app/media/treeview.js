﻿class TreeView extends React.Component {
        
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { treeData } = this.props;
        if (treeData) {
            $(this.treeContainer).jstree(treeData);
            $(this.treeContainer).on('changed.jstree', (e, data) => {
                this.props.onChange(e, data);
            });
        }

        this.props.bindAddNode(this.addNode);
    }    

    shouldComponentUpdate(nextProps) {
        return nextProps.treeData !== this.props.treeData;
    }

    componentDidUpdate() {
        //const { treeData } = this.props;
        //if (treeData) {
        //    $(this.treeContainer).jstree(true).settings = treeData;
        //    $(this.treeContainer).jstree(true).refresh();

        //}
    }

    addNode(node) {
        console.log('add node');
        var position = 'inside';
        var parent = $(this.treeContainer).jstree('get_selected');

        $(this.treeContainer).jstree("create_node", parent, position, node, false, false);
    }

    render() {
        return (
            <div id="jsTree" ref={div => this.treeContainer = div} />
        );
    }
}

export default TreeView;