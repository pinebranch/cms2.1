﻿import { Link } from "react-router-dom";
import ReactTable from "react-table";
import { Formik, Field, Form, ErrorMessage, withFormik, FieldArray } from 'formik';
import * as Yup from 'yup';
import Select from 'react-select';
import Select2 from './../components/select2';

class PostTypeList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            loading: true,
            showModal: false,
            taxonomies: [],
            formItem: {},
            formItemTaxonomies: [],
            formStatusTag: 0,
            formPrimaryTaxonomy: 0
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEditItem = this.handleEditItem.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleSubmitted = this.handleSubmitted.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });
        this.getList();
    }

    getList() {
        var app = this;
        $.get('/posttype/ajaxGetPostTypes/').then(function (data) {
            app.setState({
                list: data.Items,
                taxonomies: data.Taxonomies,
                loading: false
            });
        });
    }

    handleDelete(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn xóa loại bài viết này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/posttype/ajaxDeletePostType/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList();
            });
        }
    }

    handleEditItem(item, event) {
        event.preventDefault();
        var app = this;
        $.get('/posttype/ajaxGetPostTypeTaxonomies/', { id: item.Id }).then(function (data) {
            app.setState({
                showModal: true,
                formItem: item,
                formItemTaxonomies: data.Items,
                formStatusTag: data.StatusTag,
                formPrimaryTaxonomy: data.PrimaryTaxonomy
            });
        });
    }

    handleSubmitted() {
        this.getList();
    }

    handleCloseModal() {
        this.setState({ showModal: false, formItem: {} });
        console.log("close");
    }

    render() {
        const { list, pages, loading } = this.state;

        const cols = [
            { Header: 'Tên loại', accessor: 'DisplayName' },
            { Header: 'Mã loại', accessor: 'Name' },
            { Header: 'Rewrite', accessor: 'Rewrite' },
            { Header: 'Hình đại diện', id: 'hasThumbnail', accessor: x => x.HasThumbnail && <i className="fa fa-check-circle text-success" /> },
            {
                Header: 'Thao tác',
                id: 'actions',
                accessor: item =>
                    <div>
                        <a href="#" onClick={this.handleEditItem.bind(this, item)} className="mr-3"><i className="fa fa-pencil" /> Sửa</a>
                        <a href="#" onClick={this.handleDelete.bind(this, item.Id)}><i className="fa fa-remove" /> Xóa </a>
                    </div>

            }
        ];

        return (
            <div>
                <div className="row mb-3">
                    <div className="col-md-12 text-right">
                        <a className="btn btn-primary btn-sm" href="#" onClick={this.handleEditItem.bind(this, {})}><i className="fa fa-plus" /> Tạo mới</a>
                    </div>
                </div>
                <ReactTable
                    manual
                    data={list}
                    loading={this.state.loading}
                    columns={cols}
                    onFetchData={this.getData}
                    showPageSizeOptions={false}
                    showPagination={false}
                    minRows={5}
                />

                {this.state.showModal
                    ? <PostTypeModal
                        onCloseModal={this.handleCloseModal}
                        onSubmitted={this.handleSubmitted}
                        item={this.state.formItem}
                        taxonomies={this.state.taxonomies}
                        itemTaxonomies={this.state.formItemTaxonomies}
                        statusTag={this.state.formStatusTag}
                        primaryTaxonomy={this.state.formPrimaryTaxonomy}
                      />
                    : null}
            </div>
        );
    }
}

class PostTypeModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            item: {}
        };

        this.handleSubmitted = this.handleSubmitted.bind(this);
    }

    componentDidMount() {
        const { onCloseModal } = this.props;
        $(this.modal).modal('show');
        $(this.modal).on('hidden.bs.modal', onCloseModal);
    }

    handleSubmitted() {

        $(this.modal).modal('hide');
        this.props.onSubmitted();
        this.props.onCloseModal();
    }

    render() {
        return (
            <div ref={modal => this.modal = modal} className="modal fade custom-modal show" id="mdPostTypeForm" tabIndex="-1" role="dialog" aria-labelledby="customModal" >
                <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Loại bài viết</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <FormikPostType
                                onSubmitted={this.handleSubmitted}
                                item={this.props.item}
                                taxonomies={this.props.taxonomies}
                                itemTaxonomies={this.props.itemTaxonomies}
                                statusTag={this.props.statusTag}
                                primaryTaxonomy={this.props.primaryTaxonomy}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class FrmPostType extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let { values, errors, touched, isSubmitting, itemTaxonomies, taxonomies } = this.props;
        let taxonomyList = taxonomies.map(x => { return { value: x.Id, label: x.DisplayName }; });
        let defaultValues = taxonomyList.filter(x => values.TaxonomyIds.includes(x.value));
 
        return (
            <Form>
                <ul className="nav nav-tabs" id="formTab" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="nav-tab-info" data-toggle="tab" href="#tab-info" role="tab" aria-controls="tab-info" aria-selected="true">Thông tin</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="nav-tab-taxonomies" data-toggle="tab" href="#tab-taxonomies" role="tab" aria-controls="tab-taxonomies" aria-selected="false">Phân loại</a>
                    </li>
                </ul>
                <div className="tab-content p-3" id="formTabContent">
                    { /*tab-info*/}
                    <div className="tab-pane fade show active" id="tab-info" role="tabpanel" aria-labelledby="nav-tab-info">
                        <input type="hidden" name="Id" value={values.Id} />
                        <div className="form-group">
                            <label>Tên loại bài viết</label>
                            <Field type="text" name="DisplayName" className="form-control" placeholder="Nhập tên loại bài viết" />
                            <ErrorMessage name="DisplayName" render={msg => <div className="invalid-input">{msg}</div>} />
                        </div>
                        <div className="form-group">
                            <label>Mã loại bài viết</label>
                            <Field type="text" name="Name" className="form-control" placeholder="Nhập mã loại bài viết (vd: loai-bai-viet)" />
                            <ErrorMessage name="Name" render={msg => <div className="invalid-input">{msg}</div>} />
                        </div>
                        <div className="form-group">
                            <label>Rewrite</label>
                            <Field type="text" name="Rewrite" className="form-control" placeholder="Nhập url rewrite (vd: loai-bai-viet)" />
                            <ErrorMessage name="Rewrite" render={msg => <div className="invalid-input">{msg}</div>} />
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <label>
                                    <Field name="HasThumbnail" type="checkbox" value="true" checked={values.HasThumbnail} />
                                    <span className="label-text">Có hình đại diện</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    { /*tab-taxonomies*/}
                    <div className="tab-pane fade" id="tab-taxonomies" role="tabpanel" aria-labelledby="nav-tab-taxonomies">
                        <div className="form-group">
                            <label>Chọn các phân loại áp dụng cho loại bài viết này</label>
                            <Select
                                defaultValue={defaultValues}
                                isMulti
                                name="TaxonomyIds"
                                options={taxonomyList}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                onChange={(option) => this.props.setFieldValue("TaxonomyIds", option.map(x => x.value))}
                            /> 
                        </div>
                        {values.TaxonomyIds.length > 0 &&
                            <div>
                                <div className="form-group">
                                    <label>Trong đó, chọn phân loại chính (dùng cho breadcrumb trong chi tiết bài viết)</label>
                                    <Field component="select" name="PrimaryTaxonomy" className="form-control">
                                        <option value="0">--- Chọn phân loại --- </option>
                                        {taxonomyList.map(item => <option key={item.value} value={item.value}>{item.label}</option>)}
                                    </Field>
                                </div>
                                <div className="form-group">
                                    <label>Chọn phân loại dùng làm trạng thái bài viết</label>
                                    <Field component="select" name="StatusTag" className="form-control">
                                        <option value="0">--- Chọn phân loại --- </option>
                                        {taxonomyList.map(item => <option key={item.value} value={item.value}>{item.label}</option>)}
                                    </Field>
                                </div>
                            </div>
                        }
                    </div>
                </div>

                
                <div className="form-group">
                    <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </Form>
        );
    }
}

const FormikPostType = withFormik({
    mapPropsToValues({ item, itemTaxonomies, statusTag, primaryTaxonomy }) {
        return {
            Id: item.Id || 0,
            Name: item.Name || '',
            DisplayName: item.DisplayName || '',
            Rewrite: item.Rewrite || '',
            HasThumbnail: item.HasThumbnail || false,            
            TaxonomyIds: itemTaxonomies.map(x => x.TaxonomyId),
            StatusTag: statusTag || 0,
            PrimaryTaxonomy: primaryTaxonomy || 0
        };
    },
    validationSchema: Yup.object().shape({
        Name: Yup.string().required('Vui lòng nhập mã loại bài viết'),
        DisplayName: Yup.string().required('Vui lòng nhập tên loại bài viết'),
        Rewrite: Yup.string().required('Vui lòng nhập url rewrite')
    }),
    handleSubmit(values, { props, resetForm, setErrors, setSubmitting }) {

        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/postType/ajaxSavePostType/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);
            props.onSubmitted();
        });
    }
})(FrmPostType);

ReactDOM.render(<PostTypeList />, document.getElementById('app'));