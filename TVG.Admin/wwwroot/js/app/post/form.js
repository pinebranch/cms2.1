﻿import { Link, withRouter } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage, FieldArray, withFormik } from 'formik';
import * as Yup from 'yup';
import LoadingIndicator from './../components/loading-indicator.js';
import { Editor } from '@tinymce/tinymce-react';

class PostForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            action: this.props.match.params.action,
            item: null,
            redirectToList: this.props.match.params.action === "create"
        };
    }

    componentDidMount() {
        this.getItem(this.props.match.params.id);
    }

    getItem(id) {
        $.get('/post/AjaxGetPost/', { id: id }).done(function (data) {

            this.setState({
                item: data.Item
            });
        }.bind(this));
    }

    render() {
        if (this.state.item) {
            return <FormikPost item={this.state.item} action={this.state.action} redirectToList={this.state.redirectToList} />;
        } else {
            return <LoadingIndicator />;
        }
    }
}


class FrmPost extends React.Component {

    constructor(props) {
        super(props);

        this.handleEditorChange = this.handleEditorChange.bind(this);
    }

    componentDidMount() {
        console.log('mounted');
    }

    handleEditorChange(e) {
        console.log('Content was updated:');
        this.props.setFieldValue("Content", e.target.getContent());
    }

    render() {
        let { values, errors, touched, isSubmitting } = this.props;
        return (
            <Form id="form1" autoComplete="off" className="row">
                <input type="hidden" name="Id" value={values.Id} />
                <input type="hidden" name="FormAction" value={values.FormAction} />

                <div className="col-md-9">
                    <div className="card">
                        <div className="card-header"><h3>{postTypeText}</h3></div>
                        <div className="card-body">
                            <div className="form-group">
                                <label className="col-control-label">Tiêu đề (*)</label>
                                <div className="">
                                    <Field type="text" name="Title" className="form-control" placeholder="Tiêu đề" />
                                    <ErrorMessage name="Title" render={msg => <div className="invalid-input">{msg}</div>} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-control-label">Nội dung</label>
                                <div className="">
                                    {/*<Field component="textarea" rows="20" name="Content" className="form-control" />*/}
                                    
                                    <Editor
                                        initialValue={values.Content}
                                        init={tinyMCEConfig}
                                        onChange={this.handleEditorChange}
                                    /> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div> {/*end left col */} 

                <div className="col-md-3 pl-0">
                    <div className="card">
                        <div className="card-header"><h3>Xuất bản</h3></div>
                        <div className="card-body">
                            <div className="form-group">
                                Tạo bởi: <strong>thanhpc</strong>  <br />
                                Ngày tạo: <strong>16/01/2018 21:42</strong>
                            </div>
                            <div className="form-group">
                                Cập nhật bởi: <strong>thanhpc</strong> <br />
                                Ngày cập nhật: <strong>16/01/2018 21:42</strong> 
                            </div>
                            <div className="form-group">
                                Tình trạng: <strong>Chờ duyệt</strong> 
                            </div>
                            <div className="form-group text-center">
                                <button disabled={isSubmitting} type="submit" className="btn btn-primary btn-sm mr-3">Lưu lại</button>
                                <Link to="/post/" className="btn btn-sm btn-secondary">Quay lại</Link>
                            </div>
                        </div>
                    </div>                    
                </div>  {/*end right col */} 
                

                
            </Form>
        );
    }
}

const FormikPost = withRouter(withFormik({
    mapPropsToValues({ item, action }) {
        return {
            Id: item.Id || 0,
            Title: item.Title || '',
            Content: item.Content || '',
            FormAction: action
        };
    },
    validationSchema: Yup.object().shape({
        Title: Yup.string().required('Vui lòng nhập tiêu đề')
    }),
    handleSubmit(values, { resetForm, setErrors, setSubmitting, props }) {
        const { history } = props;
        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/post/ajaxSavePost/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);

            if (props.redirectToList) {
                history.push('/post/');
            }
        });
    }
})(FrmPost));

export default PostForm;