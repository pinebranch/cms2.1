﻿import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ReactTable from "react-table";
import PostForm from './form.js';

const PostManager = () => 
    <Router>
        <div>
            <Route exact path="/post/" component={PostList} />
            <Route path="/post/:action/:id?" component={PostForm} />
        </div>
    </Router>
;

class PostList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            pages: null,
            loading: true,
            page: 0,
            pageSize: 10
        };
        this.handleDeletePost = this.handleDeletePost.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });

        var pid = state.page + 1,
            pagesize = state.pageSize;
        this.getList(pid, pagesize);
    }

    getList(pid, pagesize) {
        var app = this;
        $.get('/post/ajaxGetPosts/', { type: postType, pid: pid, pagesize: pagesize }).then(function (data) {
            app.setState({
                list: data.Items,
                pages: data.TotalPages,
                loading: false,
                page: pid
            });
        });
    }

    handleDeletePost(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn xóa đối tượng này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/post/ajaxDeletePost/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList(app.state.page, app.state.pageSize);
            });
        }
    }

    render() {
        const { list, pages, loading } = this.state;

        const cols = [
            { Header: "Id", accessor: "Id", maxWidth:50},
            {
                Header: 'Tiêu đề',
                id: 'title',
                minWidth: 500,
                accessor: x => 
                    <div>
                        <Link to={`edit/${x.Id}`} className="mr-3">{x.Title}</Link>
                        <div className="table-row-action mt-1">
                            <Link to={`edit/${x.Id}`} className="mr-3"><i className="fa fa-pencil" /> Sửa</Link>
                            <a href="#" onClick={this.handleDeletePost.bind(this, x.Id)} className="text-danger"><i className="fa fa-trash" /> Xóa </a>
                        </div>
                    </div>                
            },
            {
                Header: 'Xuất bản',
                id: 'fullName',
                maxWidth: 100,
                accessor: x => <span className="text-white p-1 small" style={{ background: x.StatusColor }}>{x.StatusName}</span>
            },
            {
                Header: 'Người đăng',
                id: 'createdUser',
                accessor: x => <span>{x.CreatedUserName}</span>                
            },            
            {
                Header: 'Cập nhật',
                id: 'updatedUser',
                accessor: x =>
                    <div>
                        <span className={x.UpdatedBy !== x.CreatedBy ? 'text-success' : ''}>{x.UpdatedUserName}</span>
                        <div className="small mt-1">{CMS.Utility.formatDateVN(x.UpdatedDate)}</div>
                    </div> 
            }
        ];

        return (
            <div className="card">
                <div className="card-header"><h3><i className="fa fa-newspaper-o" /> Danh sách {postTypeText}</h3></div>
                <div className="card-body">
                    <div className="text-right mb-3">
                        <Link className="btn btn-primary btn-sm" to="/post/create"><i className="fa fa-plus" /> Tạo mới</Link>
                    </div>
                    <ReactTable
                        manual
                        data={list}
                        pages={pages}
                        loading={this.state.loading}
                        columns={cols}
                        onFetchData={this.getData}
                        defaultPageSize={this.state.pageSize}
                        showPageSizeOptions={false}
                        minRows={5}
                        onPageChange={page => this.setState({ page })}
                    />
                </div>                
            </div>
        );
    }
}

ReactDOM.render(
    <PostManager />
, document.getElementById('app'));