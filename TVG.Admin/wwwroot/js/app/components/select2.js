﻿import { FieldProps } from 'formik';
import Select, { Option, ReactSelectProps } from 'react-select';

const Select2 = ({
    options,
    isMulti,
    defaultValue,
    field,
    form
}) =>
    <Select
        defaultValue={defaultValue}
        options={options}
        name={field.name}
        isMulti={isMulti}
        value={options ? options.find(option => option.value === field.value) : ''}
        onChange={(option) => form.setFieldValue(field.name, option.value)}
        onBlur={field.onBlur}
    />;
export default Select2;