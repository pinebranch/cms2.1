﻿import { Link, withRouter } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage, FieldArray } from 'formik';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import LoadingIndicator from './../components/loading-indicator.js';

class UserForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            action: this.props.match.params.action,
            item: null,
            roles: [],
            userRoles: [],
            redirectToList: this.props.match.params.action === "create"
        };
    }

    componentDidMount() {
        this.getItem(this.props.match.params.id);
    }

    getItem(id) {
        $.get('/user/AjaxGetUser/', { id: id }).done(function (data) {

            this.setState({                
                item: data.User,
                roles: data.Roles,
                userRoles: data.UserRoles
            });
        }.bind(this));
    }

    render() {
        if (this.state.item) {
            return <FormikUser item={this.state.item} roles={this.state.roles} userRoles={this.state.userRoles} action={this.state.action} redirectToList={this.state.redirectToList} />;
        } else {
            return <LoadingIndicator />;
        }
    }
}


class FrmUser extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        
    }

    render() {
        let { values, roles, errors, touched, isSubmitting } = this.props;
        return (
            <Form id="form1" autoComplete="off">
                <input type="hidden" name="Id" value={values.Id} />
                <input type="hidden" name="FormAction" value={values.FormAction} />
                <div className="form-group form-flex">
                    <label className="control-label">Tên đăng nhập (*)</label>
                    <div className="control-wrapper">
                        <Field type="text" name="UserName" disabled={values.FormAction === 'edit'} className="form-control" placeholder="Tên đăng nhập" />
                        <ErrorMessage name="UserName" render={msg => <div className="invalid-input">{msg}</div>} />
                    </div>
                </div>
                <div className="form-group form-flex">
                    <label className="control-label">Email (*)</label>
                    <div className="control-wrapper">
                        <Field type="text" name="Email" className="form-control" placeholder="Email" />
                        <ErrorMessage name="Email" render={msg => <div className="invalid-input">{msg}</div>} />
                    </div>
                </div>
                <div className="form-group form-flex">
                    <label className="control-label">Họ (*)</label>
                    <div className="control-wrapper">
                        <Field type="text" name="LastName" className="form-control" placeholder="Họ và tên lót" />
                        <ErrorMessage name="LastName" render={msg => <div className="invalid-input">{msg}</div>} />
                    </div>
                </div>
                <div className="form-group form-flex">
                    <label className="control-label">Tên (*)</label>
                    <div className="control-wrapper">
                        <Field type="text" name="FirstName" className="form-control" placeholder="Tên" />
                        <ErrorMessage name="FirstName" render={msg => <div className="invalid-input">{msg}</div>} />
                    </div>
                </div>
                <div className="form-group form-flex">
                    <label className="control-label">Phone</label>
                    <div className="control-wrapper">
                        <Field type="text" name="PhoneNumber" className="form-control" placeholder="Phone" />
                        <ErrorMessage name="PhoneNumber" render={msg => <div className="invalid-input">{msg}</div>} />
                    </div>
                </div>

                <div className="form-group form-flex">
                    <label className="control-label">Nhóm</label>
                    <div className="control-wrapper">
                        <FieldArray
                            name="UserRoles"
                            render={arrayHelpers => (
                                <div>
                                    {roles.map(role => (
                                        <div key={role.Id} className="form-check">
                                            <label>
                                                <input
                                                    name="UserRoles"
                                                    type="checkbox"
                                                    value={role.Id}
                                                    checked={values.UserRoles.includes(role.Id)}
                                                    onChange={e => {
                                                        if (e.target.checked) arrayHelpers.push(role.Id);
                                                        else {
                                                            const idx = values.UserRoles.indexOf(role.Id);
                                                            arrayHelpers.remove(idx);
                                                        }
                                                    }}
                                                />
                                                <span className="label-text">{role.DisplayName}</span>
                                            </label>
                                        </div>
                                    ))}
                                </div>
                            )}
                        />
                    </div>
                </div>                

                <div className="form-group text-center">
                    <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
                    <Link to="/user/" className="btn btn-secondary">Quay lại</Link>
                </div>
            </Form>
        );
    }
}

const FormikUser = withRouter(withFormik({
    mapPropsToValues({ item, roles, userRoles, action }) {
        return {
            Id: item.Id || 0,
            UserName: item.UserName || '',
            Email: item.Email || '',
            FirstName: item.FirstName || '',
            LastName: item.LastName || '',
            PhoneNumber: item.PhoneNumber || '',           
            UserRoles: userRoles,
            FormAction: action
        };
    },
    validationSchema: Yup.object().shape({
        UserName: Yup.string().required('Vui lòng nhập tên đăng nhập'),
        Email: Yup.string().email("Email không hợp lệ").required('Vui lòng nhập email'),
        FirstName: Yup.string().required("Vui lòng nhập tên"),
        LastName: Yup.string().required("Vui lòng nhập họ")
    }),
    handleSubmit(values, { resetForm, setErrors, setSubmitting, props }) {
        const { history } = props;
        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/user/ajaxSaveUser/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);

            if (props.redirectToList) {
                history.push('/user/');
            } 
        });
    }
})(FrmUser));

export default UserForm;