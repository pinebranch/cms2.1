﻿import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ReactTable from "react-table";
import UserForm from './form.js';

const UserManager = () => (
    <Router>
        <div>            
            <Route exact path="/user/" component={UserList} />
            <Route path="/user/:action/:id?" component={UserForm} />
        </div> 
    </Router>
);

class UserList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            pages: null,
            loading: true,
            page: 0,
            pageSize: 10
        };
        this.handleLockUser = this.handleLockUser.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });

        var pid = state.page + 1,
            pagesize = state.pageSize;
        this.getList(pid, pagesize);
    }

    getList(pid, pagesize) {
        var app = this;
        $.get('/user/ajaxGetUsers/', { pid: pid, pagesize: pagesize }).then(function (data) {
            app.setState({ 
                list: data.Users,
                pages: data.TotalPages,
                loading: false,
                page: pid
            });
        });
    }

    handleLockUser(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn khóa thành viên này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/user/ajaxLockUser/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList(app.state.page, app.state.pageSize);
            });
        }
    }

    handleUnlockUser(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn mở khóa thành viên này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/user/ajaxUnlockUser/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList(app.state.page, app.state.pageSize);
            });
        }
    }

    render() {
        const { list, pages, loading } = this.state;

        const cols = [
            {
                Header: 'Tên đăng nhập',
                id: 'username',
                accessor: d => (
                    <span>
                        {d.IsLockedOut && <i className="fa fa-lock text-danger" />} {d.UserName}
                    </span>
                )
            },
            { Header: 'Họ tên', id: 'fullName', accessor: d => d.LastName + ' ' + d.FirstName },
            {
                Header: 'Email',
                id: 'email',
                accessor: d => (
                    <span>{d.EmailConfirmed && <i className="fa fa-check text-success" title="Email đã xác nhận" />} {d.Email}</span>
                )
            },
            { Header: 'Phone', accessor: 'PhoneNumber' },
            {
                Header: 'Thao tác',
                id: 'actions',
                accessor: d => (
                    <div>
                        <Link to={`edit/${d.Id}`} className="mr-3"><i className="fa fa-pencil" /> Sửa</Link>
                        {!d.IsLockedOut && d.LockoutEnabled && <a href="#" onClick={this.handleLockUser.bind(this, d.Id)} className="text-danger"><i className="fa fa-lock" /> Khóa </a>} 
                        {d.IsLockedOut && d.LockoutEnabled && <a href="#" onClick={this.handleUnlockUser.bind(this, d.Id)} className="text-success"><i className="fa fa-unlock text-success" /> Mở khóa </a>} 
                    </div>
                )
            }
        ];

        return (
            <div>
                <div className="text-right mb-3">
                    <Link className="btn btn-primary btn-sm" to="/user/create"><i className="fa fa-plus" /> Tạo mới</Link>
                </div>
                <ReactTable
                    manual
                    data={list}
                    pages={pages}
                    loading={this.state.loading}
                    columns={cols}
                    onFetchData={this.getData}
                    defaultPageSize={this.state.pageSize}
                    showPageSizeOptions={false}
                    minRows={5}
                    onPageChange={page => this.setState({ page })}
                />
            </div>
        );
    }
}

ReactDOM.render((
    <UserManager />
), document.getElementById('app'));