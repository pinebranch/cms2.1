﻿import ReactTable from "react-table";
import { Formik, Field, Form, ErrorMessage, FieldArray } from 'formik';
import { withFormik } from 'formik';
import * as Yup from 'yup';

class TaxonomyList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            loading: true,
            showModal: false,
            formItem: {}
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEditItem = this.handleEditItem.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleSubmitted = this.handleSubmitted.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });
        this.getList();
    }

    getList() {
        var app = this;
        $.get('/taxonomy/ajaxGetTaxonomies/').then(function (data) {
            app.setState({
                list: data.Items,
                loading: false
            });
        });
    }

    handleDelete(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn xóa phân loại này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/taxonomy/ajaxDeleteTaxonomy/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList();
            });
        }
    }

    handleEditItem(item, event) {
        event.preventDefault();
        this.setState({ showModal: true, formItem: item });
    }

    handleSubmitted() {
        this.getList();
    }

    handleCloseModal() {
        this.setState({ showModal: false, formItem: {} });
    }

    render() {
        const { list, pages, loading } = this.state;

        const cols = [
            {
                Header: 'Tên phân loại',
                id: 'displayName',
                accessor: x =>
                    <div>
                        {x.DisplayName}
                        {!CMS.Utility.stringIsEmpty(x.Note) && <em className="small form-text">{x.Note}</em>}
                    </div>
            },
            { Header: 'Mã phân loại', accessor: 'Name' },
            {
                Header: 'Cấu trúc cây',
                id: 'isHierarchy',
                accessor: x => x.IsHierarchy && <i className="fa fa-check-circle text-success" />
            },
            {
                Header: 'Có hình đại diện',
                id: 'hasThumbnail',
                accessor: x => x.HasThumbnail && <i className="fa fa-check-circle text-success" />
            },
            {
                Header: 'Có màu sắc',
                id: 'hasColor',
                accessor: x => x.HasColor && <i className="fa fa-check-circle text-success" />
            },
            {
                Header: 'Thao tác',
                id: 'actions',
                accessor: item =>
                    <div>
                        <a href="#" onClick={this.handleEditItem.bind(this, item)} className="mr-3"><i className="fa fa-pencil" /> Sửa</a>
                        <a href="#" onClick={this.handleDelete.bind(this, item.Id)}><i className="fa fa-remove" /> Xóa </a>
                    </div>
            }
        ];

        return (
            <div>
                <div className="row mb-3">
                    <div className="col-md-12 text-right">
                        <a className="btn btn-primary btn-sm" href="#" onClick={this.handleEditItem.bind(this, {})}><i className="fa fa-plus" /> Tạo mới</a>
                    </div>
                </div>
                <ReactTable
                    manual
                    data={list}
                    loading={this.state.loading}
                    columns={cols}
                    onFetchData={this.getData}
                    showPageSizeOptions={false}
                    showPagination={false}
                    minRows={5}
                />

                {this.state.showModal
                    ? <TaxonomyModal onCloseModal={this.handleCloseModal} onSubmitted={this.handleSubmitted} item={this.state.formItem} />
                    : null}
            </div>
        );
    }
}

class TaxonomyModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            item: {}
        };

        this.handleSubmitted = this.handleSubmitted.bind(this);
    }

    componentDidMount() {
        const { onCloseModal } = this.props;
        $(this.modal).modal('show');
        $(this.modal).on('hidden.bs.modal', onCloseModal);
    }

    handleSubmitted() {
        $(this.modal).modal('hide');
        this.props.onSubmitted();
        this.props.onCloseModal();
    }

    render() {
        return (
            <div ref={modal => this.modal = modal} className="modal fade custom-modal show" id="mdTaxonomyForm" tabIndex="-1" role="dialog" >
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Thông tin phân loại</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <FormikTaxonomy onSubmitted={this.handleSubmitted} item={this.props.item} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const FrmTaxonomy = ({ values, errors, touched, isSubmitting }) => (
    <Form>
        <input type="hidden" name="Id" value={values.Id} />
        <div className="form-group">
            <label className="col-form-label">Tên phân loại</label>
            <Field type="text" name="DisplayName" className="form-control" placeholder="Nhập tên phân loại" />
            <ErrorMessage name="DisplayName" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <label className="col-form-label">Mã phân loại (dạng "ma-phan-loai")</label>
            <Field type="text" name="Name" className="form-control" placeholder="Nhập mã phân loại" />
            <ErrorMessage name="Name" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>        
        <div className="form-group">
            <div className="form-check">
                <label>
                    <Field name="IsHierarchy" type="checkbox" value="true" checked={values.IsHierarchy} />
                    <span className="label-text">Cấu trúc phân loại dạng cây</span>
                </label>
            </div>
        </div>
        <div className="form-group">
            <div className="form-check">
                <label>
                    <Field name="HasThumbnail" type="checkbox" value="true" checked={values.HasThumbnail} />
                    <span className="label-text">Có hình đại diện</span>
                </label>
            </div>
        </div>
        <div className="form-group">
            <div className="form-check">
                <label>
                    <Field name="HasColor" type="checkbox" value="true" checked={values.HasColor} />
                    <span className="label-text">Có đánh dấu màu sắc (chỉ dùng ở phần quản lý)</span>
                </label>
            </div>
        </div>
        <div className="form-group">
            <label className="col-form-label">Ghi chú</label>
            <Field component="textarea" name="Note" className="form-control" placeholder="Ghi chú về phân loại này" />
        </div>

        <div className="form-group">
            <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
    </Form>
);

const FormikTaxonomy = withFormik({
    mapPropsToValues({ item }) {
        return {
            Id: item.Id || 0,
            Name: item.Name || '',
            DisplayName: item.DisplayName || '',
            IsHierarchy: item.IsHierarchy || false,
            HasThumbnail: item.HasThumbnail || false,
            HasColor: item.HasColor || false,
            Note: item.Note || ''
        };
    },
    validationSchema: Yup.object().shape({
        Name: Yup.string().required('Vui lòng nhập mã phân loại'),
        DisplayName: Yup.string().required('Vui lòng nhập tên phân loại')
    }),
    handleSubmit(values, { props, resetForm, setErrors, setSubmitting }) {

        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/taxonomy/ajaxSaveTaxonomy/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);
            props.onSubmitted();
        });
    }
})(FrmTaxonomy);

ReactDOM.render(<TaxonomyList />, document.getElementById('app'));