﻿import { BrowserRouter as Router, Route, Link, withRouter } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import { setTimeout } from "timers";
import LoadingIndicator from './../components/loading-indicator.js';

const RoleManager = () => (
    <Router>
        <div>
            <div className="text-right mb-3">
                <Link className="btn btn-primary btn-sm" to="/role/create"><i className="fa fa-plus"/> Tạo mới</Link>
            </div>
            <Route exact path="/role/" component={RoleList} />     
            <Route path="/role/:action/:id?" component={RoleForm} />
        </div>
    </Router>
);

class RoleList extends React.Component {

    constructor(props) {
        super(props);

        this.state = { list: [] };

        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        this.getRoles();
    }

    getRoles() {
        $.get('/role/ajaxlist/').then(function (data) {
            this.setState({
                list: data.Roles
            });
        }.bind(this));
    }   

    handleDelete(id, event) {
        event.preventDefault();
        if (confirm('Bạn thật sự muốn xóa nhóm này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/role/ajaxDeleteRole/', {id: id, "f" : token }).done(function (data) {
                toastr[data.status](data.msg);
                this.getRoles();
            }.bind(this));
        }        
    }

    render() {
        var rowItem = function (item) {
            return (
                <tr key={item.Id}>
                    <td>{item.Name}</td>
                    <td>{item.DisplayName}</td>
                    <td>
                        <Link to={`edit/${item.Id}`} className="mr-3"><i className="fa fa-pencil" /> Sửa</Link>
                        <a href="#" onClick={this.handleDelete.bind(this, item.Id)}><i className="fa fa-remove"/> Xóa </a>
                    </td>
                </tr>
            );
        }.bind(this);

        return (
            <table className="table table-bordered table-hover">
                <thead>
                    <tr className="table-header abc">
                        <th>Mã nhóm</th>
                        <th>Tên nhóm</th>
                        <th>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.list.map(rowItem)}
                </tbody>
            </table>
        );
    }
}

class RoleForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            item: null,
            redirectToList: this.props.match.params.action === "create"
        };
    }

    componentDidMount() {
        this.getItem(this.props.match.params.id);
    }

    getItem(id) {
        $.get('/role/AjaxGetRole/', { id: id }).done(function (data) {
            
            this.setState({
                Name: data.Role.Name,
                DisplayName: data.Role.DisplayName,
                item: data.Role
            });
        }.bind(this));
    }

    render() {
        if (this.state.item) {
            return <FormikRole item={this.state.item} redirectToList={this.state.redirectToList} />;
        } else {
            return <LoadingIndicator />;
        }
    }
}

const FrmRole = ({values, errors, touched, isSubmitting}) => (
    <Form>
        <input type="hidden" name="Id" value={values.Id} />
        <div className="form-group">
            <Field type="text" name="Name" className="form-control" placeholder="Mã nhóm" />
            <ErrorMessage name="Name" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <Field type="text" name="DisplayName" className="form-control" placeholder="Tên nhóm" />
            <ErrorMessage name="DisplayName" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <Field type="number" name="DisplayOrder" className="form-control" placeholder="Thứ tự" />
        </div>
        
        <div className="form-group">
            <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
            <Link to="/role/" className="btn btn-secondary">Quay lại</Link>
        </div>            
    </Form>
);

const FormikRole = withRouter(withFormik({
    mapPropsToValues({ item }) {
        return {
            Id: item.Id || 0,
            Name: item.Name || '',
            DisplayName: item.DisplayName || '',
            DisplayOrder: item.DisplayOrder || 0
        };        
    },
    validationSchema: Yup.object().shape({
        Name: Yup.string().required('Vui lòng nhập mã nhóm'),
        DisplayName: Yup.string().required('Vui lòng nhập tên nhóm')
    }),
    handleSubmit(values, { resetForm, setErrors, setSubmitting, props }) {
        const { history } = props;

        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/role/ajaxSaveRole/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.msg);
            setSubmitting(false);
            if (props.redirectToList) {
                history.push('/role/');
            }            
        });        
    }
})(FrmRole));

ReactDOM.render(<RoleManager  />, document.getElementById('app'));