﻿import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import PermissionCategoryList from './permission_category_list.js';
import PermissionList from './permission_list.js';

const PermissionManager = () => (
    <Router>
        <div>
            <Route exact path="/permission/manage/" component={PermissionList} />
            
            <Route exact path="/permission/category/" component={PermissionCategoryList} />
            
        </div>
    </Router>
);

ReactDOM.render((
    <PermissionManager />
), document.getElementById('app'));