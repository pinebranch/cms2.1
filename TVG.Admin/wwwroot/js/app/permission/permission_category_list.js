﻿import { Link } from "react-router-dom";
import ReactTable from "react-table";
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { withFormik } from 'formik';
import * as Yup from 'yup';

class PermissionCategoryList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            loading: true,
            showModal: false,
            formItem: {}
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEditItem = this.handleEditItem.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleSubmitted = this.handleSubmitted.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });        
        this.getList();
    }

    getList() {
        var app = this;
        $.get('/permission/ajaxGetPermissionCategories/').then(function (data) {
            app.setState({
                list: data.Items,
                loading: false
            });
        });
    }

    handleDelete(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn xóa nhóm quyền này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/permission/ajaxDeletePermissionCategory/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList();
            });
        }
    }

    handleEditItem(item, event) {
        
        event.preventDefault();
        this.setState({ showModal: true, formItem: item });
    }

    handleSubmitted() {
        this.getList();
    }

    handleCloseModal() {
        this.setState({ showModal: false, formItem: {} });
        console.log("close");
    }

    render() {
        const { list, pages, loading } = this.state;

        const cols = [
            { Header: 'Mã nhóm', accessor: 'Name' },            
            { Header: 'Tên nhóm', accessor: 'DisplayName' },
            {
                Header: 'Thao tác',
                id: 'actions',
                accessor: item => (
                    <div>
                        <a href="#" onClick={this.handleEditItem.bind(this, item)} className="mr-3"><i className="fa fa-pencil" /> Sửa</a>
                        <a href="#" onClick={this.handleDelete.bind(this, item.Id)}><i className="fa fa-remove" /> Xóa </a>
                    </div>
                )
            }
        ];

        return (
            <div>
                <div className="row mb-3">
                    <div className="col-md-6">
                        <Link to='/permission/manage/' className="btn btn-primary btn-sm">Danh sách quyền</Link>
                    </div>
                    <div className="col-md-6 text-right">
                        <a className="btn btn-primary btn-sm" href="#" onClick={this.handleEditItem.bind(this, {})}><i className="fa fa-plus" /> Tạo mới</a>
                    </div>
                </div>
                <ReactTable
                    manual
                    data={list}                    
                    loading={this.state.loading}
                    columns={cols}
                    onFetchData={this.getData}
                    showPageSizeOptions={false}
                    showPagination={false}
                    minRows={5}
                />

                {this.state.showModal
                    ? <PermissionCategoryModal onCloseModal={this.handleCloseModal} onSubmitted={this.handleSubmitted} item={this.state.formItem} />
                    : null}
            </div>
        );
    }
}

class PermissionCategoryModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            item: {}
        };

        this.handleSubmitted = this.handleSubmitted.bind(this);
    }

    componentDidMount() {
        const { onCloseModal } = this.props;
        $(this.modal).modal('show');
        $(this.modal).on('hidden.bs.modal', onCloseModal);
    }

    handleSubmitted() {

        $(this.modal).modal('hide');
        this.props.onSubmitted();
        this.props.onCloseModal();
    }

    render() {
        return (
            <div ref={modal => this.modal = modal} className="modal fade custom-modal show" id="mdPermissionCategoryForm" tabIndex="-1" role="dialog" aria-labelledby="customModal" >
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Thông tin nhóm</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <FormikPermissionCategory onSubmitted={this.handleSubmitted} item={this.props.item} />
                        </div>
                    </div>
                </div>
            </div>    
        );
    }
}

const FrmPermissionCategory = ({ values, errors, touched, isSubmitting }) => (
    <Form>
        <input type="hidden" name="Id" value={values.Id} />
        <div className="form-group">
            <Field type="text" name="Name" className="form-control" placeholder="Mã nhóm" />
            <ErrorMessage name="Name" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <Field type="text" name="DisplayName" className="form-control" placeholder="Tên nhóm" />
            <ErrorMessage name="DisplayName" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>

        <div className="form-group">
            <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
    </Form>
);

const FormikPermissionCategory = withFormik({
    mapPropsToValues({ item }) {
        return {
            Id: item.Id || 0,
            Name: item.Name || '',
            DisplayName: item.DisplayName || ''
        };
    },
    validationSchema: Yup.object().shape({
        Name: Yup.string().required('Vui lòng nhập mã nhóm'),
        DisplayName: Yup.string().required('Vui lòng nhập tên nhóm')
    }),
    handleSubmit(values, { props, resetForm, setErrors, setSubmitting }) {
        
        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/permission/ajaxSavePermissionCategory/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);
            props.onSubmitted();
        });
    }
})(FrmPermissionCategory);

export default PermissionCategoryList;