﻿
class PermissionRole extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            roles: [],
            permissions: [],
            permissionRoles: []
        };

        this.getPermissions = this.getPermissions.bind(this);
        this.handleSavePermissions = this.handleSavePermissions.bind(this);
    }

    componentDidMount() {
        $.get('/permission/ajaxGetPermissionCategories/').done(function (data) {
            this.setState({ categories: data.Items });
        }.bind(this));

        $.get('/role/AjaxList/').done(function (data) {
            this.setState({ roles: data.Roles });
        }.bind(this));

        $.get('/permission/ajaxGetPermissionRoles/').done(function (data) {
            this.setState({ permissionRoles: data.Items });
        }.bind(this));
    }

    getPermissions() {
        var categoryId = $(this.selectCategory).val();
        var app = this;
        $.get('/permission/ajaxGetPermissions/', { categoryId: categoryId }).done(function (data) {
            app.setState({ permissions: data.Items });
        });        
    }

    handleCheckboxChange(permissionId, roleId, event) {
        if (event.target.checked) {
            this.setState(prevState => {
                prevState.permissionRoles.push({
                    PermissionId: permissionId,
                    RoleId: roleId
                });
                return prevState;
            });
        }
        else {
            this.setState(prevState => {
                var idx = prevState.permissionRoles.findIndex(x => x.PermissionId == permissionId && x.RoleId == roleId);
                prevState.permissionRoles.splice(idx, 1);
                return prevState;
            });
        }
    }

    handleSavePermissions(event) {
        event.preventDefault();
        var token = $('#crsf').find('input[name="f"]').val();
        var formData = new FormData($(this.form)[0]);
        formData.append('f', token);

        $.ajax({
            url: '/permission/ajaxSavePermissionRoles',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                toastr[data.status](data.message);
            }
        });
    }

    render() {
        const { categories, roles, permissions, permissionRoles } = this.state;

        return (
            <div>
                <form id="frmPermissionRole" ref={el => this.form = el}>
                    <div className="row mb-3">
                        <div className="col-md-4">
                            <select name="categoryId" ref={el => this.selectCategory = el} className="form-control" onChange={this.getPermissions}>
                                <option value="0">--- Chọn nhóm để phân quyền --- </option>
                                {categories.map(item => <option key={item.Id} value={item.Id}>{item.DisplayName}</option>)}
                            </select>
                        </div>                    
                    </div>
                    
                    <table className="table table-responsive-xl">
                        <thead>
                            <tr>
                                <th />
                                {roles.map(item => <th key={item.Id}>{item.DisplayName}</th>)}
                            </tr>
                        </thead>
                        <tbody>
                            {permissions.map(permission => (
                                <tr key={permission.Id}>
                                    <td>{permission.DisplayName}</td>
                                    {roles.map(role =>
                                        <td key={role.Id}>
                                            <div className="form-check">
                                                <label>
                                                    <input type="checkbox" name="permissionRoleIds"
                                                        value={`${permission.Id}_${role.Id} `}
                                                        checked={permissionRoles.some(item => item.PermissionId == permission.Id && item.RoleId == role.Id)} 
                                                        onChange={this.handleCheckboxChange.bind(this, permission.Id, role.Id)}
                                                    />
                                                    <span className="label-text" />
                                                </label>
                                            </div>
                                        </td>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    {permissions.length > 0 &&
                        <button type="button" className="btn btn-primary" onClick={this.handleSavePermissions}>Lưu lại</button>}
                </form>
            </div>
        );
    }
}

ReactDOM.render((
    <PermissionRole />
), document.getElementById('app'));