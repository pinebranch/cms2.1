﻿import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ReactTable from "react-table";
import { Formik, Field, Form, ErrorMessage, FieldArray } from 'formik';
import { withFormik } from 'formik';
import * as Yup from 'yup';

class PermissionList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            categories: [],
            loading: true,
            showModal: false,
            formItem: {}
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEditItem = this.handleEditItem.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleSubmitted = this.handleSubmitted.bind(this);
        this.getData = this.getData.bind(this);
        this.getList = this.getList.bind(this);
    }

    componentDidMount() {
        this.getCategories();
    }

    getData(state, instance) {
        var app = this;
        app.setState({ loading: true });
        this.getList();
    }

    getList() {
        var categoryId = $(this.modal).val();
        var app = this;
        $.get('/permission/ajaxGetPermissions/', { categoryId: categoryId }).then(function (data) {
            app.setState({
                list: data.Items,
                loading: false
            });
        });
    }

    getCategories() {
        var app = this;
        $.get('/permission/ajaxGetPermissionCategories/').done(function (data) {
            app.setState({
                categories: data.Items
            });
        });
    }

    handleDelete(id, event) {
        event.preventDefault();
        var app = this;
        if (confirm('Bạn thật sự muốn xóa quyền này?')) {
            var token = $('#crsf').find('input[name="f"]').val();
            $.post('/permission/ajaxDeletePermission/', { id: id, "f": token }).done(function (data) {
                toastr[data.status](data.message);
                app.getList();
            });
        }
    }

    handleEditItem(item, event) {

        event.preventDefault();
        this.setState({ showModal: true, formItem: item });
    }

    handleSubmitted() {
        this.getList();
    }

    handleCloseModal() {
        this.setState({ showModal: false, formItem: {} });
        console.log("close");
    }

    render() {
        const { list, pages, loading } = this.state;

        const cols = [
            { Header: 'Mã ', accessor: 'Name' },
            { Header: 'Tên ', accessor: 'DisplayName' },
            {
                Header: 'Thao tác',
                id: 'actions',
                accessor: item => (
                    <div>
                        <a href="#" onClick={this.handleEditItem.bind(this, item)} className="mr-3"><i className="fa fa-pencil" /> Sửa</a>
                        <a href="#" onClick={this.handleDelete.bind(this, item.Id)}><i className="fa fa-remove" /> Xóa </a>
                    </div>
                )
            }
        ];

        var selectOptions = this.state.categories.map(item => (
            <option key={item.Id} value={item.Id}>{item.DisplayName}</option>
        ));

        return (
            <div>
                <div className="row mb-3">
                    <div className="col-md-6">
                        <Link to='/permission/category/' className="btn btn-primary btn-sm">Danh sách nhóm quyền</Link>
                    </div>
                    <div className="col-md-6 text-right">
                        <a className="btn btn-primary btn-sm" href="#" onClick={this.handleEditItem.bind(this, {})}><i className="fa fa-plus" /> Tạo mới</a>
                    </div>                    
                </div>
                <div className="form-inline mb-3">
                    <select name="categoryId" className="form-control mr-2" ref={modal => this.modal = modal} onChange={this.getList}>
                        <option value="0">-- Chọn nhóm để xem danh sách quyền --</option>
                        {selectOptions}
                    </select>
                    
                </div>
                <ReactTable
                    manual
                    data={list}
                    loading={this.state.loading}
                    columns={cols}
                    showPageSizeOptions={false}
                    showPagination={false}
                    minRows={5}
                />

                {this.state.showModal
                    ? <PermissionModal onCloseModal={this.handleCloseModal} onSubmitted={this.handleSubmitted} item={this.state.formItem} categories={this.state.categories} />
                    : null}
            </div>
        );
    }
}

class PermissionModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            item: {}
        };

        this.handleSubmitted = this.handleSubmitted.bind(this);
    }

    componentDidMount() {
        const { onCloseModal } = this.props;
        $(this.modal).modal('show');
        $(this.modal).on('hidden.bs.modal', onCloseModal);
    }

    handleSubmitted() {

        $(this.modal).modal('hide');
        this.props.onSubmitted();
        this.props.onCloseModal();
    }

    render() {
        return (
            <div ref={modal => this.modal = modal} className="modal fade custom-modal show" id="mdPermissionForm" tabIndex="-1" role="dialog" aria-labelledby="customModal" >
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Thông tin quyền</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <FormikPermission onSubmitted={this.handleSubmitted} item={this.props.item} categories={this.props.categories} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const FrmPermission = ({ values, categories, errors, touched, isSubmitting }) => (
    <Form>
        <input type="hidden" name="Id" value={values.Id} />
        <div className="form-group">
            <Field type="text" name="Name" className="form-control" placeholder="Mã quyền" />
            <ErrorMessage name="Name" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <Field type="text" name="DisplayName" className="form-control" placeholder="Tên quyền" />
            <ErrorMessage name="DisplayName" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <Field component="select" name="CategoryId" className="form-control">
                <option value="0">--- Chọn nhóm quyền --- </option>
                {categories.map(item => (<option key={item.Id} value={item.Id}>{item.DisplayName}</option>))}
            </Field>
            <ErrorMessage name="CategoryId" render={msg => <div className="invalid-input">{msg}</div>} />
        </div>
        <div className="form-group">
            <button disabled={isSubmitting} type="submit" className="btn btn-primary mr-3">Lưu lại</button>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
    </Form>
);

const FormikPermission = withFormik({
    mapPropsToValues({ item }) {
        return {
            Id: item.Id || 0,
            Name: item.Name || '',
            DisplayName: item.DisplayName || '',
            CategoryId: item.CategoryId || 0
        };
    },
    validationSchema: Yup.object().shape({
        Name: Yup.string().required('Vui lòng nhập mã'),
        DisplayName: Yup.string().required('Vui lòng nhập tên'),
        CategoryId: Yup.number().min(1, "Vui lòng chọn nhóm")
    }),
    handleSubmit(values, { props, resetForm, setErrors, setSubmitting }) {

        var token = $('#crsf').find('input[name="f"]').val();
        values.f = token;
        $.ajax({
            type: 'POST',
            url: '/permission/ajaxSavePermission/',
            data: values,
            dataType: 'json'
        }).done(function (data) {
            toastr[data.status](data.message);
            setSubmitting(false);
            props.onSubmitted();
        });
    }
})(FrmPermission);

export default PermissionList;