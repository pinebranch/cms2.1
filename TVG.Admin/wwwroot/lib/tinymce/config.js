﻿var tinyMCEConfig = {
    height: 400,
    image_caption: true,
    image_advtab: true,
    content_css: 'https://fonts.googleapis.com/css?family=Nunito',
    content_css_cors: true,
    language: 'vi_VN',
    //images_upload_url: 'postAcceptor.php',
    file_browser_callback: function (field_name, url, type, win) {
        if (typeof window.puMedia === 'undefined' || window.puMedia.closed) {
            window.puMedia = $.popupWindow("/media/window/?target=tinymce", {
                onFileSelected: function (e) {
                    win.document.getElementById(field_name).value = e.files[0].url;
                    window.puMedia.close();
                }
            });
        } else {
            window.puMedia.focus();
        }
    },
    entity_encoding: "raw",
    plugins: [
        'advlist anchor autolink code colorpicker contextmenu fullscreen image link lists media pagebreak paste print searchreplace table template textcolor toc visualblocks visualchars wordcount'
    ],
    toolbar: 'undo redo | styleselect bold italic forecolor backcolor superscript subscript | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | anchor link unlink image media table toc pagebreak | print fullscreen removeformat code'
};