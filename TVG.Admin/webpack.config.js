﻿var glob = require("glob");
var path = require('path');
var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

function getEntries (){
    return glob.sync("./wwwroot/js/app/**/*.js")
        .map((file) => {
            return {
                name: file.substring(0, file.length - 3).replace("./wwwroot/js/app/", ""),
                path: file
            };
        }).reduce((memo, file) => {
            memo[file.name] = file.path;
            return memo;
        }, {});
}

module.exports = {
    mode: 'production',
    context: __dirname,
    entry: {
        role: './wwwroot/js/app/role/index.js',
        user: './wwwroot/js/app/user/index.js',
        permission_manage: './wwwroot/js/app/permission/manage.js',
        permission_role: './wwwroot/js/app/permission/index.js',
        media_manager: './wwwroot/js/app/media/index.js',
        term: './wwwroot/js/app/term/index.js',
        taxonomy: './wwwroot/js/app/taxonomy/index.js',
        post_type: './wwwroot/js/app/post-type/index.js',
        post: './wwwroot/js/app/post/index.js'
    },    
    output: {
        path: __dirname + "/wwwroot/js/dist",
        filename: '[name].js'
    },
    //plugins: [
    //    new HardSourceWebpackPlugin()
    //],
    watch: true,
    devtool: false,
    watchOptions: {
        ignored: /node_modules/
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }
            }
        ]
    }
};