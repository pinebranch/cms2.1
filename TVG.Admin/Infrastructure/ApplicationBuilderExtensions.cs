﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVG.Core.Extensions;

namespace TVG.Admin.Infrastructure
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder MapSpaFallback(this IApplicationBuilder application, string[] matchedUrls,
           string controllerName, string actionName)
        {
            application.MapWhen(context => context.Request.Path.Value.StartsWithAny(matchedUrls), builder =>
            {
                builder.UseMvc(routes => routes.MapSpaFallbackRoute($"{controllerName}-spa-fallback", 
                    new { controller = controllerName, action = actionName }));
            });

            return application;
        }
    }
}
