﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NPoco;
using TVG.Admin.Models;
using TVG.Core.Extensions;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Identity.Services;

namespace TVG.Admin.Controllers
{
    public class PermissionController : BaseController
    {
        public readonly PermissionService permissionService;
        private readonly IdentityRoleService roleService;

        public PermissionController(PermissionService permissionService, IdentityRoleService roleService)
        {
            this.permissionService = permissionService;
            this.roleService = roleService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Manage()
        {
            return View();
        }

        #region PermissionRole

        public async Task<JsonResult> AjaxGetPermissionRoles()
        {            
            var roles = await roleService.GetRolesAsync();
            var permissionRoles = new List<PermissionRole>();
            foreach(var role in roles)
            {
                permissionRoles.AddRange(await permissionService.GetPermissionsByRoleAsync(role.Id));
            }

            return Json(new { Items = permissionRoles });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSavePermissionRoles(int categoryId, string[] permissionRoleIds)
        {
            if (categoryId <= 0) return Json(new { status = "error", message = "Thông tin không hợp lệ" });

            var permissionRoles = new List<PermissionRole>();
            foreach(var id in permissionRoleIds)
            {
                var str = id.Split("_");
                if(str.Length == 2)
                {
                    var pId = str[0].TryToInt(0);
                    var rId = str[1].TryToInt(0);
                    if(pId > 0 && rId > 0)
                    {
                        permissionRoles.Add(new PermissionRole { PermissionId = pId, RoleId = rId });
                    }
                }
            }

            await permissionService.SavePermissionRolesAsync(categoryId, permissionRoles);

            return Json(new { status = "success", message = "Cập nhật thành công" });
        }

        #endregion

        #region PermissionCategory
        public async Task<JsonResult> AjaxGetPermissionCategories()
        {
            var list = await permissionService.GetPermissionCategoriesAsync();
            return Json(new { Items = list });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSavePermissionCategory(int Id, string Name, string DisplayName)
        {
            if (Id <= 0)
            {
                var obj = new PermissionCategory { Id = Id, Name = Name, DisplayName = DisplayName };
                await permissionService.InsertPermissionCategoryAsync(obj);
            }
            else
            {
                var obj = await permissionService.GetPermissionCategoryByIdAsync(Id);
                if (obj != null)
                {
                    obj.Name = Name;
                    obj.DisplayName = DisplayName;
                    await permissionService.UpdatePermissionCategoryAsync(obj);
                }
            }
            return Json(new { status = "success", message = "Cập nhật thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeletePermissionCategory(int id)
        {
            await permissionService.DeletePermissionCategoryAsync(id);
            return Json(new { status = "success", message = "Xóa thành công" });
        }
        #endregion

        #region Permission

        public async Task<JsonResult> AjaxGetPermissions(int categoryId)
        {
            if(categoryId <=0 ) return Json(new { Items = new List<Permission>() });

            Page<Permission> list = await permissionService.GetPermissionsAsync(new PermissionQueryParameters { CategoryId = categoryId });
            return Json(new { Items = list.Items });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSavePermission(PermissionFormModel model)
        {
            if (!ModelState.IsValid) return Json(new { status = "error", message = "Thông tin không hợp lệ" });

            if(model.Id <= 0)
            {
                var obj = new Permission
                {
                    Name = model.Name,
                    DisplayName = model.DisplayName,
                    CategoryId = model.CategoryId
                };
                await permissionService.InsertPermissionAsync(obj);
            }
            else
            {
                Permission obj = await permissionService.GetPermissionByIdAsync(model.Id);
                if(obj != null)
                {
                    obj.Name = model.Name;
                    obj.DisplayName = model.DisplayName;
                    obj.CategoryId = model.CategoryId;
                    await permissionService.UpdatePermissionAsync(obj);
                }
            }

            return Json(new { status = "success", message = "Cập nhật thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeletePermission(int id)
        {
            await permissionService.DeletePermissionAsync(id);
            return Json(new { status = "success", message = "Xóa thành công" });
        }

        #endregion
    }
}