﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TVG.Admin.Controllers
{
    public class DemoController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult MediaPopup()
        {
            return View();
        }

        public IActionResult Editor()
        {
            return View();
        }
    }
}