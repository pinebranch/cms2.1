﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.Core.Caching;

namespace TVG.Admin.Controllers
{
    public class CacheManagerController : BaseController
    {
        private readonly ICacheManager cacheManager;
        
        public CacheManagerController(ICacheManager cacheManager)
        {
            this.cacheManager = cacheManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ClearAllCache()
        {
            cacheManager.ClearAll();
            return RedirectToAction("Index");
        }
    }
}