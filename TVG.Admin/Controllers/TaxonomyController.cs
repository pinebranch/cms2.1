﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class TaxonomyController : BaseController
    {
        private readonly TaxonomyService taxonomyService;

        public TaxonomyController(TaxonomyService taxonomyService)
        {
            this.taxonomyService = taxonomyService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> AjaxGetTaxonomies()
        {
            var list = await taxonomyService.GetTaxonomiesAsync();
            return Json(new { Items = list });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSaveTaxonomy(Taxonomy model)
        {
            var jsonError = new { status = "error", message = "Thông tin không hợp lệ" };
            if (!ModelState.IsValid) return Json(jsonError);

            if(model.Id == 0)
            {
                await taxonomyService.InsertTaxonomyAsync(model);
            }
            else
            {
                await taxonomyService.UpdateTaxonomyAsync(model);
            }

            return Json(new { status = "success", message = "Cập nhật thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeleteTaxonomy(int id)
        {
            await taxonomyService.DeleteTaxonomyAsync(id);
            return Json(new { status = "success", message = "Xóa đối tượng thành công" });
        }
    }
}