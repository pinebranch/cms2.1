﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.Core.Extensions;

namespace TVG.WebUI.Controllers
{
    [Route("[controller]")]
    public class ErrorController : Controller
    {
        [ResponseCache(CacheProfileName = "Error")]
        [HttpGet("{statusCode:int:range(400, 599)}/{status?}", Name = "ErrorGetError")]
        public IActionResult Error(int statusCode, string status)
        {
            Response.StatusCode = statusCode;
            return (Request.IsAjaxRequest()) ? (ActionResult)PartialView("Error", statusCode) : View("Error", statusCode);
        }
    }
}