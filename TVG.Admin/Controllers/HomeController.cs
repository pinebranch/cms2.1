﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TVG.Admin.Models;
using TVG.Core.Caching;
using TVG.Core.Configuration;

namespace TVG.Admin.Controllers
{
    public class HomeController : BaseController
    {
        readonly ICacheManager cacheManager;
        readonly TvgAppSettings appSettings;
        private readonly IHostingEnvironment hostingEnvironment;

        public HomeController(ICacheManager cacheManager, TvgAppSettings appSettings, IHostingEnvironment hostingEnvironment)
        {
            this.cacheManager = cacheManager;
            this.appSettings = appSettings;
            this.hostingEnvironment = hostingEnvironment;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }


        [Authorize(Policy = "Admin")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            var time = cacheManager.GetOrAdd("test", "time", () => { return DateTime.Now.ToString(); }, TimeSpan.FromSeconds(30));
            var time2 = cacheManager.GetOrAdd("test", "time2", () => { return DateTime.Now.ToString(); });
            var time3 = cacheManager.GetOrAdd("test", "time3", () => { return DateTime.Now.ToString(); });
            ViewData["Time"] = time;
            ViewData["Cache"] = cacheManager.GetTypeName();
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
