﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class PostTypeController : BaseController
    {
        private readonly PostTypeService postTypeService;
        private readonly TaxonomyService taxonomyService;

        public PostTypeController(PostTypeService postTypeService, TaxonomyService taxonomyService)
        {
            this.postTypeService = postTypeService;
            this.taxonomyService = taxonomyService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> AjaxGetPostTypes()
        {
            var list = await postTypeService.GetPostTypesAsync();
            var taxonomies = await taxonomyService.GetTaxonomiesAsync();
            return Json(new { Items = list, Taxonomies = taxonomies });
        }

        public async Task<JsonResult> AjaxGetPostTypeTaxonomies(int id)
        {
            var list = await postTypeService.GetPostTypeTaxonomiesAsync(id);
            var statusTag = list.Find(x => x.IsStatusTag)?.TaxonomyId ?? 0;
            var primaryTaxonomy = list.Find(x => x.IsPrimary)?.TaxonomyId ?? 0;
            return Json(new { Items = list, StatusTag = statusTag, PrimaryTaxonomy = primaryTaxonomy });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSavePostType(PostType model, List<int> TaxonomyIds, int StatusTag, int PrimaryTaxonomy)
        {
            var jsonError = new { status = "error", message = "Thông tin không hợp lệ" };
            if (!ModelState.IsValid) return Json(jsonError);

            var result = -1;
            if (model.Id == 0)
            {
                result = await postTypeService.InsertPostTypeAsync(model);
                model.Id = result;
            }
            else
            {
                result = await postTypeService.UpdatePostTypeAsync(model);
            }

            if (result > -1)
            {
                if (StatusTag > 0 && TaxonomyIds.IndexOf(StatusTag) < 0)
                {
                    TaxonomyIds.Add(StatusTag);
                }
                if(PrimaryTaxonomy > 0 && !TaxonomyIds.Contains(PrimaryTaxonomy))
                {
                    TaxonomyIds.Add(PrimaryTaxonomy);
                }
                var postTypeTaxonomies = new List<PostTypeTaxonomy>();
                foreach (var id in TaxonomyIds)
                {
                    postTypeTaxonomies.Add(new PostTypeTaxonomy
                    {
                        PostTypeId = model.Id,
                        TaxonomyId = id,
                        DisplayOrder = 0,
                        IsStatusTag = id == StatusTag,
                        IsPrimary = id == PrimaryTaxonomy
                    });
                }
                await postTypeService.SavePostTypeTaxonomiesAsync(model.Id, postTypeTaxonomies);
            }

            return Json(new { status = "success", message = "Cập nhật thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeletePostType(int id)
        {
            await postTypeService.DeletePostTypeAsync(id);
            return Json(new { status = "success", message = "Xóa đối tượng thành công" });
        }
    }
}