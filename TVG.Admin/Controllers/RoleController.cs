﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TVG.Core.Extensions;
using TVG.Identity.Entities;
using TVG.Identity.Services;

namespace TVG.Admin.Controllers
{
    public class RoleController : Controller
    {
        private readonly IdentityRoleService roleService;
        private readonly RoleManager<ApplicationRole> roleManager;

        public RoleController(IdentityRoleService roleService, RoleManager<ApplicationRole> roleManager)
        {
            this.roleService = roleService;
            this.roleManager = roleManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> AjaxList()
        {
            var roles = await roleService.GetRolesAsync();
            return Json(new { Roles = roles });
        }

        [HttpGet]
        public async Task<JsonResult> AjaxGetRole(int id)
        {
            var role = await roleService.GetRoleByIdAsync(id);
            if(role == null)
            {
                role = new ApplicationRole() { Name="", DisplayName = "" };
            }

            return Json(new { Role = role });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSaveRole(int Id, string Name, string DisplayName, int DisplayOrder)
        {
            var result = IdentityResult.Success;
            if(Id > 0)
            {
                var role = await roleService.GetRoleByIdAsync(Id);
                if(role != null)
                {
                    role.Name = Name;
                    role.DisplayName = DisplayName;
                    role.DisplayOrder = DisplayOrder;
                    result = await roleManager.UpdateAsync(role);
                }
            }
            else
            {
                var role = new ApplicationRole { Name = Name, DisplayName = DisplayName, DisplayOrder = DisplayOrder };
                result = await roleManager.CreateAsync(role); 
            }

            if (result != IdentityResult.Success) return Json(new { status = "error", msg = "Có lỗi xảy ra khi thực hiện thao tác này" });

            return Json(new { status = "success", msg = "Cập nhật thông tin nhóm thành công" });            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeleteRole(int id)
        {
            var role = await roleService.GetRoleByIdAsync(id);
            if (role != null)
            {
                await roleManager.DeleteAsync(role);
            }
            return Json(new { status = "success", msg = "Xóa thông tin nhóm thành công" });
        }
    }
}