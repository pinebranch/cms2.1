﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TVG.Identity.Entities;
using TVG.Identity.Services;

namespace TVG.Admin.Controllers
{
    public class UserController : BaseController
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IdentityUserService userService;
        private readonly IdentityRoleService roleService;
        private readonly RoleManager<ApplicationRole> roleManager;

        public UserController(
            UserManager<ApplicationUser> userManager,
            IdentityUserService userService,
            IdentityRoleService roleService,
            RoleManager<ApplicationRole> roleManager
        )
        {
            this.userManager = userManager;
            this.userService = userService;
            this.roleService = roleService;
            this.roleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> AjaxGetUsers(int pid, int pagesize)
        {
            var users = await userService.GetUsersAsync(new ApplicationUserQueryParameters
            {
                PageIndex = pid,
                PageSize = pagesize
            });

            return Json(new { Users = users.Items, TotalPages = users.TotalPages });
        }

        [HttpGet]
        public async Task<JsonResult> AjaxGetUser(int id)
        {
            var user = await userService.GetUserByIdAsync(id);
            user = user ?? new ApplicationUser();

            var roles = await roleService.GetRolesAsync();
            var userRoles = await roleService.GetRolesByUserId(user.Id);

            return Json(new { User = user, Roles = roles, UserRoles = userRoles.Select(x => x.Id).ToList() });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSaveUser(UserInputModel userInput)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = "error", message = "Thông tin thành viên không hợp lệ." });
            }

            var user = new ApplicationUser();
            if (userInput.Id > 0)
            {
                user = await userService.GetUserByIdAsync(userInput.Id) ?? user;
            }

            user.FirstName = userInput.FirstName;
            user.LastName = userInput.LastName;

            if (user.Id > 0)
            {
                await userManager.UpdateAsync(user);
                var email = await userManager.GetEmailAsync(user);
                if (userInput.Email != email)
                {
                    await userManager.SetEmailAsync(user, userInput.Email);
                }

                var phoneNumber = await userManager.GetPhoneNumberAsync(user);
                if (userInput.PhoneNumber != phoneNumber)
                {
                    await userManager.SetPhoneNumberAsync(user, userInput.PhoneNumber);
                }
            }
            else
            {
                user.Email = userInput.Email;
                user.PhoneNumber = userInput.PhoneNumber;
                user.UserName = userInput.UserName;
                await userManager.CreateAsync(user, "@123456");
            }

            if (user.Id > 0)
            {
                await roleService.InsertUserRolesAsync(user.Id, userInput.UserRoles);
            }

            return Json(new { status = "success", message = "Cập nhật thông tin thành viên thành công." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxLockUser(int id)
        {
            var user = await userService.GetUserByIdAsync(id);
            if (user != null)
            {
                user.LockoutEnd = DateTime.UtcNow.AddYears(100);
                await userService.UpdateAsync(user);
            }

            return Json(new { status = "success", message = "Thành viên đã bị khóa." });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxUnlockUser(int id)
        {
            var user = await userService.GetUserByIdAsync(id);
            if (user != null)
            {
                user.LockoutEnd = null;
                await userService.UpdateAsync(user);
            }

            return Json(new { status = "success", message = "Thành viên đã mở khóa." });
        }

        public class UserInputModel
        {
            public int Id { get; set; }

            [Required]
            public string UserName { get; set; }

            [Required(ErrorMessage = "Vui lòng nhập email")]
            [EmailAddress(ErrorMessage = "Email không hợp lệ")]
            public string Email { get; set; }

            [Required(ErrorMessage = "Vui lòng nhập tên thành viên")]
            public string FirstName { get; set; }

            [Required(ErrorMessage = "Vui lòng nhập họ và tên lót")]
            public string LastName { get; set; }

            [Phone]
            public string PhoneNumber { get; set; }

            public int[] UserRoles { get; set; }
        }
    }
}