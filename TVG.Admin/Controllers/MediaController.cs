﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using TVG.Admin.Models;
using TVG.Core.Configuration;
using TVG.Core.Helpers;

namespace TVG.Admin.Controllers
{
    public class MediaController : Controller
    {
        private readonly Dictionary<string, List<string>> FILE_TYPES = new Dictionary<string, List<string>>()
        {
            { "image", new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif" } },
            { "word", new List<string> { ".doc", ".docx" } },
            { "excel", new List<string> { ".xls", ".xlsx" } },
            { "powerpoint", new List<string> { ".ppt", ".pptx" } },
            { "pdf", new List<string> { ".pdf" } },
            { "text", new List<string> { ".txt" } },
            { "audio", new List<string> { ".mp3" } },
            { "video", new List<string> { ".mp4", ".mkv" } }
        };

        private readonly TvgAppSettings appSettings;
        private readonly IHostingEnvironment hostingEnvironment;

        public MediaController(TvgAppSettings appSettings, IHostingEnvironment hostingEnvironment)
        {
            this.appSettings = appSettings;
            this.hostingEnvironment = hostingEnvironment;
        } 

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Window(string target)
        {
            ViewData["target"] = target;
            return View();
        }

        public JsonResult GetDirectoryContents(DirectoryQueryParameters parameters)
        {
            var uploadFolder = new PhysicalFileProvider(appSettings.UploadFolderConfig.Path);
            var info = uploadFolder.GetDirectoryContents(parameters.Path)
                .Where(x => !string.Equals(x.Name, "cache", StringComparison.OrdinalIgnoreCase));
            var directories = new List<FileItemModel>();
            var files = new List<FileItemModel>();

            foreach(var file in info)
            {               
                var item = new FileItemModel()
                {
                    Name = file.Name,
                    IsDirectory = true,
                    LastModified = file.LastModified,
                    Url = GetFileUrl(parameters.Path, file.Name)
                };
                if (file.IsDirectory)
                {
                    item.FileType = "directory";
                    directories.Add(item);
                }
                else
                {
                    item.FileType = GetFileType(file.Name);
                    item.Length = file.Length;
                    item.Thumbnail = GetFileThumbnail(file, parameters.Path);
                    files.Add(item);
                }
            }

            return Json( new { Directories = directories, Files = files });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateFolder(string path, string newFolder)
        {
            if(string.IsNullOrEmpty(newFolder))
            {
                newFolder = "new-folder";
            }
            path = path ?? "";
            path = path.TrimStart('/').Replace("/", "\\");
            var uploadFolder = new PhysicalFileProvider(appSettings.UploadFolderConfig.Path);
            var newFolderPath = Path.Combine(uploadFolder.Root, path, newFolder);
            if (!Directory.Exists(newFolderPath))
            {
                Directory.CreateDirectory(newFolderPath);
            }

            return Json(new { status = "success", message = "Tạo thư mục thành công " });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteFiles(string path, string[] files)
        {
            path = NormalizeInputPath(path);
            var uploadFolder = new PhysicalFileProvider(appSettings.UploadFolderConfig.Path);
            foreach(var file in files)
            {
                string filePath = Path.Combine(uploadFolder.Root, path, file);
                var fileInfo = System.IO.File.GetAttributes(filePath);
                if (fileInfo == FileAttributes.Directory)
                {
                    Directory.Delete(filePath);
                }
                else
                {
                    System.IO.File.Delete(filePath);
                }
            }
            return Json(new { status = "success", message = "Xóa thư mục/file thành công " });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PasteFiles(string path, string[] files, string action)
        {
            path = NormalizeInputPath(path);
            var uploadFolder = new PhysicalFileProvider(appSettings.UploadFolderConfig.Path);
            foreach (var file in files)
            {
                string filePath = Path.Combine(uploadFolder.Root, NormalizeInputPath(file));
                string newFilePath = Path.Combine(uploadFolder.Root, path, Path.GetFileName(file));
                var fileInfo = System.IO.File.GetAttributes(filePath);
                if (IsFileOrDirectoryExists(newFilePath)) continue;
                
                if (string.Equals(action, "cut", StringComparison.OrdinalIgnoreCase))
                {
                    if (fileInfo == FileAttributes.Directory)
                    {
                        System.IO.Directory.Move(filePath, newFilePath);
                    }
                    else
                    {
                        System.IO.File.Move(filePath, newFilePath);
                    }
                }
                else
                {
                    if (fileInfo == FileAttributes.Directory)
                    {
                        FileHelper.DirectoryCopy(filePath, newFilePath, true, false);
                    }
                    else
                    {
                        System.IO.File.Copy(filePath, newFilePath, false);
                    }
                }
                
            }
            return Json(new { status = "success", message = "Dán thư mục/file thành công " });
        }

        [HttpPost]
        public async Task<JsonResult> Upload(string path, string qqfilename, IFormFile qqfile)
        {
            path = NormalizeInputPath(path);
            var uploadFolder = new PhysicalFileProvider(appSettings.UploadFolderConfig.Path);

            var filePath = Path.Combine(uploadFolder.Root, path, qqfilename);
            while (System.IO.File.Exists(filePath))
            {
                var newFileName = Path.GetFileNameWithoutExtension(qqfilename) + "_1";
                var ext = Path.GetExtension(qqfilename);
                filePath = Path.Combine(uploadFolder.Root, path, newFileName + ext);
            }
            if (qqfile.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await qqfile.CopyToAsync(stream);
                }
            }
            return Json(new { success = true, files = qqfile });
            
        }

        public class DirectoryQueryParameters
        {
            public string Path { get; set; } = string.Empty;
        }

        private string GetFileType(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return string.Empty;          

            var ext = Path.GetExtension(fileName);

            foreach(var item in FILE_TYPES)
            {
                if(item.Value.Any(x => x == ext))
                {
                    return item.Key;
                }
            }

            return "file";
        }

        private string NormalizeInputPath(string path)
        {
            path = path ?? "";
            path = path.TrimStart('/').Replace("/", "\\");
            return path;
        }

        private bool IsFileOrDirectoryExists(string path)
        {
            return System.IO.Directory.Exists(path) || System.IO.File.Exists(path);
        }

        private string GetFileThumbnail(IFileInfo file, string directoryPath)
        {
            var fileType = GetFileType(file.Name);
            if (!string.Equals(fileType, "image", StringComparison.OrdinalIgnoreCase)) return "";

            var uploadFolder = new PhysicalFileProvider(appSettings.UploadFolderConfig.Path);
            var newFileName = Path.GetFileNameWithoutExtension(file.Name) + "-80x80" + Path.GetExtension(file.Name);
            var cacheDirectoryPath = Path.Combine(uploadFolder.Root, "cache", NormalizeInputPath(directoryPath));
            var newFilePath = Path.Combine(cacheDirectoryPath, newFileName);
            var newFileUrl = appSettings.UploadFolderConfig.Request + "/cache/" + directoryPath.TrimStart('/') + "/" + newFileName;

            if (!System.IO.Directory.Exists(cacheDirectoryPath))
            {
                Directory.CreateDirectory(cacheDirectoryPath);
            }
            if (!System.IO.File.Exists(newFilePath))
            {
                ImageHelper.ResizeImage(file.PhysicalPath, newFilePath, 80, 80, true);
            }
            return newFileUrl;
        }

        private string GetFileUrl(string directory, string fileName)
        {
            directory = directory.TrimStart('/');
            var fileUrl = new StringBuilder("");
            fileUrl.Append(appSettings.UploadFolderConfig.Request).Append("/");
            if (string.IsNullOrEmpty(directory))
            {
                fileUrl.Append(fileName);
            }
            else
            {
                fileUrl.Append(directory).Append("/").Append(fileName);
            }

            return fileUrl.ToString();
        }
    }
}