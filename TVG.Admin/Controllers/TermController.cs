﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.Admin.Models;
using TVG.Core.Extensions;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class TermController : BaseController
    {
        private readonly TaxonomyService taxonomyService;

        public TermController(TaxonomyService taxonomyService)
        {
            this.taxonomyService = taxonomyService;
        }

        public async Task<IActionResult> Index(string type)
        {
            var taxonomy = await taxonomyService.GetTaxonomyAsync(type);
            if (taxonomy == null) return NotFound();
            
            ViewBag.Taxonomy = taxonomy;
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> AjaxGetTerms(string type, int pid, int pagesize)
        {
            var taxonomy = await taxonomyService.GetTaxonomyAsync(type);
            if(taxonomy == null) return Json(new { Items = new List<Taxonomy>(), TotalPages = 0, Taxonomy = taxonomy });

            var list = await taxonomyService.GetTermsAsync(new TermQueryParameters
            {
                Taxonomy = type,
                PageIndex = pid,
                PageSize = pagesize
            });

            return Json(new { list.Items, list.TotalPages, Taxonomy = taxonomy });
        }

        public async Task<JsonResult> AjaxGetTerm(int id, string taxonomy)
        {
            if(id < 0 || string.IsNullOrEmpty(taxonomy)) return Json(new { Terms = new List<Term>() });

            var taxonomyType = await taxonomyService.GetTaxonomyAsync(taxonomy);
            var item = await taxonomyService.GetTermByIdAsync(id) ?? new Term();
            var terms = await taxonomyService.GetTermsAsync(taxonomy);
            var idx = terms.FindIndex(x => x.Id == id);
            if(idx > -1)
            {
                terms.RemoveAt(idx);
            }            

            return Json(new { Item = item, Terms = terms, Taxonomy = taxonomyType });            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxSaveTerm(TermFormModel model)
        {
            var jsonError = new { status = "error", message = "Thông tin không hợp lệ" };
            if (!ModelState.IsValid) return Json(jsonError);

            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.TryToInt();
            if (model.Id > 0)
            {
                var term = await taxonomyService.GetTermByIdAsync(model.Id);
                if (term == null) return Json(jsonError);
                term = model.MapToObject(term, userId);
                await taxonomyService.UpdateTermAsync(term);
            }
            else
            {
                var term = model.MapToObject(null, userId);
                await taxonomyService.InsertTermAsync(term);
            }

            return Json(new { status = "success", message = "Cập nhật thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeleteTerm(int id)
        {
            await taxonomyService.DeleteTermAsync(id);
            return Json(new { status = "success", message = "Xóa đối tượng thành công" });
        }
    }
}