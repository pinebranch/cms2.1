﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.Data.Domain;
using TVG.Data.Services;

namespace TVG.Admin.Controllers
{
    public class PostController : BaseController
    {
        private readonly PostService postService;
        private readonly PostTypeService postTypeService;
        private readonly TaxonomyService taxonomyService;

        public PostController(PostService postService, PostTypeService postTypeService, TaxonomyService taxonomyService)
        {
            this.postTypeService = postTypeService;
            this.postService = postService;
            this.taxonomyService = taxonomyService;
        }

        public async Task<IActionResult> Index(string type = "post")
        {
            var postType = await postTypeService.GetPostTypeAsync(type);
            if (postType == null) return NotFound();

            return View(postType);
        }

        public async Task<JsonResult> AjaxGetPosts(string type, int pid, int pagesize)
        {
            var parameters = new PostQueryParameters
            {
                PostType = type,
                PageIndex = pid,
                PageSize = pagesize
            };

            var list = await postService.GetPostsAsync(parameters);

            return Json(new { list.Items, list.TotalPages });
        }

        public async Task<JsonResult> AjaxGetPost(int id)
        {
            var post = await postService.GetPostByIdAsync(id);

            return Json(new { Item = post });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AjaxDeletePost(int id)
        {
            await postService.DeletePostAsync(id);
            return Json(new { status = "success", message = "Xóa đối tượng thành công" });
        }
    }
}