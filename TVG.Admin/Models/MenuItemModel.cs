﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVG.Admin.Models
{
    public class MenuItemModel
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public string Icon { get; set; }
        public string Permission { get; set; }
        public bool DevOnly { get; set; }
        public List<MenuItemModel> Children { get; set; } = new List<MenuItemModel>();
    }
}
