﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVG.Admin.Models
{
    public class FileItemModel
    {
        public string Name { get; set; }
        public bool IsDirectory { get; set; }
        public DateTimeOffset LastModified { get; set; }
        public long Length { get; set; }
        public string Url { get; set; }
        public string FileType { get; set; }
        public string Thumbnail { get; set; } = string.Empty;
        public string PhysicalPath { get; set; }
    }
}
