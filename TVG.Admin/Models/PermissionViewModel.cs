﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TVG.Admin.Models
{
    public class PermissionFormModel
    {
        public int Id { get; set; }
        [Required] public int CategoryId { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string DisplayName { get; set; }
    }
}
