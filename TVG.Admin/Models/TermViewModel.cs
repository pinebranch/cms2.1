﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TVG.Core;
using TVG.Core.Extensions;
using TVG.Data.Domain;

namespace TVG.Admin.Models
{
    public class TermFormModel
    {
        public int Id { get; set; }
        public string Taxonomy { get; set; }
        public int ParentId { get; set; }
        [Required] public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string Image { get; set; }
        public int DisplayOrder { get; set; }
        public string Color { get; set; }

        public void MapFromObject(Term obj)
        {
            this.Id = obj.Id;
            this.Taxonomy = obj.Taxonomy;
            this.ParentId = obj.ParentId;
            this.Name = obj.Name;
            this.Slug = obj.Slug;
            this.Description = obj.Description;
            this.MetaDescription = obj.MetaDescription;
            this.MetaKeyword = obj.MetaKeyword;
            this.Image = obj.Image;
            this.Color = obj.Color;
            this.DisplayOrder = obj.DisplayOrder;
        } 

        public Term MapToObject(Term obj, int userId)
        {
            if(obj == null)
            {
                obj = new Term
                {
                    Id = 0,
                    CreatedBy = userId,
                    CreatedDate = DateTime.Now,
                    IsDeleted = false
                };
            }

            obj.ParentId = this.ParentId;
            obj.Taxonomy = this.Taxonomy;
            obj.Name = this.Name;
            obj.Slug = !string.IsNullOrEmpty(this.Slug) ? this.Slug : StringHelper.Slugify(this.Name.TruncateAtWord(100, ""));
            obj.Description = this.Description;
            obj.MetaDescription = this.MetaDescription;
            obj.MetaKeyword = this.MetaKeyword;
            obj.DisplayOrder = this.DisplayOrder;
            obj.Image = this.Image;
            obj.Color = this.Color;
            obj.UpdatedBy = userId;
            obj.UpdatedDate = DateTime.Now;

            return obj;
        }
    }
}
