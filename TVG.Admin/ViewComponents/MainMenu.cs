﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using TVG.Admin.Models;
using TVG.Core.Extensions;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Identity.Entities;
using TVG.Identity.Services;

namespace TVG.Admin.ViewComponents
{
    [Authorize]
    public class MainMenu : ViewComponent
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IdentityRoleService roleService;
        private readonly PermissionService permissionService;
        private readonly UserManager<ApplicationUser> userManager;

        public MainMenu(IHostingEnvironment hostingEnvironment, 
            IdentityRoleService roleService, 
            PermissionService permissionService,
            UserManager<ApplicationUser> userManager)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.roleService = roleService;
            this.permissionService = permissionService;
            this.userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {           
            bool isLoggedIn = UserClaimsPrincipal.FindFirst(ClaimTypes.NameIdentifier) != null;
            if(isLoggedIn)
            {
                var claim = UserClaimsPrincipal.FindFirst(ClaimTypes.NameIdentifier).Value;
                var userId = claim.TryToInt();
                var roles = await roleService.GetRolesByUserId(userId);
                var permissions = new List<PermissionRole>();
                foreach (var role in roles)
                {
                    permissions.AddRange(await permissionService.GetPermissionsByRoleAsync(role.Id));
                }

                string contentRootPath = hostingEnvironment.ContentRootPath;
                var JSON = await System.IO.File.ReadAllTextAsync(contentRootPath + "/config/menu.json");

                ViewData["menu"] = JsonConvert.DeserializeObject<List<MenuItemModel>>(JSON);
                ViewData["permissions"] = permissions;
            }

            ViewData["isLoggedIn"] = isLoggedIn;
            return View();
        }
    }
}
