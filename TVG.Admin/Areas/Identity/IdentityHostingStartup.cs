﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TVG.Admin.Data;

[assembly: HostingStartup(typeof(TVG.Admin.Areas.Identity.IdentityHostingStartup))]
namespace TVG.Admin.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        { 
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}