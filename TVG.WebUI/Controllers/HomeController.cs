﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TVG.WebUI.Models;
using TVG.Core;
using TVG.Core.Caching;
using NPoco;
using Microsoft.Extensions.Logging;
using TVG.Core.Configuration;
using Microsoft.Extensions.Options;
using TVG.Core.Extensions;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.AspNetCore.Http.Extensions;
using TVG.Core.Filters;
using Microsoft.AspNetCore.Hosting;

namespace TVG.WebUI.Controllers
{
    public class HomeController : Controller
    {
        ICacheManager cache;
        IDatabase db;
        ILogger<HomeController> logger;
        readonly TvgAppSettings appSettings;
        IHostingEnvironment currentEnv;

        public HomeController(ICacheManager cache, IDatabase db, ILogger<HomeController> logger, TvgAppSettings appSettings, IHostingEnvironment currentEnv)
        {
            this.cache = cache;
            this.db = db;
            this.logger = logger;
            this.appSettings = appSettings;
            this.currentEnv = currentEnv;
        }

        [HttpGet("", Name = "HomeGetIndex")]
        public IActionResult Index()
        {
            var banners = db.Fetch<Banner>();
            ViewData["Banners"] = banners;

            var time = cache.GetOrAdd("test", "time", () => { return DateTime.Now.ToString(); }, TimeSpan.FromSeconds(30));
            var time2 = cache.GetOrAdd("test", "time2", () => { return DateTime.Now.ToString(); });
            var time3 = cache.GetOrAdd("test", "time3", () => { return DateTime.Now.ToString(); });
            ViewData["Time"] = time;
            ViewData["Cache"] = cache.GetTypeName();
            ViewData["UploadFolder"] = appSettings.UploadFolderConfig.Path;

            ViewData["CurrentEnv"] = currentEnv.EnvironmentName;
            
            logger.LogError("tesst");
            return View();
        }
        
        public IActionResult About(AboutQueryString query)
        {
            ViewData["Message"] = "Your application description page.";

            var rawurl = "https://bencull.com/some/path?key1=val1&key2=val2&key2=valdouble&key3=";

            ViewData["UrlAdd"] = MyUrlHelper.AddKeyToQueryString(rawurl, "pt", "p123");
            ViewData["UrlUpdate"] = MyUrlHelper.UpdateKeyInQueryString(rawurl, "key1", "key1234");
            ViewData["UrlRemove"] = TvgAppSettings.Current.CacheDurationLong;

            //var pId = Request.Query["pId"].ToString().TryToInt();
            
            ViewData["MySqlConnection"] = TvgAppSettings.Current.ConnectionStrings.MySqlConnection;
            ViewData["pId"] = query.PId;
            ViewData["name"] = query.Name;
            return View();
        }

        public async Task<IActionResult> About2()
        {
            ViewData["Message"] = "Your application description page.";

            var time = await cache.GetOrAddAsync("test", "time", async () => { return await Task.FromResult(DateTime.Now.ToString()); }, TimeSpan.FromSeconds(30));
            ViewData["time"] = time;
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
