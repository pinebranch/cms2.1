﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using TVG.Core.Configuration;
using TVG.Core.Filters;
using TVG.Identity;

namespace TVG.WebUI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = _ => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.HttpOnly = HttpOnlyPolicy.Always;
                options.Secure = CookieSecurePolicy.SameAsRequest;
            });

            services
                .AddAntiforgerySecurely()
                .AddOptions(Configuration)
                .AddCaching()
                .AddDatabaseProvider(Configuration)
                .AddRouting(options =>
                {
                    options.AppendTrailingSlash = true;
                    options.LowercaseUrls = true;
                })
                .AddResponseCaching()
                .AddResponseCompression(options =>
                {
                    var responseCompressionSettings = new ResponseCompressionSettings();
                    Configuration.Bind("ResponseCompressionSettings", responseCompressionSettings);

                    options.Providers.Add<GzipCompressionProvider>();
                    options.MimeTypes =
                        ResponseCompressionDefaults.MimeTypes.Concat(responseCompressionSettings.MimeTypes);
                    options.EnableForHttps = true;
                })
                .Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal)
                .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .AddScoped<IUrlHelper>(x => x
                    .GetRequiredService<IUrlHelperFactory>()
                    .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext))
                .AddSingleton<RedirectToCanonicalUrlAttribute>()
                ;

            services.AddIdentityServices();
            services.AddInfrastructureServices();

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            services.AddMvc(options =>
            {
                var cacheProfileSettings = Configuration.GetSection<CacheProfileSettings>();
                foreach (var keyValuePair in cacheProfileSettings.CacheProfiles)
                {
                    options.CacheProfiles.Add(keyValuePair);
                }

                // Adds a filter which help improve search engine optimization (SEO).
                options.Filters.AddService(typeof(RedirectToCanonicalUrlAttribute));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/error/{0}/");
                app.UseInternalServerErrorOnException();
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseNoServerHttpHeader();
            app.UseResponseCaching();
            app.UseResponseCompression();//UseResponseCompression phai dat truoc UseStaticFiles thi moi compress duoc static files
            app.UseStaticFilesWithCacheControl();
            app.UseCookiePolicy();
            app.UseSecurityHttpHeaders();
            UseCspHttpHeader(app);
            app.UseAuthentication();
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(Configuration.GetValue<string>("TvgAppSettings:UploadFolderConfig:Path", "")),
                RequestPath = new PathString(Configuration.GetValue<string>("TvgAppSettings:UploadFolderConfig:Request", "")),
                EnableDirectoryBrowsing = false
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void UseCspHttpHeader(IApplicationBuilder app)
        {
            app.UseCsp(options =>
            {
                options
                    // Enables logging of CSP violations. Register with the https://report-uri.io/ service to get a
                    // URL where you can send your CSP violation reports and view them.
                    .ReportUris(x => x.Uris("http://example.com/csp-report"))
                    // default-src - Sets a default source list for a number of directives. If the other directives
                    // below are not used then this is the default setting.
                    .DefaultSources(x => x.None()) // We disallow everything by default.

                    // base-uri - This directive restricts the document base URL
                    //            See http://www.w3.org/TR/html5/infrastructure.html#document-base-url.
                    // .BaseUris(x => ...)

                    // child-src - This directive restricts from where the protected resource can load web workers
                    //             or embed frames. This was introduced in CSP 2.0 to replace frame-src. frame-src
                    //             should still be used for older browsers.
                    // .ChildSources(x => ...)

                    // connect-src - This directive restricts which URIs the protected resource can load using
                    //               script interfaces (Ajax Calls and Web Sockets).
                    .ConnectSources(x =>
                    {
                        x.Self();// Allow all AJAX and Web Sockets calls from the same domain.

                        x.CustomSources(new string[]
                        {
                            // "*.example.com", // Allow AJAX and Web Sockets to example.com.
                            "localhost:*",
                            "ws://localhost:*"
                        });
                    })

                    // font-src - This directive restricts from where the protected resource can load fonts.
                    .FontSources( x =>
                    {
                        x.Self();
                        x.CustomSources(new string[]
                        {
                            // "*.example.com", // Allow AJAX and Web Sockets to example.com.
                            "maxcdn.bootstrapcdn.com" // Allow fonts from maxcdn.bootstrapcdn.com.
                        });
                    })

                    // form-action - This directive restricts which URLs can be used as the action of HTML form elements.
                    .FormActions(x =>
                    {
                        x.Self();
                        x.CustomSources(new string[] {
                            "*.google.com",
                            "*.facebook.com"
                        });
                    })

                    // frame-src - This directive restricts from where the protected resource can embed frames.
                    //             This is deprecated in favour of child-src but should still be used for older browsers.
                    // .FrameSources(x => ...)

                    // frame-ancestors - This directive restricts from where the protected resource can embed
                    //                   frame, iframe, object, embed or applet's.
                    // .FrameAncestors(x => ...)

                    // img-src - This directive restricts from where the protected resource can load images.
                    .ImageSources(x =>
                    {
                        x.Self();
                        x.CustomSources(new string[]
                        {
                            "data:"
                        });
                    })

                    // script-src - This directive restricts which scripts the protected resource can execute.
                    //              The directive also controls other resources, such as XSLT style sheets, which
                    //              can cause the user agent to execute script.
                    .ScriptSources(x =>
                    {
                        x.Self();
                        var customSources = new List<string>()
                        {
                            "*.googleapis.com",
                            "*.aspnetcdn.com",
                            "*.facebook.net",
                            "*.addthis.com",
                            "*.addthisedge.com",
                            "*.google-analytics.com",
                            "localhost:*"
                        };
                        x.CustomSources(customSources.ToArray());
                        // Allow the use of the eval() method to create code from strings. This is unsafe and
                        // can open your site up to XSS vulnerabilities.
                        // x.UnsafeEval();
                        // Allow in-line JavaScript, this is unsafe and can open your site up to XSS vulnerabilities.
                        x.UnsafeInline();
                    })

                    // media-src - This directive restricts from where the protected resource can load video and audio.
                    // .MediaSources(x => ...)

                    // object-src - This directive restricts from where the protected resource can load plug-ins.
                    // .ObjectSources(x => ...)

                    // plugin-types - This directive restricts the set of plug-ins that can be invoked. You can
                    //                also use the @Html.CspMediaType("application/pdf") HTML helper instead of this
                    //                attribute. The HTML helper will add the media type to the CSP header.
                    // .PluginTypes(x => x.MediaTypes("application/x-shockwave-flash", "application/xaml+xml"))

                    // style-src - This directive restricts which styles the user applies to the protected resource.
                    .StyleSources( x =>
                    {
                        x.Self();
                        x.CustomSources(new string[]
                        {
                            "maxcdn.bootstrapcdn.com"
                        });
                    });
            });
        }
    }
}
