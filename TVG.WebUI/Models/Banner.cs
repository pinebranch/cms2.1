﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVG.WebUI.Models
{
    [TableName("Banner")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Banner
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string BannerType { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class AboutQueryString
    {
        public int PId { get; set; }
        public string Name { get; set; }
    }
}
