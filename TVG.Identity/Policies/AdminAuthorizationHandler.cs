﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Identity.Policies
{
    public class AdminAuthorizationHandler : AuthorizationHandler<AdminRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminRequirement requirement)
        {
            if (context.User.Identity.IsAuthenticated && context.User.Identity.Name == "thanhpc")
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
