﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Identity.Entities;
using TVG.Identity.Services;
using TVG.Identity.Stores;

namespace TVG.Identity
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIdentityServices(this IServiceCollection services)
        {
            services.AddTransient<IdentityUserService>();
            services.AddTransient<IdentityUserClaimService>();
            services.AddTransient<IdentityUserLoginService>();
            services.AddTransient<IdentityRoleService>();
            services.AddTransient<IUserStore<ApplicationUser>, UserStore>();
            services.AddTransient<IRoleStore<ApplicationRole>, RoleStore>();
            services.AddIdentity<ApplicationUser, ApplicationRole>(x =>
            {
                x.Password.RequireDigit = false;
                x.Password.RequiredLength = 6;
                x.Password.RequireLowercase = false;
                x.Password.RequireNonAlphanumeric = false;
                x.Password.RequireUppercase = false;
                x.User.RequireUniqueEmail = true;
            }).AddDefaultTokenProviders();

            return services;
        }
    }
}
