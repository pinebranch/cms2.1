﻿using NPoco;
using TVG.Core.Caching;
using TVG.Data.TvgConstants;
using TVG.Identity.Entities;
using System.Threading.Tasks;
using System.Collections.Generic;
using TVG.Core.Extensions;

namespace TVG.Identity.Services
{
    public class IdentityUserService
    {
        private readonly IDatabase db;
        private readonly ICacheManager cache;

        public IdentityUserService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.USERS;
        }

        public Task<Page<ApplicationUser>> GetUsersAsync(ApplicationUserQueryParameters parameters = null)
        {
            if (parameters == null) parameters = new ApplicationUserQueryParameters();
            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USERS_ALL, 
                parameters.Name, parameters.Email, parameters.PageIndex, parameters.PageSize);
            string group = CacheNames.IDENTITY_USERS_ALL_GROUP;
            return cache.GetOrAddAsync(group, key, () =>
            {
                var sql = new Sql();
                sql.Select("*");
                sql.From(TableNames.TABLE_USERS);
                sql.WhereIf(!string.IsNullOrEmpty(parameters.Name), "Name LIKE @0", parameters.Name + "%");
                sql.WhereIf(!string.IsNullOrEmpty(parameters.Email), "Email LIKE @0", parameters.Email + "%");
                var strSql = sql.ToString();
                return db.PageAsync<ApplicationUser>(parameters.PageIndex, parameters.PageSize, sql);
            }, CacheHelper.CacheDurationMedium);
        }

        public Task<ApplicationUser> GetUserByIdAsync(int userId)
        {
            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_BY_ID, userId);
            string group = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_GROUP_BY_ID, userId);
            return cache.GetOrAddAsync(group, key, 
                () => db.FirstOrDefaultAsync<ApplicationUser>("WHERE Id = @0", userId), 
                CacheHelper.CacheDurationMedium);
        }

        public async Task<ApplicationUser> GetUserByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName)) return null;
            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_BY_NAME, userName);

            var user = await cache.GetOrAddAsync("", key, 
                () => db.FirstOrDefaultAsync<ApplicationUser>("WHERE NormalizedUserName = @0", userName), 
                CacheHelper.CacheDurationMedium);

            if (user != null)
            {
                await cache.AddKeyToGroupAsync(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_GROUP_BY_ID, user.Id), key, CacheHelper.CacheDurationMedium);
            }
            return user;
        }

        public async Task<ApplicationUser> GetUserByEmailAsync(string email)
        {
            if (string.IsNullOrEmpty(email)) return null;

            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_BY_EMAIL, email);
            var obj = await cache.GetOrAddAsync(null, key, 
                () => db.FirstOrDefaultAsync<ApplicationUser>("WHERE NormalizedEmail = @0", email), 
                CacheHelper.CacheDurationMedium);
            if (obj != null)
            {
                cache.AddKeyToGroup(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_GROUP_BY_ID, obj.Id), key, CacheHelper.CacheDurationMedium);
            }
            return obj;
        }        

        public async Task<int> InsertAsync(ApplicationUser user)
        {
            AfterInsert(user);
            var result = await db.InsertAsync(user);
            return await (result != null ? Task.FromResult<int>(1) : Task.FromResult<int>(0));
        }

        private Task<int> DeleteAsync(int userId)
        {
            string sql = $"DELETE FROM {TableNames.TABLE_USERS} WHERE Id = @0";
            AfterDelete(userId);
            return db.ExecuteAsync(sql, userId);
        }

        public Task<int> DeleteAsync(ApplicationUser user)
        {
            return DeleteAsync(user.Id);
        }

        public Task<int> UpdateAsync(ApplicationUser user)
        {
            AfterUpdate(user);
            return db.UpdateAsync(user);
        }   

        private void AfterInsert(ApplicationUser user)
        {
            cache.RemoveByGroup(string.Format(CacheNames.IDENTITY_USERS_ALL_GROUP));
        }

        private void AfterUpdate(ApplicationUser user)
        {
            cache.RemoveByGroup(string.Format(CacheNames.IDENTITY_USERS_ALL_GROUP));
            cache.RemoveByGroup(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_GROUP_BY_ID, user.Id));
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_BY_NAME, user.NormalizedUserName));
        }

        private void AfterDelete(int userId)
        {
            cache.RemoveByGroup(string.Format(CacheNames.IDENTITY_USERS_ALL_GROUP));
            cache.RemoveByGroup(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_GROUP_BY_ID, userId));
        }
    }
}
