﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVG.Data.TvgConstants;
using TVG.Core.Caching;
using TVG.Identity.Entities;

namespace TVG.Identity.Services
{
    public class IdentityRoleService
    {
        private readonly IDatabase db;
        private readonly ICacheManager cache;

        public IdentityRoleService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.USERS;
        }

        public Task<List<ApplicationRole>> GetRolesAsync()
        {
            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_ROLE_ALL);
            return cache.GetOrAddAsync(null, key, 
                () => db.FetchAsync<ApplicationRole>("ORDER BY DisplayOrder"), 
                CacheHelper.CacheDurationMedium);
        }

        public async Task<ApplicationRole> GetRoleByIdAsync(int roleId)
        {
            var roles = await GetRolesAsync();
            return roles.Find(x => x.Id == roleId);
        }

        public async Task<ApplicationRole> GetRoleByNameAsync(string roleName)
        {
            var roles = await GetRolesAsync();
            return roles.Find(x => string.Equals(x.NormalizedName, roleName, StringComparison.InvariantCultureIgnoreCase));
        }

        public async Task<List<ApplicationUser>> GetUsersInRoleAsync(string roleName)
        {
            var role = await GetRoleByNameAsync(roleName);
            if (role == null) return new List<ApplicationUser>();

            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USERS_BY_ROLE, role.Id);
            return await cache.GetOrAddAsync(null, key, () => {
                var sql = Sql.Builder
                .Select($"u.*")
                .From($"{TableNames.TABLE_USERS} u")
                .InnerJoin($"{TableNames.TABLE_USER_ROLES} ur").On($"u.Id = ur.UserId")
                .Where($"ur.RoleId = @0", role.Id);

                return db.FetchAsync<ApplicationUser>(sql);
            }, CacheHelper.CacheDurationMedium);
        }

        public async Task<int> InsertRoleAsync(ApplicationRole role)
        {
            var result = await db.InsertAsync(role);
            AfterInsert(role);
            return role.Id;
        }

        private Task<int> DeleteRoleAsync(int roleId)
        {
            string sql = $"DELETE FROM {TableNames.TABLE_ROLES} WHERE Id = @0";
            AfterDelete(roleId);

            return db.ExecuteAsync(sql, roleId);
        }

        public Task<int> DeleteRoleAsync(ApplicationRole role) {
            return DeleteRoleAsync(role.Id);
        }

        public async Task<int> UpdateRoleAsync(ApplicationRole role)
        {
            AfterUpdate(role);
            var result = await db.UpdateAsync(role);
            return result;
        }

        #region UserRole
        public Task<List<ApplicationRole>> GetRolesByUserId(int userId)
        {
            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_ROLES_BY_ID, userId);
            
            return cache.GetOrAddAsync("", key, () =>
            {
                var sql = Sql.Builder
                .Select("r.*")
                .From($"{TableNames.TABLE_ROLES} r")
                .InnerJoin($"{TableNames.TABLE_USER_ROLES} ur").On($"r.Id = ur.RoleId")
                .Where($"ur.UserId = @0", userId);
                return db.FetchAsync<ApplicationRole>(sql);
            }, CacheHelper.CacheDurationMedium);
        }

        public Task<int> DeleteUserRoleAsync(int userId, int roleId)
        {
            ClearCacheUserRole(userId, roleId);
            
            return db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_USER_ROLES} WHERE UserId=@0 AND RoleId=@1", userId, roleId);
        }

        public async Task<int> InsertUserRoleAsync(int userId, int roleId)
        {
            ClearCacheUserRole(userId, roleId);

            var userRole = new ApplicationUserRole { UserId = userId, RoleId = roleId };
            var result = await db.InsertAsync(userRole);
            return await (result != null ? Task.FromResult<int>(1) : Task.FromResult<int>(0));
        }

        public async Task InsertUserRolesAsync(int userId, int[] roleIds)
        {
            if (roleIds == null) return;

            await db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_USER_ROLES} WHERE UserId=@0 ", userId);
            var userRoles = new List<ApplicationUserRole>();
            foreach(var roleId in roleIds)
            {
                ClearCacheUserRole(userId, roleId);
                await db.InsertAsync(new ApplicationUserRole { UserId = userId, RoleId = roleId });
            }
        }

        private void ClearCacheUserRole(int userId, int roleId)
        {
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USERS_BY_ROLE, roleId));
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_ROLE, userId, roleId));
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_ROLES_BY_ID, userId));
        } 
        #endregion

        private void AfterInsert(ApplicationRole role)
        {
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_ROLE_ALL));
        }

        private void AfterUpdate(ApplicationRole role)
        {
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_ROLE_ALL));
        }

        private void AfterDelete(int roleId)
        {
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_ROLE_ALL));
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USERS_BY_ROLE, roleId));
        }
    }
}
