﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.TvgConstants;
using TVG.Core.Caching;
using TVG.Identity.Entities;

namespace TVG.Identity.Services
{
    public class IdentityUserClaimService
    {
        private readonly IDatabase db;
        private readonly ICacheManager cache;

        public IdentityUserClaimService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.USERS;
        }

        public async Task<IList<ApplicationUser>> GetUsersByClaimAsync(Claim claim)
        {
            string key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USERS_ALL_BY_CLAIM, claim.Type, claim.Value);
            string group = CacheHelper.SetCacheName(CacheNames.IDENTITY_USERS_ALL_CLAIM_GROUP);
            return await cache.GetOrAddAsync(group, key, () => {
                var sql = new Sql();
                sql.Select("u.*").From($"{TableNames.TABLE_USERS} u");
                sql.InnerJoin($"{TableNames.TABLE_USER_CLAIMS} uc").On("u.Id = uc.UserId");
                sql.Where("u.ClaimType = @0 AND u.ClaimValue = @1", claim.Type, claim.Value);

                return db.FetchAsync<ApplicationUser>(sql);
            }, CacheHelper.CacheDurationMedium);
        }

        public async Task<IList<Claim>> GetClaimsByUserIdAsync(int userId)
        {
            var key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_CLAIMS_BY_ID, userId);
            var group = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_GROUP_BY_ID, userId);            

            var userClaims = await cache.GetOrAddAsync(group, key, 
                () => db.FetchAsync<ApplicationUserClaim>("WHERE UserId=@0", userId), 
                CacheHelper.CacheDurationMedium);
            
            return userClaims.Select(x => new Claim(x.ClaimType, x.ClaimValue)).ToList();
        }

        public Task<int> InsertUserClaimsAsync(int userId, IEnumerable<Claim> claims)
        {
            if (userId <= 0 || claims == null) return Task.FromResult(0);

            var saveClaims = claims.Select(x => new ApplicationUserClaim { UserId = userId, ClaimType = x.Type, ClaimValue = x.Value });
            db.InsertBatch(saveClaims);
            AfterCRUD(userId);
            return Task.FromResult(1);
        }

        public Task<int> DeleteUserClaimsAsync(int userId, IEnumerable<Claim> claims)
        {
            if (userId <= 0 || claims == null) return Task.FromResult(0);
            using (var transaction = db.GetTransaction())
            {
                foreach (var claim in claims)
                {
                    db.DeleteWhere<ApplicationUserClaim>("UserId = @0 AND ClaimType = @1 AND ClaimValue = @2",
                            userId, claim.Type, claim.Value);
                }

                transaction.Complete();
            }
                
            AfterCRUD(userId);
            return Task.FromResult(1);
        }

        private void AfterCRUD(int userId)
        {
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_CLAIMS_BY_ID, userId));
            cache.RemoveByGroup(CacheNames.IDENTITY_USERS_ALL_CLAIM_GROUP);
        }
    }
}
