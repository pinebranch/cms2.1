﻿using Microsoft.AspNetCore.Identity;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Data.TvgConstants;
using TVG.Core.Caching;
using TVG.Identity.Entities;

namespace TVG.Identity.Services
{
    public class IdentityUserLoginService
    {
        private readonly IDatabase db;
        private readonly ICacheManager cache;

        public IdentityUserLoginService(IDatabase db, ICacheManager cache)
        {
            this.db = db;
            this.cache = cache;
            cache.DatabaseNumber = RedisDatabases.USERS;
        }

        public Task AddLoginAsync(ApplicationUser user, UserLoginInfo login)
        {
            if (user == null || login == null) return Task.FromResult(0);

            var userLogin = new ApplicationUserLogin {
                LoginProvider = login.LoginProvider,
                ProviderDisplayName = login.ProviderDisplayName,
                ProviderKey = login.ProviderKey,
                UserId = user.Id
            };
            
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_LOGINS_BY_ID, user.Id));
            
            return db.InsertAsync(userLogin);
        }

        public Task<ApplicationUser> FindByLoginAsync(string loginProvider, string providerKey)
        {
            if (string.IsNullOrEmpty(loginProvider) || string.IsNullOrEmpty(providerKey)) return Task.FromResult<ApplicationUser>(null);

            var key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_BY_LOGIN, loginProvider, providerKey);
            var group = CacheNames.IDENTITY_USER_LOGIN_GROUP;
            var sql = Sql.Builder
                .Select($"u.*")
                .From($"{TableNames.TABLE_USERS} u")
                .InnerJoin($"{TableNames.TABLE_USER_LOGINS} ul").On($"u.Id = ul.UserId")
                .Where($"ul.LoginProvider = @0 AND ul.ProviderKey = @1", loginProvider, providerKey);
            return cache.GetOrAddAsync(group, key, 
                () => db.FirstOrDefaultAsync<ApplicationUser>(sql), 
                CacheHelper.CacheDurationMedium);
        }

        public async Task<IList<UserLoginInfo>> GetLoginsAsync(ApplicationUser user)
        {
            var key = CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_LOGINS_BY_ID, user.Id);
            var group = CacheNames.IDENTITY_USER_LOGIN_GROUP;
            var logins = await cache.GetOrAddAsync(group, key, 
                () => db.FetchAsync<ApplicationUserLogin>("WHERE UserId=@0", user.Id), 
                CacheHelper.CacheDurationMedium);

            var result = logins.Select(x => new UserLoginInfo(x.LoginProvider, x.ProviderKey, x.ProviderDisplayName)).ToList();

            return result;
        }

        public Task<int> RemoveLoginAsync(ApplicationUser user, string loginProvider, string providerKey)
        {
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_BY_LOGIN, loginProvider, providerKey));
            cache.Remove(CacheHelper.SetCacheName(CacheNames.IDENTITY_USER_LOGINS_BY_ID, user.Id));
            
            return db.ExecuteAsync($"DELETE FROM {TableNames.TABLE_USER_LOGINS} WHERE LoginProvider = @0 AND ProviderKey = @1 AND UserId = @2",
                loginProvider, providerKey, user.Id);
        }
    }    
}
