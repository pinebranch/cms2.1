﻿using Microsoft.AspNetCore.Identity;
using NPoco;
using System;
using System.Threading;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Extensions;
using TVG.Identity.Entities;
using TVG.Identity.Services;

namespace TVG.Identity.Stores
{
    public class RoleStore : IRoleStore<ApplicationRole>
    {
        private readonly IdentityRoleService roleService;

        public RoleStore(IDatabase database, ICacheManager cacheManager)
        {
            this.roleService = new IdentityRoleService(database, cacheManager);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));

            var result = await roleService.InsertRoleAsync(role);

            if (result > 0) return IdentityResult.Success;
            return IdentityResult.Failed(new IdentityError { Description = $"Could not insert role {role.Name}." });
        }        

        public async Task<IdentityResult> DeleteAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));

            var result = await roleService.DeleteRoleAsync(role);

            if (result > 0) return IdentityResult.Success;
            return IdentityResult.Failed(new IdentityError { Description = $"Could not delete role {role.Name}." });
        }

        public Task<ApplicationRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var rId = roleId.TryToInt();
            if (rId <= 0) throw new ArgumentOutOfRangeException(nameof(roleId));

            return roleService.GetRoleByIdAsync(rId);
        }

        public Task<ApplicationRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (string.IsNullOrWhiteSpace(normalizedRoleName)) throw new ArgumentOutOfRangeException(nameof(normalizedRoleName));

            return roleService.GetRoleByNameAsync(normalizedRoleName);
        }

        public Task<string> GetNormalizedRoleNameAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));

            return Task.FromResult(role.NormalizedName);
        }

        public Task<string> GetRoleIdAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));

            return Task.FromResult(role.Id.ToString());
        }

        public Task<string> GetRoleNameAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));

            return Task.FromResult(role.Name);
        }

        public Task SetNormalizedRoleNameAsync(ApplicationRole role, string normalizedName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));
            if (string.IsNullOrWhiteSpace(normalizedName)) throw new ArgumentOutOfRangeException(nameof(normalizedName));

            role.NormalizedName = normalizedName;

            return Task.FromResult(0);
        }

        public Task SetRoleNameAsync(ApplicationRole role, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));
            if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentOutOfRangeException(nameof(roleName));

            role.Name = roleName;

            return Task.FromResult(0);
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (role == null) throw new ArgumentNullException(nameof(role));

            // Update concurrency stamp for optimistic concurrency
            var originalStamp = role.ConcurrencyStamp;
            role.ConcurrencyStamp = Guid.NewGuid().ToString();

            var result = await roleService.UpdateRoleAsync(role);

            if (result > 0) return IdentityResult.Success;
            return IdentityResult.Failed(new IdentityError { Description = $"Could not update role {role.Name}." });
        }

        #region Disposal

        ~RoleStore()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            // Unused
        }

        #endregion
    }
}
