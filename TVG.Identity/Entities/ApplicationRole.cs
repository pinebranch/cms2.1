﻿using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_ROLES)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ApplicationRole
    {
        public ApplicationRole()
        {

        }

        public ApplicationRole(string name) : this()
        {
            Name = name;
        }

        public ApplicationRole(string name, int id)
        {
            Name = name;
            Id = id;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string ConcurrencyStamp { get; set; }
        public int DisplayOrder { get; set; }
    }
}
