﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_USER_LOGINS)]
    [PrimaryKey("LoginProvider,ProviderKey", AutoIncrement = false)]
    public class ApplicationUserLogin
    {
        public int UserId { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string ProviderDisplayName { get; set; }
    }
}
