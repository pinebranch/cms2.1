﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_USER_CLAIMS)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ApplicationUserClaim
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}
