﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_USERS)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ApplicationUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public virtual bool EmailConfirmed { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual bool PhoneNumberConfirmed { get; set; }
        public virtual bool TwoFactorEnabled { get; set; }
        public virtual DateTime? LockoutEnd { get; set; }
        public virtual bool LockoutEnabled { get; set; }
        public virtual int AccessFailedCount { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual int CreatedBy { get; set; }
        public virtual DateTime UpdatedDate { get; set; }
        public virtual int UpdatedBy { get; set; }
        public virtual DateTime LastActiveDate { get; set; }
        public bool IsDeleted { get; set; }

        [Ignore]
        public bool IsLockedOut => LockoutEnabled && LockoutEnd > DateTime.UtcNow;
    }

    public class ApplicationUserQueryParameters
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = int.MaxValue;
        public string Name { get; set; } = "";
        public string Email { get; set; } = "";
    }
}
