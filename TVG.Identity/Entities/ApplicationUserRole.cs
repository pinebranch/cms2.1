﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_USER_ROLES)]
    [PrimaryKey("UserId,RoleId", AutoIncrement = false)]
    public class ApplicationUserRole
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
