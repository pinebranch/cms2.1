﻿using NPoco;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_ROLE_CLAIMS)]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ApplicationRoleClaim
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}
