﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Data.TvgConstants;

namespace TVG.Identity.Entities
{
    [TableName(TableNames.TABLE_USER_TOKENS)]
    [PrimaryKey("UserId, LoginProvider, Name", AutoIncrement = false)]
    public class ApplicationUserToken
    {
        public int UserId { get; set; }
        public string LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
