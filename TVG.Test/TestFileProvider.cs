﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Configuration;
using Xunit;
using Xunit.Abstractions;

namespace TVG.Test
{
    public class TestFileProvider
    {
        private readonly TvgAppSettings configuration;
        private readonly ITestOutputHelper output;
        private readonly IServiceProvider serviceProvider;

        public TestFileProvider(ITestOutputHelper output)
        {
            this.output = output;
            this.serviceProvider = TestHelper.SetupServices();
            this.configuration = serviceProvider.GetRequiredService<TvgAppSettings>();
        }

        [Theory]
        [InlineData("", "abc")]
        [InlineData("", "New Text Document.txt")]
        [InlineData("", "abc/")]
        [InlineData("", "New Text Document.txt/")]
        public void Test_PathIsDirectory(string path, string file)
        {
            var uploadFolder = new PhysicalFileProvider(configuration.UploadFolderConfig.Path);
            var fullPath = Path.Combine(uploadFolder.Root, path, file);
            var fileInfo = System.IO.File.GetAttributes(fullPath);
            Assert.True(fileInfo == FileAttributes.Directory);
        }

        [Theory]
        [InlineData("", "abc")]
        [InlineData("", "New Text Document.txt")]
        public void Test_FileExists(string path, string file)
        {
            var uploadFolder = new PhysicalFileProvider(configuration.UploadFolderConfig.Path);
            var fullPath = Path.Combine(uploadFolder.Root, path, file);
            Assert.True(System.IO.Directory.Exists(fullPath) || System.IO.File.Exists(fullPath));
        }
    }
}
