﻿using Microsoft.Extensions.DependencyInjection;
using NPoco;
using System;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Configuration;
using TVG.Identity.Services;
using Xunit;
using Xunit.Abstractions;

namespace TVG.Test
{
    public class TestIdentityUserClaimService
    {
        private TvgAppSettings configuration;
        private readonly ITestOutputHelper output;
        private IDatabase db;
        private IServiceProvider serviceProvider;
        private ICacheManager cacheManager;
        private IdentityUserClaimService userClaimService;

        public TestIdentityUserClaimService(ITestOutputHelper output)
        {
            this.output = output;
            this.serviceProvider = TestHelper.SetupServices();
            this.configuration = serviceProvider.GetRequiredService<TvgAppSettings>();
            this.db = serviceProvider.GetRequiredService<IDatabase>();
            this.cacheManager = serviceProvider.GetRequiredService<ICacheManager>();
            this.userClaimService = serviceProvider.GetRequiredService<IdentityUserClaimService>();
        }

        [Theory]
        [InlineData(-1, false)]
        [InlineData(0, false)]
        [InlineData(1, false)]
        public async Task Test_GetClaimsByUserIdAsync(int value, bool result)
        {
            var list = await userClaimService.GetClaimsByUserIdAsync(value).ConfigureAwait(false);
            Assert.Equal(list.Count > 0, result);
        }
    }
}
