﻿using System;
using System.Collections.Generic;
using System.Text;
using TVG.Test.Entities;
using Xunit;

namespace TVG.Test
{
    public class SimpleTest
    {
        [Fact]
        public void Test_Find()
        {
            var studentList = new List<Student>()
            {
                new Student {Id = 1, Name = "st1"},
                new Student {Id = 2, Name = "st2"},
                new Student {Id = 3, Name = "st3"},
                new Student {Id = 4, Name = "st4"}
            };

            var student = studentList.Find(x => x.Id == 1);
        }

    }
}
