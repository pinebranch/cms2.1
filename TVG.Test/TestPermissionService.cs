﻿using Microsoft.Extensions.DependencyInjection;
using NPoco;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Configuration;
using TVG.Data.Domain;
using TVG.Data.Services;
using TVG.Identity.Services;
using Xunit;
using Xunit.Abstractions;

namespace TVG.Test
{
    public class TestPermissionService
    {
        private readonly TvgAppSettings configuration;
        private readonly ITestOutputHelper output;
        private readonly IDatabase db;
        private readonly IServiceProvider serviceProvider;
        private readonly ICacheManager cacheManager;
        private readonly PermissionService permissionService;
        private readonly IdentityRoleService roleService;

        public TestPermissionService(ITestOutputHelper output)
        {
            this.output = output;
            this.serviceProvider = TestHelper.SetupServices();
            this.configuration = serviceProvider.GetRequiredService<TvgAppSettings>();
            this.db = serviceProvider.GetRequiredService<IDatabase>();
            this.cacheManager = serviceProvider.GetRequiredService<ICacheManager>();
            this.permissionService = serviceProvider.GetRequiredService<PermissionService>();
            this.roleService = serviceProvider.GetRequiredService<IdentityRoleService>();
        }
        

        [Theory]
        [InlineData(1, 5)]
        [InlineData(2, 3)]
        [InlineData(3, 2)]
        [InlineData(4, 0)]
        public async Task Test_PermissionsByUser2(int value, int result)
        {
            var roles = await roleService.GetRolesByUserId(value);
            var permissions = new List<PermissionRole>();
            foreach(var role in roles)
            {
                permissions.AddRange(await permissionService.GetPermissionsByRoleAsync(role.Id));
            }
            Assert.Equal(permissions.Count, result);
        }
    }
}
