﻿
using GenFu;
using Microsoft.Extensions.DependencyInjection;
using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Configuration;
using TVG.Core.Extensions;
using TVG.Data.Domain;
using TVG.Data.Services;
using Xunit;
using Xunit.Abstractions;

namespace TVG.Test
{
    public class TestTaxonomyService
    {
        private readonly TvgAppSettings configuration;
        private readonly ITestOutputHelper output;
        private readonly IDatabase db;
        private readonly IServiceProvider serviceProvider;
        private readonly ICacheManager cacheManager;
        private readonly TaxonomyService taxonomyService;

        public TestTaxonomyService(ITestOutputHelper output)
        {
            this.output = output;
            this.serviceProvider = TestHelper.SetupServices();
            this.configuration = serviceProvider.GetRequiredService<TvgAppSettings>();
            this.db = serviceProvider.GetRequiredService<IDatabase>();
            this.cacheManager = serviceProvider.GetRequiredService<ICacheManager>();
            this.taxonomyService = serviceProvider.GetRequiredService<TaxonomyService>();
        }

        [Fact]
        public async Task Test_InsertTaxonomy()
        {
            var obj = new Taxonomy
            {
                Name = "category",
                DisplayName = "Chuyên mục",
                IsHierarchy = true,
                HasThumbnail = false,
                HasColor = false,
                Note = ""
            };

            var result = await taxonomyService.InsertTaxonomyAsync(obj);
            Assert.True(result > 0);
        }

        [Theory]
        [InlineData("category")]
        [InlineData("tag")]
        public async Task Test_CheckTaxonomyExists(string name)
        {
            Assert.True(await taxonomyService.IsTaxonomyExistsAsync(name).ConfigureAwait(false));
        }

        [Fact]
        public async Task Test_GetTaxonomies()
        {
            var list = await taxonomyService.GetTaxonomiesAsync();
            Assert.True(list.Count == 1);
        }

        [Fact]
        public async Task Test_GetTaxonomy()
        {
            var obj = await taxonomyService.GetTaxonomyAsync("category");
            Assert.False(!obj.HasThumbnail);
        }

        [Fact]
        public async Task Test_UpdateTaxonomy()
        {
            var obj = await taxonomyService.GetTaxonomyAsync("category");
            obj.HasThumbnail = true;

            var result = await taxonomyService.UpdateTaxonomyAsync(obj);
            Assert.True(obj.HasThumbnail);
        }

        [Fact]
        public async Task Test_DeleteTaxonomy()
        {
            var obj = await taxonomyService.GetTaxonomyAsync("category");
            var result = await taxonomyService.DeleteTaxonomyAsync(obj.Id);

            Assert.True(result > 0);
        }

        [Theory]
        [InlineData("category")]
        [InlineData("tag")]
        public async Task Test_Term_GetTerms(string taxonomy)
        {
            var parameters = new TermQueryParameters { Taxonomy = taxonomy };
            var list = await taxonomyService.GetTermsAsync(parameters);
            output.WriteLine(PrintTermList(list.Items));

            Assert.True(list.Items.Count > 0);
        }

        [Theory]
        [InlineData("category")]
        [InlineData("tag")]
        public async Task Test_Term_GetTermsList(string taxonomy)
        {
            var list = await taxonomyService.GetTermsAsync(taxonomy);
            output.WriteLine(PrintTermList(list));

            Assert.True(list.Count > 0);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        public async Task Test_Term_GetTerm(int value)
        {
            var obj = await taxonomyService.GetTermByIdAsync(value);

            Assert.True(obj != null);
        }

        [Fact]
        public async Task Test_Term_InsertNull()
        {
            var result = await taxonomyService.InsertTermAsync(null);
            Assert.True(result == 0);
        }

        [Fact]
        public async Task Test_Term_InsertTerm()
        {
            var obj = A.New<Term>();
            obj.Taxonomy = "category";
            obj.CreatedBy = 1;
            obj.UpdatedBy = 1;
            obj.IsDeleted = false;
            var result = await taxonomyService.InsertTermAsync(obj);
            Assert.True(result > 0);
        }

        [Fact]
        public async Task Test_Term_InsertTerms()
        {
            var list = A.ListOf<Term>(10);
            foreach(var obj in list)
            {
                obj.Taxonomy = "tag";
                obj.ParentId = 0;
                obj.CreatedBy = 1;
                obj.UpdatedBy = 1;
                obj.IsDeleted = false;
                var result = await taxonomyService.InsertTermAsync(obj);
            }
            
            Assert.True(1 > 0);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task Test_Term_UpdateTerm(int id)
        {
            var obj = await taxonomyService.GetTermByIdAsync(id);
            obj.UpdatedDate = DateTime.Now;
            var result = await taxonomyService.UpdateTermAsync(obj);
            Assert.True(result > 0);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task Test_Term_DeleteTerm(int id)
        {
            var result = await taxonomyService.DeleteTermAsync(id);

            var obj = await taxonomyService.GetTermByIdAsync(id);
            Assert.True(obj.IsDeleted == true);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task Test_Term_DeleteTermPermanent(int id)
        {
            var result = await taxonomyService.DeleteTermAsync(id, true);

            var obj = await taxonomyService.GetTermByIdAsync(id);
            Assert.True(obj == null);
        }

        [Fact]
        public void Test_Term_Fake()
        {
            var obj = A.New<Term>();
            output.WriteLine(PrintTerm(obj));
            Assert.True(!string.IsNullOrEmpty(obj.Name));
        }

        private string PrintTerm(Term obj)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Id:").Append(obj.Id.ToString()).Append(Environment.NewLine);
            sb.Append("Parent:").Append(obj.ParentId).Append(Environment.NewLine);
            sb.Append("Name:").Append(obj.Name).Append(Environment.NewLine);
            sb.Append("Taxonomy:").Append(obj.Taxonomy).Append(Environment.NewLine);
            sb.Append("Description:").Append(obj.Description).Append(Environment.NewLine);
            sb.Append("MetaDescription:").Append(obj.MetaDescription).Append(Environment.NewLine);
            sb.Append("MetaKeyword:").Append(obj.MetaKeyword).Append(Environment.NewLine);
            sb.Append("DisplayOrder:").Append(obj.DisplayOrder).Append(Environment.NewLine);
            sb.Append("CreatedDate:").Append(obj.CreatedDate).Append(Environment.NewLine);
            sb.Append("UpdatedDate:").Append(obj.UpdatedDate).Append(Environment.NewLine);

            return sb.ToString();
        }

        private string PrintTermList(List<Term> list)
        {
            var sb = new StringBuilder();
            foreach(var item in list)
            {
                sb.Append($"{item.Id}: ").Append(item.Name.Prepend("-", item.Level)).AppendLine();
            }
            return sb.ToString();
        }
    }
}
