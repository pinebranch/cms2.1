﻿using GenFu;
using Microsoft.Extensions.DependencyInjection;
using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TVG.Core;
using TVG.Core.Caching;
using TVG.Core.Configuration;
using TVG.Data.Domain;
using TVG.Data.Services;
using Xunit;
using Xunit.Abstractions;
using ChanceNET;
using NLipsum.Core;

namespace TVG.Test
{
    public class TestPostService
    {
        private readonly TvgAppSettings configuration;
        private readonly ITestOutputHelper output;
        private readonly IDatabase db;
        private readonly IServiceProvider serviceProvider;
        private readonly ICacheManager cacheManager;
        private readonly PostService postService;

        public TestPostService(ITestOutputHelper output)
        {
            this.output = output;
            this.serviceProvider = TestHelper.SetupServices();
            this.configuration = serviceProvider.GetRequiredService<TvgAppSettings>();
            this.db = serviceProvider.GetRequiredService<IDatabase>();
            this.cacheManager = serviceProvider.GetRequiredService<ICacheManager>();
            this.postService = serviceProvider.GetRequiredService<PostService>();
        }

        [Fact]
        public void Test_ChanceParagraph()
        {
            Chance chance = new Chance();
            var text = chance.Paragraph();
            output.WriteLine(text);
            Assert.True(text.Length > 0);
        }

        [Fact]
        public void Test_NLipsum()
        {
            string rawText = Lipsums.TheRaven;
            LipsumGenerator lipsum = new LipsumGenerator(rawText, false);
            string text = lipsum.GenerateLipsum(2);

            output.WriteLine(text);
            Assert.True(text.Length > 0);
        }

        public async Task Test_InsertPosts()
        {
            A.Configure<Post>()
                .Fill(x => x.Id, 0)
                .Fill(x => x.PostType, "post")
                .Fill(x => x.ParentId, 0)
                .Fill(x => x.Image, "")
                .Fill(x => x.PublishStatus, 1)
                .Fill(x => x.CreatedBy, 1)
                .Fill(x => x.UpdatedBy, 1)
                .Fill(x => x.IsDeleted, false)
                .Fill(x => x.Content, new LipsumGenerator(Lipsums.RobinsonoKruso, false).GenerateLipsum(3));
            var list = A.ListOf<Post>(15);
            foreach (var obj in list)
            {
                await postService.InsertPostAsync(obj).ConfigureAwait(false);
            }

            return;
        }

        [Fact]
        public async Task Test_GetPosts()
        {
            var result = await postService.GetPostsAsync(new PostQueryParameters());
            Assert.True(result.TotalItems > 0);
        }

        [Fact]
        public async Task Test_UpdatePost()
        {
            var list = await postService.GetPostsAsync(new PostQueryParameters());
            var count = 0;
            foreach(var item in list.Items)
            {
                item.Content = new LipsumGenerator(Lipsums.RobinsonoKruso, false).GenerateLipsum(3);
                count += await postService.UpdatePostAsync(item).ConfigureAwait(false);                
            }

            Assert.Equal(count, list.TotalItems);
        }

        [Fact]
        public async Task Test_DeletePost()
        {
            int id = 1;
            var obj = await postService.GetPostByIdAsync(id).ConfigureAwait(false);
            Assert.NotNull(obj);

            await postService.DeletePostAsync(id).ConfigureAwait(false);
            var obj2 = await postService.GetPostByIdAsync(id).ConfigureAwait(false);
            Assert.NotNull(obj2);
            Assert.True(obj2.IsDeleted);
        }

        [Fact]
        public async Task Test_DeletePostPermanent()
        {
            int id = 1;
            var obj = await postService.GetPostByIdAsync(id).ConfigureAwait(false);
            Assert.NotNull(obj);

            await postService.DeletePostAsync(id, true).ConfigureAwait(false);
            var obj2 = await postService.GetPostByIdAsync(id).ConfigureAwait(false);
            Assert.Null(obj2);
        }
    }
}
