using Microsoft.Extensions.DependencyInjection;
using NPoco;
using System;
using System.Threading.Tasks;
using TVG.Core.Caching;
using TVG.Core.Configuration;
using TVG.Identity.Services;
using Xunit;
using Xunit.Abstractions;

namespace TVG.Test
{
    public class TestIdentityUserService
    {
        private TvgAppSettings configuration;
        private readonly ITestOutputHelper output;
        private IDatabase db;
        private IServiceProvider serviceProvider;
        private ICacheManager cacheManager;
        private IdentityUserService userService;

        public TestIdentityUserService(ITestOutputHelper output)
        {
            this.output = output;
            this.serviceProvider = TestHelper.SetupServices();
            this.configuration = serviceProvider.GetRequiredService<TvgAppSettings>();
            this.db = serviceProvider.GetRequiredService<IDatabase>();
            this.cacheManager = serviceProvider.GetRequiredService<ICacheManager>();
            this.userService = serviceProvider.GetRequiredService<IdentityUserService>();
        }
        

        [Theory]
        [InlineData(-1, false)]
        [InlineData(0, false)]
        [InlineData(1, true)]
        public async Task Test_GetUserById(int value, bool expectedUserExists)
        {
            var user = await userService.GetUserByIdAsync(value);
            Assert.Equal(user.UserName == "thanhpc", expectedUserExists);
        }

        [Theory]
        [InlineData("test", false)]
        [InlineData(null, false)]
        [InlineData("thanhpc", true)]
        public async Task Test_GetUserByName(string value, bool expectedUserExists)
        {
            var user = await userService.GetUserByNameAsync(value);
            Assert.Equal(user != null, expectedUserExists);
        }

        [Theory]
        [InlineData("test", false)]
        [InlineData(null, false)]
        [InlineData("pinebranch@gmail.com", true)]
        public async Task Test_GetUserByEmail(string value, bool expectedUserExists)
        {
            var user = await userService.GetUserByEmailAsync(value);
            Assert.Equal(user != null, expectedUserExists);
        }
    }
}
