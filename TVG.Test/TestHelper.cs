﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using NLog.Extensions.Logging;
using NPoco;
using System;
using TVG.Core.Caching;
using TVG.Core.Configuration;
using TVG.Data.Services;
using TVG.Identity.Services;

namespace TVG.Test
{
    public class TestHelper
    {
        public static IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

        public static TvgAppSettings GetApplicationConfiguration(string outputPath)
        {
            var configuration = new TvgAppSettings();

            var iConfig = GetIConfigurationRoot(outputPath);

            iConfig
                .GetSection("TvgAppSettings")
                .Bind(configuration);

            return configuration;
        }

        public static IServiceProvider SetupServices()
        {
            var services = new ServiceCollection();

            var configuration = TestHelper.GetApplicationConfiguration(AppContext.BaseDirectory);
            // Simple configuration object injection (no IOptions<T>)
            services.AddSingleton(configuration);

            services.AddSingleton<IDatabase>(t => {
                return new Database(configuration.ConnectionStrings.MySqlConnection,
                    DatabaseType.MySQL, MySqlClientFactory.Instance);
            });

            // has a depedency on DbContext and Configuration
            services.AddTransient<ICacheManager, RedisCacheManager>();

            SetDataServices(services);
            SetLogger(services);

            // Build the service provider
            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            NLog.LogManager.LoadConfiguration("nlog.config");

            return serviceProvider;
        }

        protected static void SetDataServices(ServiceCollection services)
        {
            services.AddTransient<IdentityUserService>();
            services.AddTransient<IdentityUserClaimService>();
            services.AddTransient<IdentityRoleService>();
            services.AddTransient<IdentityUserLoginService>();
            services.AddTransient<PermissionService>();
            services.AddTransient<TaxonomyService>();
            services.AddTransient<PostService>();
        }

        protected static void SetLogger(ServiceCollection services)
        {
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Trace));
        }
    }
}
