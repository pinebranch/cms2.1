﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TVG.Core
{
    public static class MyUrlHelper
    {
        public static string RemoveKeyFromQueryString(string url, string key)
        {
            if (string.IsNullOrEmpty(key)) return url;

            var uri = new Uri(url);
            var baseUri = uri.GetComponents(UriComponents.Scheme | UriComponents.Host | UriComponents.Port | UriComponents.Path, UriFormat.UriEscaped);

            var query = QueryHelpers.ParseQuery(uri.Query);
            var items = query.SelectMany(item => item.Value, (i, iValue) => new KeyValuePair<string, string>(i.Key, iValue)).ToList();

            items.RemoveAll(x => x.Key == key);

            var qb = new QueryBuilder(items);

            var fullUri = baseUri + qb.ToQueryString();

            return fullUri;
        }

        public static string AddKeyToQueryString(string url, string key, string value)
        {
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value)) return url;

            var uri = new Uri(url);
            var baseUri = uri.GetComponents(UriComponents.Scheme | UriComponents.Host | UriComponents.Port | UriComponents.Path, UriFormat.UriEscaped);

            var query = QueryHelpers.ParseQuery(uri.Query);
            var items = query.SelectMany(item => item.Value, (i, iValue) => new KeyValuePair<string, string>(i.Key, iValue)).ToList();

            var qb = new QueryBuilder(items)
            {
                { key, value }
            };

            var fullUri = baseUri + qb.ToQueryString();

            return fullUri;
        }

        public static string AddKeysToQueryString(string url, Dictionary<string, string> keyValuePairs)
        {
            if (keyValuePairs == null || keyValuePairs.Count == 0) return url;

            var uri = new Uri(url);
            var baseUri = uri.GetComponents(UriComponents.Scheme | UriComponents.Host | UriComponents.Port | UriComponents.Path, UriFormat.UriEscaped);

            var query = QueryHelpers.ParseQuery(uri.Query);
            var items = query.SelectMany(item => item.Value, (i, iValue) => new KeyValuePair<string, string>(i.Key, iValue)).ToList();

            var qb = new QueryBuilder(items);
            foreach(var item in keyValuePairs)
            {
                qb.Add(item.Key, item.Value);
            }

            var fullUri = baseUri + qb.ToQueryString();

            return fullUri;
        }

        public static string UpdateKeyInQueryString(string url, string key, string value)
        {
            if (string.IsNullOrEmpty(key)) return url;

            var uri = new Uri(url);
            var baseUri = uri.GetComponents(UriComponents.Scheme | UriComponents.Host | UriComponents.Port | UriComponents.Path, UriFormat.UriEscaped);

            var query = QueryHelpers.ParseQuery(uri.Query);
            var items = query.SelectMany(item => item.Value, (i, iValue) => new KeyValuePair<string, string>(i.Key, iValue)).ToList();

            items.RemoveAll(x => x.Key == key);

            var qb = new QueryBuilder(items);
            if (!string.IsNullOrEmpty(value))
            {
                qb.Add(key, value);
            }            

            var fullUri = baseUri + qb.ToQueryString();

            return fullUri;
        }
    }
}
