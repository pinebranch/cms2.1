﻿using System;
using System.Text.RegularExpressions;

namespace TVG.Core
{
    public static class StringHelper
    {
        public static string Slugify(string str)
        {
            if (string.IsNullOrEmpty(str)) return "";

            string strFormD = str.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormD);
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            str = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            str = Regex.Replace(str, "[^a-zA-Z0-9]+", " ", RegexOptions.Compiled).Replace(' ', '-');
            return str;
        }
    }
}
