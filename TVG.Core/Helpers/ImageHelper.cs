﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System.IO;
using System.Linq;

namespace TVG.Core.Helpers
{
    public static class ImageHelper
    {
        public static void ResizeImage(string filePath, string newPath, int width, int height, bool keepAspectRatio = true)
        {
            if (!File.Exists(filePath)) return;

            //kiem tra dinh dang file
            string ext = Path.GetExtension(filePath);
            if (!ImageHelper.IsImage(ext)) return;

            var size = new SixLabors.Primitives.Size
            {
                Width = width,
                Height = keepAspectRatio ? 0 : height
            };

            using (Image<Rgba32> image = Image.Load(filePath))
            {
                image.Mutate(x =>
                    x.Resize(new ResizeOptions { Size = size }));
                image.Save(newPath);
            }
        }

        public static bool IsImage(string ext)
        {
            string[] _validExtensions = { ".jpg", ".jpeg", ".gif", ".png" };
            return _validExtensions.Any(x => x.Contains(ext.ToLower()));
        }
    }
}
