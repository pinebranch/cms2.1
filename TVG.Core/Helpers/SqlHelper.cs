﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Helpers
{
    public class SqlHelper
    {
        public static string GenerateMySqlFtsPhrase(string ftsCols, string keyword)
        {
            return $" MATCH({ftsCols}) AGAINST ('{keyword}') ";
        }

        public static string GenerateMySqlFtsRankColumn(string ftsCols, string keyword, string rankColName)
        {
            return $" {GenerateMySqlFtsPhrase(ftsCols, keyword)} AS {rankColName} ";
        }

        public static string GenerateMySqlFtsWhere(string ftsCols, string keyword)
        {
            return $" {GenerateMySqlFtsPhrase(ftsCols, keyword)} > 0 ";
        }
    }
}
