﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NWebsec.Core.Common.Middleware.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TVG.Core.Extensions;
using TVG.Core.Middleware;

namespace TVG.Core.Configuration
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseNoServerHttpHeader(this IApplicationBuilder application)
        {
            return application.UseMiddleware<NoServerHttpHeaderMiddleware>();
        }

        public static IApplicationBuilder UseStaticFilesWithCacheControl(this IApplicationBuilder application)
        {
            return application
                .UseStaticFiles(new StaticFileOptions()
                {
                    OnPrepareResponse = context =>
                    {
                        const int cachePeriod = 60;
                        context.Context.Response.Headers.Append("Cache-Control", $"public, max-age={cachePeriod}");
                    }
                });
        }

        public static IApplicationBuilder UseSecurityHttpHeaders(this IApplicationBuilder application) =>
           application
               // X-Content-Type-Options - Adds the X-Content-Type-Options HTTP header. Stop IE9 and below from
               //                          sniffing files and overriding the Content-Type header (MIME type).
               .UseXContentTypeOptions()
               // X-Download-Options - Adds the X-Download-Options HTTP header. When users save the page, stops them
               //                      from opening it and forces a save and manual open.
               .UseXDownloadOptions()
               // X-Frame-Options - Adds the X-Frame-Options HTTP header. Stop clickjacking by stopping the page from
               //                   opening in an iframe or only allowing it from the same origin.
               //   SameOrigin - Specifies that the X-Frame-Options header should be set in the HTTP response,
               //                instructing the browser to display the page when it is loaded in an iframe - but only
               //                if the iframe is from the same origin as the page.
               //   Deny - Specifies that the X-Frame-Options header should be set in the HTTP response, instructing
               //          the browser to not display the page when it is loaded in an iframe.
               .UseXfo(options => options.SameOrigin());

        public static IApplicationBuilder UseInternalServerErrorOnException(this IApplicationBuilder application)
        {
            if (application == null)
            {
                throw new ArgumentNullException(nameof(application));
            }

            return application.UseMiddleware<InternalServerErrorOnExceptionMiddleware>();
        }
    }
    
}
