﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Configuration
{
    public class ResponseCompressionSettings
    {
        public string[] MimeTypes { get; set; }
    }
}
