﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using NPoco;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using TVG.Core.Caching;
using TVG.Core.Email;

namespace TVG.Core.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAntiforgerySecurely(this IServiceCollection services)
        {
            services.AddAntiforgery(options => {
                options.Cookie.Name = "f";
                options.FormFieldName = "f";
                options.HeaderName = "X-XSRF-TOKEN";
            });

            return services;
        }

        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            var config = new TvgAppSettings();
            configuration.Bind("TvgAppSettings", config);
            services.AddSingleton(config);

            return services;
        }

        public static IServiceCollection AddCaching(this IServiceCollection services)
        {
            services.AddTransient<ICacheManager, RedisCacheManager>();
            return services;
        }

        public static IServiceCollection AddDatabaseProvider(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<NPoco.IDatabase>(t => {
                return new Database(configuration.GetValue<string>("TvgAppSettings:ConnectionStrings:MySqlConnection"),
                    DatabaseType.MySQL, MySqlClientFactory.Instance);
            });

            return services;
        }

        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services)
        {
            services.AddTransient<IEmailSender, TvgEmailSender>();
            return services;
        }
    }
}
