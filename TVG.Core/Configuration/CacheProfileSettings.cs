﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Configuration
{
    public class CacheProfileSettings
    {
        public Dictionary<string, CacheProfile> CacheProfiles { get; set; }
    }
}
