﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TVG.Core.Configuration
{
    public class TvgAppSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        public int CacheDurationLong { get; set; }
        public int CacheDurationMedium { get; set; }
        public int CacheDurationShort { get; set; }
        public string DeveloperNames { get; set; }

        public UploadFolderConfig UploadFolderConfig { get; set; }
        public EmailConfig EmailConfig { get; set; }

        public static TvgAppSettings Current;

        public TvgAppSettings()
        {
            Current = this;
        }
    }

    public class ConnectionStrings
    {
        public string MySqlConnection { get; set; }
        public string RedisConnection { get; set; }
    }
    
    public class EmailConfig
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public bool SmtpSSL { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public string DefaultSender { get; set; }
    }

    public class UploadFolderConfig
    {
        public string Path { get; set; }
        public string Request { get; set; }
    }
}
