﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace TVG.Core.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class NoTrailingSlashAttribute : Attribute, IResourceFilter
    {
        private const char SlashCharacter = '/';

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var path = context.HttpContext.Request.Path;
            if (path.HasValue)
            {
                if (path.Value[path.Value.Length - 1] == SlashCharacter)
                {
                    this.HandleTrailingSlashRequest(context);
                }
            }
        }

        protected virtual void HandleTrailingSlashRequest(ResourceExecutingContext context)
        {
            context.Result = new NotFoundResult();
        }
    }
}
