﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace TVG.Core.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class NoLowercaseQueryStringAttribute : Attribute, IFilterMetadata
    {
    }
}
