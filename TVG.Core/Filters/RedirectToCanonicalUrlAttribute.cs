﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Filters
{
    //Base on class RedirectToCanonicalUrlAttribute of project ASP.NET-MVC-Boilerplate
    //https://github.com/Matthew-Bonner/ASP.NET-MVC-Boilerplate

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class RedirectToCanonicalUrlAttribute : Attribute, IResourceFilter
    {
        #region Fields

        private const char SlashCharacter = '/';

        #endregion

        public bool AppendTrailingSlash { get; }

        public bool LowercaseUrls { get; }

        public RedirectToCanonicalUrlAttribute(IOptions<RouteOptions> options)
            : this(options.Value.AppendTrailingSlash, options.Value.LowercaseUrls)
        {
        }

        public RedirectToCanonicalUrlAttribute(
            bool appendTrailingSlash,
            bool lowercaseUrls)
        {
            this.AppendTrailingSlash = appendTrailingSlash;
            this.LowercaseUrls = lowercaseUrls;
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            if (HttpMethods.IsGet(context.HttpContext.Request.Method))
            {
                if (!this.TryGetCanonicalUrl(context, out string canonicalUrl))
                {
                    this.HandleNonCanonicalRequest(context, canonicalUrl);
                }
            }
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }

        protected virtual bool TryGetCanonicalUrl(ResourceExecutingContext context, out string canonicalUrl)
        {
            bool isCanonical = true;

            var request = context.HttpContext.Request;

            // If we are not dealing with the home page. Note, the home page is a special case and it doesn't matter
            // if there is a trailing slash or not. Both will be treated as the same by search engines.
            if (request.Path.HasValue && (request.Path.Value.Length > 1))
            {
                bool hasTrailingSlash = request.Path.Value[request.Path.Value.Length - 1] == SlashCharacter;

                if (this.AppendTrailingSlash)
                {
                    // Append a trailing slash to the end of the URL.
                    if (!hasTrailingSlash && !this.HasAttribute<NoTrailingSlashAttribute>(context))
                    {
                        request.Path = new PathString(request.Path.Value + SlashCharacter);
                        isCanonical = false;
                    }
                }
                else
                {
                    // Trim a trailing slash from the end of the URL.
                    if (hasTrailingSlash)
                    {
                        request.Path = new PathString(request.Path.Value.TrimEnd(SlashCharacter));
                        isCanonical = false;
                    }
                }

                if (this.LowercaseUrls && !this.HasAttribute<NoTrailingSlashAttribute>(context))
                {
                    foreach (char character in request.Path.Value)
                    {
                        if (char.IsUpper(character))
                        {
                            request.Path = new PathString(request.Path.Value.ToLower());
                            isCanonical = false;
                            break;
                        }
                    }

                    //if (request.QueryString.HasValue && !this.HasAttribute<NoLowercaseQueryStringAttribute>(context))
                    //{
                    //    foreach (char character in request.QueryString.Value)
                    //    {
                    //        if (char.IsUpper(character))
                    //        {
                    //            request.QueryString = new QueryString(request.QueryString.Value.ToLower());
                    //            isCanonical = false;
                    //            break;
                    //        }
                    //    }
                    //}
                }
            }

            if (isCanonical)
            {
                canonicalUrl = null;
            }
            else
            {
                canonicalUrl = request.GetEncodedUrl();
            }

            return isCanonical;
        }

        protected virtual void HandleNonCanonicalRequest(ResourceExecutingContext context, string canonicalUrl)
        {
            context.Result = new RedirectResult(canonicalUrl, true);
        }

        protected virtual bool HasAttribute<T>(ResourceExecutingContext context)
        {
            foreach (IFilterMetadata filterMetadata in context.Filters)
            {
                if (filterMetadata is T)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
