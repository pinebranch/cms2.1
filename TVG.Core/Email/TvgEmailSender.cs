﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Configuration;

namespace TVG.Core.Email
{
    public class TvgEmailSender : IEmailSender
    {
        private readonly TvgAppSettings appSettings;

        public TvgEmailSender(TvgAppSettings appSettings)
        {
            this.appSettings = appSettings;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var client = new SmtpClient(appSettings.EmailConfig.SmtpHost, appSettings.EmailConfig.SmtpPort)
            {
                Credentials = new NetworkCredential(appSettings.EmailConfig.SmtpUsername, appSettings.EmailConfig.SmtpPassword),
                EnableSsl = appSettings.EmailConfig.SmtpSSL
            };
            return client.SendMailAsync(
                new MailMessage(appSettings.EmailConfig.DefaultSender, email, subject, htmlMessage) { IsBodyHtml = true }
            );
        }

        
    }
}
