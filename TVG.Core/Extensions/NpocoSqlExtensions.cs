﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Extensions
{
    public static class NpocoSqlExtensions
    {
        public static Sql WhereIf(this Sql sql, bool condition, string where, params object[] args)
        {
            if (condition)
            {
                sql.Where(where, args);
            }
            return sql;
        }

        public static Sql WhereInInt(this Sql sql, string whereColumn, string inClause)
        {
            if (string.IsNullOrEmpty(inClause)) return sql;
            
            int[] arrInt = Array.ConvertAll(inClause.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
            sql.WhereIf(
                arrInt.Length > 1 || (arrInt.Length == 1 && arrInt[0] != 0),
                string.Format("{0} IN (@0) ", whereColumn),
                arrInt
            );
            
            return sql;
        }

        public static Sql WhereInInt(this Sql sql, string whereColumn, List<string> inClauses)
        {
            if (inClauses == null || inClauses.Count == 0) return sql;

            foreach(var clause in inClauses)
            {
                if (string.IsNullOrEmpty(clause)) continue;

                sql.WhereInInt(whereColumn, clause);
            }
            return sql;
        }

        public static Sql LeftJoinIf(this Sql sql, bool condition, string tableJoin, string onClause)
        {
            if (condition)
            {
                sql.LeftJoin(tableJoin).On(onClause);
            }
            return sql;
        }
    }
}
