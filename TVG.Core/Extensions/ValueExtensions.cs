﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVG.Core.Extensions
{
    public static class ValueExtensions
    {
        #region Convertion
        /// <summary>
        /// TVG - Try convert object to integer
        /// </summary>
        public static int TryToInt(this object obj, int defaultValue = default(int))
        {
            if (obj == null) return defaultValue;
            if (obj is int value)
            {
                return value;
            }
            else if (int.TryParse(obj.ToString(), out int result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try convert object to double
        /// </summary>
        public static double TryToDouble(this object obj, double defaultValue = default(double))
        {
            if (obj == null) return defaultValue;
            if (obj is double value)
            {
                return value;
            }
            else if (double.TryParse(obj.ToString(), out double result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try convert object to bool
        /// </summary>
        public static bool TryToBoolean(this object obj, bool defaultValue = default(bool))
        {
            if (obj == null) return defaultValue;
            if (obj is bool value)
            {
                return value;
            }
            else if (bool.TryParse(obj.ToString(), out bool result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try convert object to date time
        /// </summary>
        public static DateTime TryToDateTime(this object obj, DateTime defaultValue = default(DateTime))
        {
            if (obj == null) return defaultValue;
            if (obj is DateTime value)
            {
                return value;
            }
            else if (DateTime.TryParse(obj.ToString(), out DateTime result))
            {
                return result;
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try convert object to string
        /// </summary>
        public static string TryToString(this object obj, string defaultValue = default(string))
        {
            if (obj == null) return defaultValue;
            return obj.ToString();
        }
        #endregion

        #region Get value from dictionary

        /// <summary>
        /// TVG - Try get a value from dictionary as interger
        /// </summary>
        public static int TryGetInt(this Dictionary<string, object> dictionary, string key, int defaultValue = default(int))
        {
            if (dictionary.TryGetValue(key, out object result))
            {
                return result.TryToInt(defaultValue);
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try get a value from dictionary as double
        /// </summary>
        public static double TryGetDouble(this Dictionary<string, object> dictionary, string key, double defaultValue = default(double))
        {
            if (dictionary.TryGetValue(key, out object result))
            {
                return result.TryToDouble(defaultValue);
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try get a value from dictionary as boolean
        /// </summary>
        public static bool TryGetBoolean(this Dictionary<string, object> dictionary, string key, bool defaultValue = default(bool))
        {
            if (dictionary.TryGetValue(key, out object result))
            {
                return result.TryToBoolean(defaultValue);
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try get a value from dictionary as date time
        /// </summary>
        public static DateTime TryGetDateTime(this Dictionary<string, object> dictionary, string key, DateTime defaultValue = default(DateTime))
        {
            if (dictionary.TryGetValue(key, out object result))
            {
                return result.TryToDateTime(defaultValue);
            }
            return defaultValue;
        }

        /// <summary>
        /// TVG - Try get a value from dictionary as string
        /// </summary>
        public static string TryGetString(this Dictionary<string, object> dictionary, string key, string defaultValue = default(string))
        {
            if (dictionary.TryGetValue(key, out object result))
            {
                return result.TryToString(defaultValue);
            }
            return defaultValue;
        }
        #endregion

        #region Convert to list

        /// <summary>
        /// TVG - Try convert an array of string to list of interger
        /// </summary>
        public static List<int> TryToListInt(this string[] array)
        {
            if (array == null) return new List<int>();
            return array.Select(x => x.TryToInt()).ToList();
        }

        /// <summary>
        /// TVG - Try convert a list of string to list of interger
        /// </summary>
        public static List<int> TryToListInt(this List<string> list)
        {
            if (list == null) return new List<int>();
            return list.Select(x => x.TryToInt()).ToList();
        }

        /// <summary>
        /// TVG - Try convert an array of string to list of double
        /// </summary>
        public static List<double> TryToListDouble(this string[] array)
        {
            if (array == null) return new List<double>();
            return array.Select(x => x.TryToDouble()).ToList();
        }

        /// <summary>
        /// TVG - Try convert a list of string to list of double
        /// </summary>
        public static List<double> TryToListDouble(this List<string> list)
        {
            if (list == null) return new List<double>();
            return list.Select(x => x.TryToDouble()).ToList();
        }

        /// <summary>
        /// TVG - Try convert an array of string to list of boolean
        /// </summary>
        public static List<bool> TryToListBoolean(this string[] array)
        {
            if (array == null) return new List<bool>();
            return array.Select(x => x.TryToBoolean()).ToList();
        }

        /// <summary>
        /// TVG - Try convert a list of string to list of boolean
        /// </summary>
        public static List<bool> TryToListBoolean(this List<string> list)
        {
            if (list == null) return new List<bool>();
            return list.Select(x => x.TryToBoolean()).ToList();
        }

        /// <summary>
        /// TVG - Try convert an array of string to list of date time
        /// </summary>
        public static List<DateTime> TryToListDateTime(this string[] array)
        {
            if (array == null) return new List<DateTime>();
            return array.Select(x => x.TryToDateTime()).ToList();
        }

        /// <summary>
        /// TVG - Try convert a list of string to list of date time
        /// </summary>
        public static List<DateTime> TryToListDateTime(this List<string> list)
        {
            if (list == null) return new List<DateTime>();
            return list.Select(x => x.TryToDateTime()).ToList();
        } 
        #endregion
    }
}
