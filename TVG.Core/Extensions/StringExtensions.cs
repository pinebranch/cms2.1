﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Extensions
{
    public static class StringExtensions
    {
        public static string Prepend(this string str, string character, int numberOfCharacters)
        {
            if (numberOfCharacters <= 0 || string.IsNullOrEmpty(character)) return str;

            StringBuilder sb = new StringBuilder();
            for (var i = 1; i <= numberOfCharacters; i++)
            {
                sb.Append(character);
            }
            sb.Append(str);
            return sb.ToString();
        }

        public static string Append(this string str, string character, int numberOfCharacters)
        {
            if (numberOfCharacters <= 0 || string.IsNullOrEmpty(character)) return str;

            StringBuilder sb = new StringBuilder(str);
            for (var i = 1; i <= numberOfCharacters; i++)
            {
                sb.Append(character);
            }
            return sb.ToString();
        }

        /// <summary>
		/// Thêm chuỗi không rỗng vào list dạng chuỗi
		/// </summary>
		public static void AddNonEmpty(this List<string> list, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                list.Add(value);
            }
        }

        /// <summary>
		/// Thêm số nguyên khác 0 vào list dạng chuỗi
		/// </summary>
		public static void AddNonEmpty(this List<string> list, int value)
        {
            if (value != 0)
            {
                list.Add(value.ToString());
            }
        }

        /// <summary>
        /// Cắt chuỗi thành chuỗi con có khoảng n ký tự và cắt tại khoảng trắng mà không cắt ngang từ
        /// </summary>
        /// <param name="input">Chuỗi cần cắt</param>
        /// <param name="length">Số ký tự của chuỗi mới</param>
        /// <param name="trailing">Chuỗi gắn vào cuối chuỗi mới (vd: "...")</param>
        /// <returns></returns>
        public static string TruncateAtWord(this string input, int length, string trailing = "...")
        {
            if (string.IsNullOrEmpty(input) || input.Length < length) return input;

            int iNextSpace = input.LastIndexOf(" ", length);
            return string.Format("{0}{1}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim(), trailing);
        }

        /// <summary>
        /// Kiểm tra chuỗi có bắt đầu bằng một trong những chuỗi con nào đó
        /// </summary>
        public static bool StartsWithAny(this string source, IEnumerable<string> strings)
        {
            foreach (var valueToCheck in strings)
            {
                if (source.StartsWith(valueToCheck, StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool StartsWithAny(this string source, string[] strings)
        {
            foreach (var valueToCheck in strings)
            {
                if (source.StartsWith(valueToCheck, StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
