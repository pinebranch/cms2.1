﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVG.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToShortDateStringVN(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }

        public static string ToLongDateStringVN(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
