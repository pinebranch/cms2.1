﻿using System;
using System.Collections.Generic;
using System.Text;
using TVG.Core.Configuration;

namespace TVG.Core.Caching
{
    public static class CacheHelper
    {
        public static TimeSpan CacheDurationShort
        {
            get
            {
                var minutes = TvgAppSettings.Current.CacheDurationShort;
                return TimeSpan.FromMinutes(minutes);
            }
        }

        public static TimeSpan CacheDurationMedium
        {
            get
            {
                var minutes = TvgAppSettings.Current.CacheDurationMedium;
                return TimeSpan.FromMinutes(minutes);
            }
        }

        public static TimeSpan CacheDurationLong
        {
            get
            {
                var minutes = TvgAppSettings.Current.CacheDurationLong;
                return TimeSpan.FromMinutes(minutes);
            }
        }

        public static TimeSpan CacheDurationDay => TimeSpan.FromDays(1);

        public static TimeSpan CacheDurationWeek => TimeSpan.FromDays(7);

        public static TimeSpan CacheDurationMonth => TimeSpan.FromDays(30);

        public static TimeSpan CacheDurationYear => TimeSpan.FromDays(365);

        public static TimeSpan CacheDurationForever => TimeSpan.FromDays(36500);

        /// <summary>
        /// Đặt tên cache
        /// </summary>
        /// <param name="prefix">Tiền tố để phân biệt module</param>
        /// <param name="values">Các giá trị gắn kèm để phân biệt cache</param>
        /// <returns></returns>
        public static string SetCacheName(string prefix, params object[] values)
        {
            var cacheName = new StringBuilder();
            cacheName.Append(prefix);
            if (values.Length > 0)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    if (values[i] == null) continue;
                    cacheName.Append(values[i].ToString());
                    if (i < values.Length - 1)
                    {
                        cacheName.Append(";");
                    }
                }
            }
            return cacheName.ToString();
        }
    }
}
