﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Caching
{
    public interface ICacheManager
    {
        int DatabaseNumber { get; set; }
        
        string GetTypeName();
        
        /// <summary>
        /// Get cache entry by key name
        /// </summary>
        T Get<T>(string key);
        Task<T> GetAsync<T>(string key);

        /// <summary>
        /// Get cache entry by key name, if the key does not exist, get value by a function, then save that value to cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">Name of the group that the key is in</param>
        /// <param name="key">Key name of cache entry</param>
        /// <param name="getValue">Function that returns value, be executed if key name does not exist</param>       
        T GetOrAdd<T>(string group, string key, Func<T> getValue);
        Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue);

        /// <summary>
        /// Get cache entry by key name, if the key does not exist, get value by a function, then save that value to cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">Name of the group that the key is in</param>
        /// <param name="key">Key name of cache entry</param>
        /// <param name="getValue">Function that returns value, will be executed if key name does not exist</param>
        /// <param name="slidingExpiration">How long the entry cache can be inactive (not accessed) before it will be removed</param>
        T GetOrAdd<T>(string group, string key, Func<T> getValue, TimeSpan? slidingExpiration);
        Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue, TimeSpan? slidingExpiration);

        /// <summary>
        /// Set cache entry
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="group">Name of the group that the key is in</param>
        /// <param name="key">Key name of cache entry</param>
        /// <param name="value">Value of cache entry</param>
        /// <param name="slidingExpiration">How long the entry cache can be inactive (not accessed) before it will be removed</param>
        void Set<T>(string group, string key, T value, TimeSpan? slidingExpiration);
        Task SetAsync<T>(string group, string key, T value, TimeSpan? slidingExpiration);

        /// <summary>
        /// Add key name to a group
        /// </summary>
        void AddKeyToGroup(string group, string key, TimeSpan? slidingExpiration);
        Task AddKeyToGroupAsync(string group, string key, TimeSpan? slidingExpiration);

        /// <summary>
        /// Clear all caches
        /// </summary>
        void ClearAll();

        /// <summary>
        /// Clear all caches from a database (Redis)
        /// </summary>
        void Clear(int dbIndex);

        /// <summary>
        /// Remove a cache entry by key name
        /// </summary>
        void Remove(string key);

        /// <summary>
        /// Remove cache entries by pattern
        /// </summary>
        void RemoveByPattern(string pattern);

        /// <summary>
        /// Remove cache entries by group
        /// </summary>
        void RemoveByGroup(string group);
    }
}
