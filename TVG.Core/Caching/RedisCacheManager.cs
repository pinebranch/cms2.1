﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVG.Core.Configuration;

namespace TVG.Core.Caching
{
    public class RedisCacheManager : ICacheManager
    {
        private readonly ConnectionMultiplexer redisConnections;
        public int DatabaseNumber { get; set; }
        
        private static readonly Lazy<ConfigurationOptions> configOptions = new Lazy<ConfigurationOptions>(() =>
        {
            var configOptions = ConfigurationOptions.Parse(TvgAppSettings.Current.ConnectionStrings.RedisConnection);
            configOptions.ConnectTimeout = 100000;
            configOptions.SyncTimeout = 100000;
            configOptions.AbortOnConnectFail = false;
            configOptions.ClientName = "RedisConnection";
            return configOptions;
        });

        private static readonly Lazy<ConnectionMultiplexer> conn = new Lazy<ConnectionMultiplexer>(
                () => ConnectionMultiplexer.Connect(configOptions.Value));

        private static ConnectionMultiplexer Connection
        {
            get
            {
                return conn.Value;
            }
        }

        public RedisCacheManager()
        {
            this.redisConnections = Connection;
        }

        public void AddKeyToGroup(string group, string key, TimeSpan? slidingExpiration)
        {
            if (string.IsNullOrEmpty(group)) return;
            var db = redisConnections.GetDatabase(DatabaseNumber);
            db.SetAdd(group, key);
            db.KeyExpire(group, slidingExpiration);
        }

        public async Task AddKeyToGroupAsync(string group, string key, TimeSpan? slidingExpiration)
        {
            if (string.IsNullOrEmpty(group)) return;
            var db = redisConnections.GetDatabase(DatabaseNumber);
            await db.SetAddAsync(group, key);
            await db.KeyExpireAsync(group, slidingExpiration);
        }

        public T Get<T>(string key)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);
            if (serializedObject.IsNullOrEmpty) return default(T);

            T value = default(T);
            try
            {
                value = JsonConvert.DeserializeObject<T>(serializedObject);
            }
            catch
            {
                //khong deserialize duoc ---> key bi loi ---> xoa key
                db.KeyDelete(key);
                value = default(T);
            }
            
            return value;
        }

        public async Task<T> GetAsync<T>(string key)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = await db.StringGetAsync(key);
            if (serializedObject.IsNullOrEmpty) return default(T);

            T value = default(T);
            try
            {
                value = JsonConvert.DeserializeObject<T>(serializedObject);
            }
            catch
            {
                //khong deserialize duoc ---> key bi loi ---> xoa key
                db.KeyDelete(key);
                value = default(T);
            }

            return value;
        }

        public T GetOrAdd<T>(string group, string key, Func<T> getValue)
        {
            return GetOrAdd(group, key, getValue, null);
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue)
        {
            return GetOrAddAsync(group, key, getValue, null);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> getValue, TimeSpan? slidingExpiration)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = db.StringGet(key);
            T value = default(T);
            bool isDeserialized = false;

            if (!serializedObject.IsNullOrEmpty)
            {
                try
                {
                    value = JsonConvert.DeserializeObject<T>(serializedObject);
                    db.KeyExpire(key, slidingExpiration);
                    if (!string.IsNullOrEmpty(group))
                    {
                        db.KeyExpire(group, slidingExpiration);
                    }
                    isDeserialized = true;
                }
                catch
                {
                    //khong deserialize duoc ---> key bi loi ---> xoa key
                    db.KeyDelete(key);
                    isDeserialized = false;
                }
            }

            if(!isDeserialized)
            {
                value = getValue();
                Set(group, key, value, slidingExpiration);
            }

            return value;
        }

        public async Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue, TimeSpan? slidingExpiration)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = await db.StringGetAsync(key);
            T value = default(T);
            bool isDeserialized = false;

            if (!serializedObject.IsNullOrEmpty)
            {
                try
                {
                    value = JsonConvert.DeserializeObject<T>(serializedObject);
                    await db.KeyExpireAsync(key, slidingExpiration);
                    if (!string.IsNullOrEmpty(group))
                    {
                        await db.KeyExpireAsync(group, slidingExpiration);
                    }
                    isDeserialized = true;
                }
                catch
                {
                    //khong deserialize duoc ---> key bi loi ---> xoa key
                    db.KeyDelete(key);
                    isDeserialized = false;
                }
            }
            
            if(!isDeserialized)
            {
                value = await getValue();
                await SetAsync(group, key, value, slidingExpiration);
            }

            return value;
        }

        public string GetTypeName()
        {
            return "Redis";
        }

        public void Set<T>(string group, string key, T value, TimeSpan? slidingExpiration)
        {
            if (value == null) return;

            var expiration = slidingExpiration ?? TimeSpan.FromMinutes(TvgAppSettings.Current.CacheDurationShort);

            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = JsonConvert.SerializeObject(value);
            db.StringSet(key, serializedObject, expiration);
            if (!string.IsNullOrEmpty(group))
            {
                db.SetAdd(group, key);
                db.KeyExpire(group, expiration);
            }
        }

        public async Task SetAsync<T>(string group, string key, T value, TimeSpan? slidingExpiration)
        {
            if (value == null) return;

            var expiration = slidingExpiration ?? TimeSpan.FromMinutes(TvgAppSettings.Current.CacheDurationShort);

            var db = redisConnections.GetDatabase(DatabaseNumber);
            var serializedObject = JsonConvert.SerializeObject(value);
            await db.StringSetAsync(key, serializedObject, expiration);
            if (!string.IsNullOrEmpty(group))
            {
                await db.SetAddAsync(group, key);
                await db.KeyExpireAsync(group, expiration);
            }
        }

        public void Clear(int dbIndex)
        {
            if (dbIndex < 0) return;
            var endpoints = redisConnections.GetEndPoints();
            var server = redisConnections.GetServer(endpoints.First());
            server.FlushDatabase(dbIndex);
        }

        public void ClearAll()
        {
            var endpoints = redisConnections.GetEndPoints();
            var server = redisConnections.GetServer(endpoints.First());
            server.FlushAllDatabases();
        }

        public void Remove(string key)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            db.KeyDelete(key);
        }

        public void RemoveByPattern(string pattern)
        {
            var endpoints = redisConnections.GetEndPoints();
            var server = redisConnections.GetServer(endpoints.First());
            var keys = server.Keys(pattern: pattern + "*", database: DatabaseNumber);
            var db = redisConnections.GetDatabase(DatabaseNumber);
            db.KeyDelete(keys.ToArray());
        }

        public void RemoveByGroup(string group)
        {
            var db = redisConnections.GetDatabase(DatabaseNumber);
            var items = db.SetPop(group);
            while (items.HasValue)
            {
                db.KeyDelete(items.ToString());
                items = db.SetPop(group);
            }
            db.KeyDelete(group);
        }
    }
}
