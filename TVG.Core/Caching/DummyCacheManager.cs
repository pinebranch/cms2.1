﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TVG.Core.Caching
{
    public class DummyCacheManager : ICacheManager
    {
        public int DatabaseNumber { get; set; }
        

        public string GetTypeName()
        {
            return "Dummy Cache";
        }

        public T GetOrAdd<T>(string group, string key, Func<T> getValue)
        {
            return GetOrAdd(group, key, getValue, null);
        }

        public T Get<T>(string key)
        {
            return default(T);
        }

        public T GetOrAdd<T>(string group, string key, Func<T> getValue, TimeSpan? slidingExpiration)
        {
            return getValue();
        }

        public void Set<T>(string group, string key, T value, TimeSpan? slidingExpiration)
        {
            return;
        }

        public void AddKeyToGroup(string group, string key, TimeSpan? slidingExpiration)
        {
            return;
        }

        public void ClearAll()
        {
            return;
        }

        public void Clear(int dbIndex)
        {
            return;
        }

        public void Remove(string key)
        {
            return;
        }

        public void RemoveByPattern(string pattern)
        {
            return;
        }

        public void RemoveByGroup(string group)
        {
            return;
        }

        public Task<T> GetAsync<T>(string key)
        {
            return Task.FromResult(default(T));
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue)
        {
            return getValue();
        }

        public Task<T> GetOrAddAsync<T>(string group, string key, Func<Task<T>> getValue, TimeSpan? slidingExpiration)
        {
            return getValue();
        }

        public Task SetAsync<T>(string group, string key, T value, TimeSpan? slidingExpiration)
        {
            return Task.FromResult(0);
        }

        public Task AddKeyToGroupAsync(string group, string key, TimeSpan? slidingExpiration)
        {
            return Task.FromResult(0);
        }
    }
}
